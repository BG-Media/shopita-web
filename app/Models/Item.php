<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Item extends Model
{
    protected $table = 'item';
    protected $primaryKey = 'item_id';

    use SoftDeletes;
    protected $dates = ['deleted_at'];
}
