<?php
namespace App\Helpers;

class HttpHelper {
    public static function showErrors($errors) {
        return response()->json([
            'success' => false,
            'errors' => $errors,
        ]);
    }

    public static function showError($error) {
        return response()->json([
            'success' => false,
            'error' => $error,
        ]);
    }

    public static function showSuccess($data) {
        return response()->json([
            'success' => true,
            'data' => $data,
        ]);
    }
}