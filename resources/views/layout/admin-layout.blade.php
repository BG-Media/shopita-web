<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Shopita Админка</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="/admin/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/admin/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="/admin/dist/css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="/admin/plugins/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="/admin/plugins/morris/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="/admin/plugins/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="/admin/plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

    <link rel="stylesheet" href="/admin/plugins/datatables/dataTables.bootstrap.css">
    <link rel="stylesheet" href="/css/bootstrap-datetimepicker.css">
    <link rel="stylesheet" href="/css/bootstrap-datetimepicker-standalone.min.css">
    <style>
        #notification {
            display: inline-block;
            margin-left: 10px;
            padding: 0 5px;
            border-radius: 3px;
            background-color: rgb(248, 23, 23);
        }
    </style>

  </head>
  <body class="hold-transition skin-blue sidebar-mini layout-boxed">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="/" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini">Shopita</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg">Shopita</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    @if(strpos(Auth::user()->avatar, 'http') !== false)
                        <img src="{{Auth::user()->avatar}}" class="user-image" alt="User Image">
                    @else
                        <img src="/image/avatar/{{Auth::user()->avatar}}" class="user-image" alt="User Image">
                    @endif

                  <span class="hidden-xs">{{Auth::user()->name}}</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                      @if(strpos(Auth::user()->avatar, 'http') !== false)
                          <img src="{{Auth::user()->avatar}}" class="img-circle" alt="User Image">
                      @else
                          <img src="/image/avatar/{{Auth::user()->avatar}}" class="img-circle" alt="User Image">
                      @endif
                    <p>
                      Администратор
                      <small>shopita.kz</small>
                    </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="/admin/password" class="btn btn-default btn-flat">Сменить пароль</a>
                    </div>
                    <div class="pull-right">
                      <a href="/logout" class="btn btn-default btn-flat">Выйти</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

          <ul class="sidebar-menu">
              <li class="header">МЕНЮ</li>
              <li class="treeview">
                  <a href="/admin/order">
                      <i class="fa fa-bars"></i>
                      <span>Заказы</span>
                  </a>
              </li>
              <li class="treeview">
                  <a href="/admin/product">
                      <i class="fa fa-tasks"></i>
                      <span>Товары</span>
                  </a>
              </li>
              <li class="treeview">
                  <a href="/admin/user">
                    <i class="fa fa-user"></i>
                    <span>Пользователи</span>
                  </a>
              </li>
              <li class="treeview">
                  <a href="/admin/review?active=1">
                      <i class="fa fa-comment-o"></i>
                      <span>Отзыв <p id="notification">{{ $commentsCount }}</p></span>
                  </a>
              </li>
              <li class="treeview">
                  <a href="/admin/blog">
                      <i class="fa fa-bold"></i>
                      <span>Блог</span>
                  </a>
              </li>
              <li class="treeview">
                  <a href="/admin/status-of-sale">
                      <i class="fa fa-tasks"></i>
                      <span>Статус продажа</span>
                  </a>
              </li>
              <li class="treeview">
                  <a href="/admin/discount">
                      <i class="fa fa-shopping-cart"></i>
                      <span>Скидки</span>
                  </a>
              </li>
              <li class="treeview">
                  <a href="/admin/sale">
                      <i class="fa fa-shopping-cart"></i>
                      <span>Скидочные купоны</span>
                  </a>
              </li>
              <li class="treeview">
                  <a href="#">
                      <i class="fa fa-files-o"></i>
                      <span>Контент сайта</span> <i class="fa fa-angle-left pull-right"></i>
                  </a>
                  <ul class="treeview-menu">
                      <li class="treeview">
                          <a href="/admin/banner">
                              <i class="fa fa-files-o"></i>
                              <span>Баннер</span>
                          </a>
                      </li>
                      <li class="treeview">
                          <a href="/admin/page">
                              <i class="fa fa-files-o"></i>
                              <span>Статичные страницы</span>
                          </a>
                      </li>
                      <li class="treeview">
                          <a href="/admin/brand">
                              <i class="fa fa-files-o"></i>
                              <span>Бренд (слайдер)</span>
                          </a>
                      </li>
                      <li class="treeview">
                          <a href="/admin/main-page-text">
                              <i class="fa fa-files-o"></i>
                              <span>Текст главной страницы</span>
                          </a>
                      </li>
                  </ul>
              </li>
              <li class="treeview">
                  <a href="#">
                      <i class="fa fa-envelope"></i>
                      <span>Рассылка</span> <i class="fa fa-angle-left pull-right"></i>
                  </a>
                  <ul class="treeview-menu">
                      <li>
                          <a href="/admin/delivery">
                              <i class="fa fa-envelope"></i>
                              <span>История</span>
                          </a>
                      </li>
                      <li class="treeview">
                          <a href="/admin/delivery-send/0">
                              <i class="fa fa-envelope"></i>
                              <span>Отправить</span>
                          </a>
                      </li>
                      <li class="treeview">
                          <a href="/admin/delivery-kind">
                              <i class="fa fa-envelope"></i>
                              <span>База</span>
                          </a>
                      </li>
                      <li class="treeview">
                          <a href="/admin/contact-delivery">
                              <i class="fa fa-envelope"></i>
                              <span>Контакты</span>
                          </a>
                      </li>
                      <li class="treeview">
                          <a href="/admin/subscriber/create">
                              <i class="fa fa-envelope"></i>
                              <span>Добавить подписку</span>
                          </a>
                      </li>
                  </ul>
              </li>
              <li class="treeview">
                  <a href="#">
                      <i class="fa fa-sitemap"></i>
                      <span>Категория товара</span> <i class="fa fa-angle-left pull-right"></i>
                  </a>
                  <ul class="treeview-menu">
                      <li class="treeview">
                          <a href="/admin/main-category">
                              <i class="fa fa-files-o"></i>
                              <span>Главная категория</span>
                          </a>
                      </li>
                      <li class="treeview">
                          <a href="/admin/category">
                              <i class="fa fa-files-o"></i>
                              <span>Категория</span>
                          </a>
                      </li>
                      <li class="treeview">
                          <a href="/admin/subcategory">
                              <i class="fa fa-files-o"></i>
                              <span>Подкатегория</span>
                          </a>
                      </li>
                  </ul>
              </li>
              <li class="treeview">
                  <a href="#">
                      <i class="fa fa-list"></i>
                      <span>Справочник</span> <i class="fa fa-angle-left pull-right"></i>
                  </a>
                  <ul class="treeview-menu">
                      <li class="treeview">
                          <a href="/admin/brand-product">
                              <i class="fa fa-list"></i>
                              <span>Бренд товара</span>
                          </a>
                      </li>
                      <li class="treeview">
                          <a href="/admin/feature">
                              <i class="fa fa-list"></i>
                              <span>Характеристика</span>
                          </a>
                      </li>
                      <li class="treeview">
                          <a href="/admin/size">
                              <i class="fa fa-list"></i>
                              <span>Размер товара</span>
                          </a>
                      </li>
                      <li class="treeview">
                          <a href="/admin/color">
                              <i class="fa fa-list"></i>
                              <span>Цвет товара</span>
                          </a>
                      </li>
                      <li class="treeview">
                          <a href="/admin/promo-code">
                              <i class="fa fa-list"></i>
                              <span>Промокод</span>
                          </a>
                      </li>
                  </ul>
              </li>
              <li class="treeview">
                  <a href="/admin/password">
                      <i class="fa fa-user"></i>
                      <span>Сменить пароль</span>
                  </a>
              </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">

             @yield('content')

        </section>

      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <strong>Copyright &copy; 2016 <a href="/">shopita.kz</a>.</strong>
      </footer>

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">

        </ul>
        <div class="tab-content">
          <div class="tab-pane" id="control-sidebar-home-tab"></div>
        </div>
      </aside>
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->




  </body>
</html>

<div class="blur"></div>

<i class="ajax-loader"></i>

<meta name="csrf-token" content="{{ csrf_token() }}">

<style>
 .redirect-url {
    color: #333333;
 }
 .redirect-url:hover {
    text-decoration: underline;
    color: #333333;
 }
 .user-avatar {
    float:left;
    padding: 2px;
    border-radius: 5px;
    border: 1px solid #afafaf;
    margin-right: 20px;
    margin-left: 15px;
 }
 .banner-img {
    float:left;
    padding: 2px;
    border-radius: 5px;
    border: 1px solid #afafaf;
    margin-right: 20px;
    margin-left: 15px;
 }
 .banner-img img {
    border-radius: 5px;
    max-width: 400px;
    max-height: 150px;
 }
 .user-avatar img {
    width: 60px;
    height: 60px;
    border-radius: 5px;
 }
 .user-name {
    float: left;
    font-size: 16px;
    font-family: arial;
 }
 .arial-font {
    font-size: 16px;
     font-family: arial;
 }
 .clear-float {
    clear: both;
 }
 .left-float {
    float: left;
 }
 .delivery-checkbox {
    padding-right: 10px;
 }
  .avatar-form {
     text-align: center; border-radius: 50%; height: 40px; width: 40px; padding: 4px 0px 0px; background-color: rgb(181, 227, 242); border-color: rgb(181, 227, 242); margin: 0px auto; color: white; font-size: 23px;
  }
  .avatar-form input {
     opacity: 0; position: absolute; border-radius: 50%; width: 40px; height: 40px; margin-top: -37px; cursor: pointer;
  }
  .variant-class input {
    margin-left: 10px;
  }
  .margin-top-15 {
    margin-top: 15px;
    margin-bottom: 25px;
  }
 .no-sort::after { display: none!important; }

 .ajax-loader {
     background-image: url("/img/ajax-loader.gif");
     background-size: 60px 60px;
     border-radius: 60px;
     display: none;
     height: 60px;
     left: 50%;
     margin-left: -30px;
     margin-top: -30px;
     position: fixed;
     top: 50%;
     width: 60px;
     z-index: 1000;
 }
    .active-paginator a {
        background-color: #ECF0F5 !important;
    }

     .modal-form {
         left: 50%;
         margin-left: -200px;
         max-width: 400px;
         position: fixed;
         top: 30%;
         z-index: 10001;
         display: none;
     }

     .blur {
         background-color: black;
         display: none;
         height: 100%;
         opacity: 0.5;
         position: fixed;
         top: 1px;
         width: 100%;
         z-index: 10000;
     }

     .redirect-url {
         color: #236c96;
         text-decoration: underline;
     }

      .feature-item {
          border: 1px solid #858585;
          margin-bottom: 5px;
      }
      .feature-item-left {
         width: 70%; float: left; margin-bottom: 0px; padding-top: 2px;
      }
      .feature-item-right {
         width: 30%; float: left; margin-bottom: 0px; padding-top: 2px;text-align: right;
      }

</style>

<!-- jQuery 2.1.4 -->
<script src="/admin/plugins/jQuery/jQuery-2.1.4.min.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/js/bootstrap-select.min.js"></script>

<!-- Bootstrap 3.3.5 -->
<script src="/admin/bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="/admin/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/admin/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="/admin/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="/admin/plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->
<script src="/admin/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/admin/dist/js/demo.js"></script>
@yield('script')
<script>
    $(function () {
        $("#example1").DataTable();
    });

</script>

<script>
    $("#avatar_form").submit(function(event) {
        event.preventDefault();
        var formData = new FormData($(this)[0]);
        $.ajax({
            url:'/admin/user/upload-avatar',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                if(data.success == 0){
                    alert(data.error);
                    return;
                }

                $('#avatar').val(data.file_name);
                $('#avatar_img').attr('src','/image/avatar/' + data.file_name);
            }
        });
    });

    $("#blog_image_form").submit(function(event) {
        event.preventDefault();
        var formData = new FormData($(this)[0]);
        $.ajax({
            url:'/admin/blog/upload-image',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                if(data.success == 0){
                    alert(data.error);
                    return;
                }

                $('#page_image').val(data.file_name);
                $('#avatar_img').attr('src','/image/blog/' + data.file_name);
            }
        });
    });

    $("#sale_image_form").submit(function(event) {
        event.preventDefault();
        var formData = new FormData($(this)[0]);
        $.ajax({
            url:'/admin/sale/upload-image',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                if(data.success == 0){
                    alert(data.error);
                    return;
                }

                $('#page_image').val(data.file_name);
                $('#avatar_img').attr('src','/image/sale/' + data.file_name);
            }
        });
    });

    $("#brand_image_form").submit(function(event) {
        event.preventDefault();
        var formData = new FormData($(this)[0]);
        $.ajax({
            url:'/admin/brand/upload-image',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                if(data.success == 0){
                    alert(data.error);
                    return;
                }

                $('#page_image').val(data.file_name);
                $('#avatar_img').attr('src','/image/brand/' + data.file_name);
            }
        });
    });

    $("#banner_image_form").submit(function(event) {
        event.preventDefault();
        var formData = new FormData($(this)[0]);
        $.ajax({
            url:'/admin/banner/upload-image',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                if(data.success == 0){
                    alert(data.error);
                    return;
                }

                $('#page_image').val(data.file_name);
                $('#avatar_img').attr('src','/image/banner/' + data.file_name);
            }
        });
    });

    $("#product_image_form").submit(function(event) {
        event.preventDefault();

        var formData = new FormData($(this)[0]);
        $.ajax({
            url:'/admin/product/upload-image?product_id=' + $('#product_id').val() + '&product_token=' + $('meta[name="csrf-token"]').attr('content'),
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                if(data.success == 0){
                    alert(data.error);
                    return;
                }
                getImageList(1);
            }
        });
    });

    $('.blur').click(function(){
        $('.modal-form').fadeOut(100);
        $('.blur').fadeOut(100);
    });
</script>


    