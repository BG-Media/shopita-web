{{--{{dd($request->all())}}--}}
@extends('layout.layout')

@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="container">
                    <div class="row">
                        @include('index.catalog.left-menu')
                        <div class="col-md-10 col-sm-10">
                            <div class="clearfix hidden-xs">
                                <ol class="breadcrumb">
                                    <li>
                                        <a href="/">Главная</a>
                                    </li>
                                    <li>
                                        <a href="/catalog">@if($sex == '1') Мужчинам @else Женщинам @endif</a>
                                    </li>

                                    @if(isset($request->main_category))
                                        <li>
                                            @if(isset($request->category))
                                                <a href="/catalog?main={{$request->main_category['category_url']}}-u{{$request->main_category['category_id']}}">{{$request->main_category['category_name_ru']}}</a>
                                            @else
                                                {{$request->main_category['category_name_ru']}}
                                            @endif
                                        </li>
                                    @endif

                                    @if(isset($request->category))
                                        <li>
                                            @if(isset($request->subcategory))
                                                <a href="/catalog?category={{$request->category['category_url']}}-u{{$request->category['category_id']}}">{{$request->category['category_name_ru']}}</a>
                                            @else
                                                {{$request->category['category_name_ru']}}
                                            @endif
                                        </li>
                                    @endif

                                    @if(isset($request->subcategory))
                                        <li>
                                            {{$request->subcategory['category_name_ru']}}
                                        </li>
                                    @endif
                                </ol>
                            </div>
                            <div class="clearfix title_page_block">
                                <h3 class="title_page">{{$request->page_title}}</h3>
                                <div class="list_sort dropdowns_top">
                                    <div class="dropdown">
                                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                            Сортировка
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                            <li>
                                                <a href="/catalog?{{\App\Helpers::replaceGetUrl('sort,product_price,sort_desc,asc')}}">По возрастанию цены</a>
                                            </li>
                                            <li>
                                                <a href="/catalog?{{\App\Helpers::replaceGetUrl('sort,product_price,sort_desc,desc')}}">По убыванию цены</a>
                                            </li>
                                            <li>
                                                <a href="/catalog?{{\App\Helpers::replaceGetUrl('sort,random')}}">Случайно</a>
                                            </li>
                                            <li>
                                                <a href="/catalog?{{\App\Helpers::replaceGetUrl('sort,view_count,sort_desc,desc')}}">По популярности</a>
                                            </li>

                                        </ul>
                                    </div>

                                    <div class="right_link pull-right">
                                        <a href="/catalog?{{\App\Helpers::getResetProduct()}}" class="sort_reset">Сбросить все</a>
                                    </div>
                                </div>
                                <p class="results_number">Найдено {{$product->total()}} товаров</p>

                            </div>

                            <div class="clearfix">

                                @include('index.catalog.product-sort')

                                <div class="catalogMainProducts clearfix">

                                    @include('index.catalog.product-list-loop')

                                </div>

                            </div>
                        </div>
                    </div>

                    @include('layout.footer')

                </div>
            </div>
        </div>
    </div>

@endsection

