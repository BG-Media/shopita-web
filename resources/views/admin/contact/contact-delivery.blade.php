@extends('layout.admin-layout')

@section('content')

<div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
            <h3 class="box-title" style="margin-top: 5px; margin-right: 30px;float: left">
                <a  style="color: #333333;">Контакты</a>
            </h3>
            <a href="/admin/contact-delivery/create?delivery_kind_id=@if(isset($request->delivery_kind_id)){{$request->delivery_kind_id}}@endif" style="float: right">
                <button class="btn btn-primary" style="margin-bottom: 10px; ">Добавить контакт</button>
            </a>
            <div style="clear: both"></div>
        </div>
          <div style="clear: both"></div>
          <div style="padding-top: 10px; border-top: 1px solid rgb(127, 127, 127);">
              <div class="form-group col-md-3" >
                  <label>Поиск</label>
                  <input id="search_word" value="{{$request->search}}" type="text" class="form-control" name="search_word" placeholder="Введите">
              </div>
              <div class="form-group col-md-3" >
                  <label>База</label>
                  <select class="form-control" id="delivery_kind_id">
                      <option value="0">-------------</option>
                      @foreach($delivery_kind as $value)
                          <option value="{{$value->delivery_kind_id}}" @if(isset($request->delivery_kind_id) && $request->delivery_kind_id == $value->delivery_kind_id) {{ ' selected ' }} @endif>{{$value->delivery_kind_name}}</option>
                      @endforeach
                  </select>
              </div>
              <div class="form-group col-md-3" >
                  <label>Показать</label>
                  <select class="form-control" id="list_count">
                      <option value="10" @if(isset($request->list_count) && $request->list_count == '10') selected @endif>10 штук</option>
                      <option value="20" @if(isset($request->list_count) && $request->list_count == '20') selected @endif>20 штук</option>
                      <option value="30" @if(isset($request->list_count) && $request->list_count == '30') selected @endif>30 штук</option>
                      <option value="50" @if(isset($request->list_count) && $request->list_count == '50') selected @endif>50 штук</option>
                  </select>
              </div>
                <div class="form-group col-md-3" style="padding-top: 25px" >
                    <a href="javascript:void(0)" onclick="searchBySort()">
                        <button type="button" class="btn btn-block btn-success">Поиск</button>
                    </a>
                </div>
          </div>
<div style="clear: both"></div>
<div>
    <div style="text-align: left" class="form-group col-md-6" >

    </div>
    <div style="text-align: right" class="form-group col-md-6" >
        <h4 class="box-title" style="margin-top: 5px; margin-bottom: 0px">
            <a href="javascript:void(0)" onclick="deleteAll()" style="text-decoration: underline; font-size: 17px; color: rgb(248, 23, 23);">Удалить отмеченные</a>
        </h4>
    </div>
</div>
<div class="box-body">
<table id="news_datatable" class="table table-bordered table-striped">
  <thead>
    <tr style="border: 1px">
      <th style="width: 30px">№</th>
      <th style="width: 100px">Имя</th>
      <th style="width: 120px">Почта и телефон</th>
      <th>Инфо</th>
      <th>База</th>
      <th style="width: 90px">Дата</th>
      {{--<th style="width: 30px">Просмотрено</th>--}}
        <th style="width: 30px"></th>
        <th style="width: 30px"></th>
        <th class="no-sort" style="width: 0px; text-align: center; padding-right: 16px; padding-left: 14px;" >
          <input onclick="selectAllCheckbox(this)" style="font-size: 15px" type="checkbox" value="1"/>
        </th>
        </tr>
  </thead>

            <tbody>
                  @foreach($row as $key => $val)

                     <tr>
                        <td> {{ $key + 1 }}</td>
                        <td>
                            {{ $val->name }}
                        </td>
                        <td>
                            {{ $val->email }} </br>
                            {{ $val->phone }}
                        </td>
                        <td>
                            {{ $val->info }}
                        </td>
                        <td>
                             {{ $val->delivery_kind_name }}
                        </td>
                        <td>
                            {{ $val->date }}
                        </td>
{{--                         <td style="text-align: center;">
                             <input @if($val->is_show == '1') {{ ' checked ' }} @endif onclick="isShow(this,'{{ $val->feedback_id }}')" style="font-size: 15px" type="checkbox" value="1"/>
                         </td>--}}
                        <td style="text-align: center">
                            <a href="javascript:void(0)" onclick="delItem(this,{{ $val->subscriber_id }})">
                                <li class="fa fa-trash-o" style="font-size: 20px; color: red;"></li>
                            </a>
                        </td>
                         <td style="text-align: center">
                             <a href="/admin/contact-delivery/{{ $val->subscriber_id }}/edit">
                                 <li class="fa fa-pencil" style="font-size: 20px;"></li>
                             </a>
                         </td>
                        <td style="text-align: center;">
                             <input class="select-all" style="font-size: 15px" type="checkbox" value="{{$val->subscriber_id}}"/>
                        </td>
                     </tr>

                  @endforeach

            </tbody>

          </table>

            @if($row->lastPage() > 1)

                <div class="page_number" style="text-align: center">
                    <ul class="pagination">
                        <li>
                            <a @if($row->currentPage() > 1) href="?{{http_build_query($_GET)}}&page={{ $row->currentPage() - 1 }}" @endif >&laquo;</a>
                        </li>
                        <?
                        $start = 1;
                        if($row->currentPage() >= 10){
                            $start = $row->currentPage();
                        }
                        $finish = $start + 9;
                        if($finish > $row->lastPage()){
                            $finish = $row->lastPage();
                            $start = $finish - 10;
                            if($start <= 0){
                                $start = 1;
                            }
                        }
                        ?>
                        @for ($i = $start; $i <= $finish; $i++)
                            <li class="{{ ($row->currentPage() == $i) ? 'active-paginator' : '' }}">
                                <a href="?{{http_build_query($_GET)}}&page={{ $i }}">{{ $i }}</a>
                            </li>
                        @endfor
                        <li>
                            <a @if($row->currentPage() < $row->lastPage()) href="?{{http_build_query($_GET)}}&page={{ $row->currentPage() + 1 }}" @endif>&raquo;</a>
                        </li>
                    </ul>
                </div>

            @endif

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
    </div><!-- /.row -->

    <meta name="csrf-token" content="{{ csrf_token() }}">

       <script>
           function delItem(ob,id){
               if(confirm('Действительно хотите удалить?')){
                   $(ob).closest('tr').remove();
                   $.ajax({
                       type: 'DELETE',
                       headers: {
                           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                       },
                       url: "/admin/subscriber/" + id,
                       success: function(data){

                       }
                   });
               }
           }



           function selectAllCheckbox(ob) {
               if ($(ob).is(':checked')) {
                   $('.select-all').prop('checked', true);
                   console.log('ok');
               }
               else {
                   $('.select-all').prop('checked', false);
                   console.log('ok1');
               }

           }

           function deleteAll() {
               if(confirm('Действительно хотите удалить?')){
                   $('.ajax-loader').fadeIn(100);
                   $('.select-all').each(function(){
                       if ($(this).is(':checked')) {
                           $.ajax({
                               type: 'DELETE',
                               headers: {
                                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                               },
                               url: "/admin/subscriber/" + $(this).val(),
                               success: function(){

                               }
                           });
                           $(this).closest('tr').remove();
                       }
                   });
                   $('.ajax-loader').fadeOut(100);
               }
           }


           function searchBySort() {
               href = '?delivery_kind_id=' + $('#delivery_kind_id').val() + '&search=' + $('#search_word').val() + '&list_count=' + $('#list_count').val();
               window.location.href = href;
           }

       </script>

@endsection