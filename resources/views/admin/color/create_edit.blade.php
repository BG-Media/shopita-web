@extends('layout.admin-layout')

@section('content')
    <section class="content-header">
        <h1>Цвет товаров</h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
                @if(isset($color))
                    <form action="/admin/color/{{$color->id}}" method="post">
                        <input type="hidden" name="_method" value="PUT">
                @else
                    <form action="/admin/color" method="POST">
                @endif
                    <input type="hidden" name="_token" value=" {{csrf_token()}}">
                    <div class="form-group">
                        <div class="form-group">
                            <label for="id_select">Заголовок цвета</label>
                            <input type="text" class="form-control" name="title_color" value="@if(isset($color->title_color )){{$color->title_color}}@endif">
                        </div>
                        <div class="form-group">
                            <label for="id_select">Код цвета</label>
                            <input type="text" class="form-control" name="code_color" value="@if(isset($color->code_color )){{$color->code_color}}@endif">
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

