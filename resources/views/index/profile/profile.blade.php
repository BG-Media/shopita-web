@extends('layout.layout')

@section('content')

    <div class="content profile">
        <div class="container-fluid">
            <div class="row">
                <div class="container">
                    <div class="row content_contacts">
                        <div class="tabs-left tab_1">
                            <div class="col-md-3 col-sm-3 col-12">
                                <ul class="nav nav-tabs">
                                    <li class="@if(!isset($request->page_tab) || $request->page_tab == 1) active @endif"><a href="#a" data-toggle="tab"><span>Мои данные</span></a></li>
                                    <li class="@if(isset($request->page_tab) && $request->page_tab == 2) active @endif"><a href="#b" data-toggle="tab"><span>Мои заказы</span></a></li>
                                    <li class="@if(isset($request->page_tab) && $request->page_tab == 3) active @endif"><a href="#c" data-toggle="tab"><span>Мои подписки</span></a></li>
                                    <li class="@if(isset($request->page_tab) && $request->page_tab == 4) active @endif"><a href="#d" data-toggle="tab"><span>Избранные</span></a></li>
                                </ul>
                            </div>

                            <div class="col-md-9 col-sm-9 col-12 tab-content">

                                <div class="tab-pane @if(!isset($request->page_tab) || $request->page_tab == 1) active @endif" id="a">
                                    @include('index.profile.profile-edit')
                                </div>

                                <div class="tab-pane @if(isset($request->page_tab) && $request->page_tab == 2) active @endif " id="b">
                                    @include('index.profile.my-orders')
                                </div>

                                <div class="tab-pane @if(isset($request->page_tab) && $request->page_tab == 3) active @endif" id="c">
                                    @include('index.profile.subscribe')
                                </div>

                                <div class="tab-pane @if(isset($request->page_tab) && $request->page_tab == 4) active @endif" id="d">
                                    @include('index.profile.my-favorite-product')
                                </div>

                            </div>
                        </div>
                    </div>

                    @include('layout.footer')

                </div>
            </div>
        </div>
    </div>

@endsection

