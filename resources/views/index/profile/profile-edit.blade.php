<ol class="breadcrumb">
    <li><a href="/">Главная</a></li>
    <li>Профиль</li>
    <li class="active">Мои данные</li>
</ol>
<a href="#" class="pull-right change_code">Сменить пароль</a>
<h2>Мои данные</h2>

<div class="col-md-12 data">
    <div class="row">
        <div class="col-md-2 col-sm-3 col-xs-3">
            <div class="row">
                <p>Ф.И.О</p>
            </div>
        </div>
        <div class="col-md-10 col-sm-9 col-xs-9">
            <p id="user_name">{{Auth::user()->name}}</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-sm-3 col-xs-3">
            <div class="row">
                <p>Телефон</p>
            </div>
        </div>
        <div class="col-md-10 col-sm-9 col-xs-9">
            <p id="user_phone">{{Auth::user()->mobile_phone}}</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-sm-3 col-xs-3">
            <div class="row">
                <p>Email</p>
            </div>
        </div>
        <div class="col-md-10 col-sm-9 col-xs-9">
            <p id="user_email">{{Auth::user()->email}}</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-sm-3 col-xs-3">
            <div class="row">
                <p>Пол</p>
            </div>
        </div>
        <div class="col-md-10 floor col-sm-9 col-xs-9">
            <p>
                <span id="user_male_0" class="@if(Auth::user()->sex == '0') active-sex @endif green user_male">Мужской</span><span id="user_male_1" class="@if(Auth::user()->sex == '1') active-sex @endif white user_male">Женский</span>
            </p>
        </div>
    </div>
    <div class="row">
        <a href="#">
            <button type="button" class="change_data">Изменить данные</button>
        </a>
    </div>
</div>
<div class="col-md-12 data3">
    <div class="row">
        <div class="col-md-3 col-sm-3 col-xs-4">
            <div class="row">
                <span>Ф.И.О</span>
            </div>
        </div>
        <div class="col-md-5 col-sm-5 col-xs-8">
            <input type="text" value="{{Auth::user()->name}}" id="profile_name">
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 col-sm-3 col-xs-4">
            <div class="row">
                <span>Телефон</span>
            </div>
        </div>
        <div class="col-md-5 col-sm-5 col-xs-8">
            <input type="text" value="{{Auth::user()->mobile_phone}}" id="profile_phone">
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 col-sm-3 col-xs-4">
            <div class="row">
                <span>E-mail</span>
            </div>
        </div>
        <div class="col-md-5 col-sm-5 col-xs-8">
            <input type="email" value="{{Auth::user()->email}}" id="profile_email">
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-sm-3 col-xs-3">
            <div class="row">
                <p>Пол</p>
            </div>
        </div>
        <div class="col-md-10 floor col-sm-9 col-xs-9">
            <input type="hidden" value="{{Auth::user()->sex}}" id="profile_hidden_sex">
            <p>
                <select name="preselected-select" class="standard-demo" id="profile_sex">
                    <option @if(Auth::user()->sex == '0') selected="selected" @endif value="0" >Мужской</option>
                    <option @if(Auth::user()->sex == '1') selected="selected" @endif value="1" >Женский</option>
                </select>
            </p>
        </div>
    </div>
    <div class="row">
        <a href="javascript:void(0)" onclick="closeProfileEdit()">
            <button type="button" class="change_data1">Назад</button>
        </a>
        <a href="javascript:void(0)" onclick="saveProfile()">
            <button type="button" class="change_data1">Сохранить</button>
        </a>
    </div>
</div>
<div class="col-md-12 data2">
    <div class="row">
        <div class="col-md-3 col-sm-3 col-xs-4">
            <div class="row">
                <span>Старый пароль</span>
            </div>
        </div>
        <div class="col-md-5 col-sm-5 col-xs-8">
            <input type="password" id="profile_password">
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 col-sm-3 col-xs-4">
            <div class="row">
                <span>Новый пароль</span>
            </div>
        </div>
        <div class="col-md-5 col-sm-5 col-xs-8">
            <input type="password" id="new_password">
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 col-sm-3 col-xs-4">
            <div class="row">
                <span>Повторите пароль</span>
            </div>
        </div>
        <div class="col-md-5 col-sm-5 col-xs-8">
            <input type="password" id="confirm_password">
        </div>
    </div>
    <div class="row">
        <a href="javascript:void(0)" onclick="closeProfileEdit()">
            <button type="button" class="change_data1">Назад</button>
        </a>
        <a href="javascript:void(0)" onclick="changePassword()">
            <button type="button" class="change_data1">Сохранить</button>
        </a>
    </div>
</div>