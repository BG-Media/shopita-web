<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class RequestProduct extends Model
{
    protected $table = 'request_product';
    protected $primaryKey = 'request_product_id';

    public function checkExistProductRequest($product_id,$request_id){
        $row = RequestProduct::where('product_id','=',$product_id)
            ->where('request_id','=',$request_id)
            ->first();
        return $row;
    }

    public function getSumByRequest($request_id){
        $row = RequestProduct::where('request_id','=',$request_id)
            ->select(DB::raw('sum(unit * price) as total_sum'))
            ->first();
        
        $total = 0;
        if($row != null){
            $total = $row->total_sum;
        }

        return $total;
    }

    public function getProductUnitByRequest($request_id){
        $row = RequestProduct::where('request_id','=',$request_id)
            ->select(DB::raw('sum(unit) as total_unit'))
            ->first();

        $total = 0;
        if($row != null){
            $total = $row->total_unit;
        }

        return $total;
    }

    public  function getAddress($id){
        return \DB::table('request')->where('request_id',$id)->first()->address;
    }
}
