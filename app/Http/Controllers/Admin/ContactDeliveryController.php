<?php

namespace App\Http\Controllers\Admin;

use App\Models\Delivery;
use App\Models\DeliveryKind;
use App\Models\DeliveryKindUser;
use App\Models\Subscriber;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Rubric;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use App\Helpers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use DB;
use View;

class ContactDeliveryController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');

        $kind = DeliveryKind::get();
        View::share('delivery_kind', $kind);
    }
    
    public function index(Request $request)
    {
        $search = '';
        if(isset($request->search)){
            $search = $request->search;
        }

        $paginator_count = 10;
        if(isset($request->list_count)){
            $paginator_count = $request->list_count;
        }

        $delivery_kind_id = -1;
        $delivery_cond = '!=';
        if(isset($request->delivery_kind_id) && $request->delivery_kind_id > 0){
            $delivery_kind_id = $request->delivery_kind_id;
            $delivery_cond = '=';
        }

            $row = Subscriber::LeftJoin('delivery_kind','delivery_kind.delivery_kind_id','=','subscriber.delivery_kind_id')
            ->where('subscriber.delivery_kind_id','>',0)
            ->where('subscriber.delivery_kind_id',$delivery_cond,$delivery_kind_id)
            ->where(function($query) use ($search){
                $query->where('email','like','%' .$search .'%')
                    ->orWhere('name','like','%' .$search .'%')
                    ->orWhere('info','like','%' .$search .'%');
            })
            ->orderBy('subscriber_id','desc')
            ->select('subscriber.subscriber_id',
                     'subscriber.name as name',
                     'subscriber.email',
                     'subscriber.phone',
                     'subscriber.info',
                     'delivery_kind.*',
                     DB::raw('DATE_FORMAT(subscriber.created_at,"%d.%m.%Y %H:%i") as date')
                    )
            //->union($row_first)
            ->paginate($paginator_count);

        return  view('admin.contact.contact-delivery',[
            'row' => $row,
            'request' => $request
        ]);
    }

    public function create(Request $request)
    {
        $row = new Subscriber();
        return  view('admin.contact.contact-delivery-edit', [
            'title' => 'Добавить контакт',
            'row' => $row,
            'request' => $request,
            'arr' => []
        ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required'
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return  view('admin.contact.contact-delivery-edit', [
                'title' => 'Добавить контакт',
                'row' => (object) $request->all(),
                'error' => $error[0],
                'arr' => []
            ]);
        }

        $ids = Subscriber::select('delivery_kind_id')->where('email', $request->email)->get();
        foreach ($ids as $id)
        {
            if ($id->delivery_kind_id == $request->delivery_kind_id)
            {
                return view('admin.contact.contact-delivery-edit', [
                    'title' => 'Добавить контакт',
                    'row' => (object) $request->all(),
                    'error' => 'Это рассылка уже используется',
                    'arr' => []
                ]);
            }
        }

        $subscriber = new Subscriber();
        $subscriber->email = $request->email;
        $subscriber->name = $request->name;
        $subscriber->phone = $request->phone;
        $subscriber->info = $request->info;
        $subscriber->delivery_kind_id = $request->delivery_kind_id;
        $subscriber->save();

        return redirect('/admin/contact-delivery');
    }

    public function edit($id)
    {
        $row = Subscriber::find($id);
        $subs = Subscriber::where('email', $row->email)->get();
        if ($subs)
        {
            $arr = [];
            foreach ($subs as $sub)
            {
                if ($sub->subscriber_id != $row->subscriber_id)
                {
                    $arr[] = $sub->delivery_kind_id;
                }
            }
        }
        
        return  view('admin.contact.contact-delivery-edit', [
            'title' => 'Изменить контакт',
            'row' => $row,
            'arr' => $arr
        ]);
    }

    public function show(Request $request,$id){

    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return  view('admin.contact.contact-delivery-edit', [
                'title' => 'Добавить контакт',
                'row' => (object) $request->all(),
                'error' => $error[0],
                'arr' => []
            ]);
        }

        $subscriber = Subscriber::find($id);
        $subscriber->email = $request->email;
        $subscriber->name = $request->name;
        $subscriber->phone = $request->phone;
        $subscriber->info = $request->info;
        $subscriber->delivery_kind_id = $request->delivery_kind_id;
        $subscriber->save();
        return redirect('/admin/contact-delivery');
    }

    public function destroy($id)
    {
        $subscriber = Subscriber::find($id);
        $subscriber->delete();
    }
}
