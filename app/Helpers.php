<?php

namespace App;
use Mockery\Exception;
use Stichoza\GoogleTranslate\TranslateClient;
use Auth;
use Illuminate\Support\Facades\DB;

class Helpers {

    public static function getTranslatedSlugRu($text)
    {
        $cyr2lat_replacements = array (
            "А" => "a","Ә" => "a", "Б" => "b","В" => "v","Г" => "g","Ғ" => "gh","Д" => "d",
            "Е" => "e","Ё" => "yo","Ж" => "dg","З" => "z","И" => "i","І" => "i",
            "Й" => "y","К" => "k","Қ" => "q","Һ" => "q","Л" => "l","М" => "m","Н" => "n","Ң" => "nh",
            "О" => "o","Ө" => "o","П" => "p","Р" => "r","С" => "s","Т" => "t",
            "У" => "u","Ұ" => "u","Ү" => "u","Ф" => "f","Х" => "kh","Ц" => "ts","Ч" => "ch",
            "Ш" => "sh","Щ" => "csh","Ъ" => "","Ы" => "y","Ь" => "",
            "Э" => "e","Ю" => "yu","Я" => "ya","?" => "",

            "а" => "a","ә" => "a","б" => "b","в" => "v","г" => "g","ғ" => "gh","д" => "d",
            "е" => "e","ё" => "yo","ж" => "dg","з" => "z","и" => "i","і" => "i",
            "й" => "y","к" => "k","қ" => "q","һ" => "q","л" => "l","м" => "m","н" => "n","ң" => "nh",
            "о" => "o","ө" => "o","п" => "p","р" => "r","с" => "s","т" => "t",
            "у" => "u","ұ" => "u","ү" => "u","ф" => "f","х" => "kh","ц" => "ts","ч" => "ch",
            "ш" => "sh","щ" => "sch","ъ" => "","ы" => "y","ь" => "",
            "э" => "e","ю" => "yu","я" => "ya",
            "(" => "", ")" => "", "," => "", "." => "",

            "-" => "-","%" => "-"," " => "-", "+" => "", "®" => "", "«" => "", "»" => "", '"' => "", "`" => "", "&" => "","/" => "-"
        );

        return strtr (trim($text),$cyr2lat_replacements);
    }

    public static function getSessionLang(){
        $lang = 'ru';
        if (isset($_COOKIE['site_lang'])) {
            $lang = $_COOKIE['site_lang'];
        }
        return $lang;
    }

    public static function getIdFromUrl($url){
        $url = strrev($url);
        $id = strstr($url,'u',true);
        $id = strrev($id);
        return $id;
    }

    public static function getUserId(){
        $user_id = 0;
        if (Auth::check()) {
            $user_id = Auth::user()->user_id;
        }
        return $user_id;
    }
    public static function getUserEmail(){
        $user_id = 0;
        if (Auth::check()) {
            $email = Auth::user()->email;
        }
        return $email;
    }

    public static function send_mime_mail($name_from, // имя отправителя
                            $email_from, // email отправителя
                            $name_to, // имя получателя
                            $email_to, // email получателя
                            $data_charset, // кодировка переданных данных
                            $send_charset, // кодировка письма
                            $subject, // тема письма
                            $body // текст письма
    ) 
    {
        $to = Helpers::mime_header_encode($name_to, $data_charset, $send_charset)
            . ' <' . $email_to . '>';
        $from = Helpers::mime_header_encode($name_from, $data_charset, $send_charset)
            .' <' . $email_from . '>';

        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $headers .= "From: $from\r\n";

        return mail($to, $subject, $body, $headers);
    }

    public static function mime_header_encode($str, $data_charset, $send_charset) {
        if($data_charset != $send_charset) {
            $str = iconv($data_charset, $send_charset, $str);
        }
        return '=?' . $send_charset . '?B?' . base64_encode($str) . '?=';
    }

    public static function getSessionSex(){
        $sex = 0;
        if (isset($_COOKIE['site_sex'])) {
            $sex = $_COOKIE['site_sex'];
        }
        return $sex;
    }

    public static function replaceGetUrl($param) {
        $parsed = parse_url("http://example?" .http_build_query($_GET));
        $query = '';
        if(isset($parsed['query'])){
            $query = $parsed['query'];
        }

        parse_str($query, $new_url);
        $param = explode(',', $param);
        $new_val = '';

        foreach($param as $key => $value){
            if($key % 2 == 0){
                unset($new_url[$value]);
                $new_val = $value;
            }
            else {
                $new_url[$new_val] = $value;
            }
        }

        $string = http_build_query($new_url);
        return $string;
    }

    public static  function getResetProduct(){
        $parsed = parse_url("http://example?" .http_build_query($_GET));
        $query = '';
        if(isset($parsed['query'])){
            $query = $parsed['query'];
        }

        parse_str($query, $new_url);
        $param = explode('&', $query);
        return $param[0];
    }

    public  static function getBrandTitle($id){
        $brand = \DB::table('brand')->where('brand_id',$id)->first();

        return $brand->brand_name;
    }


} 