<?php

namespace App\Http\Controllers\Admin;

use App\Models\Delivery;
use App\Models\DeliveryKind;
use App\Models\Follower;
use App\Models\News;
use App\Models\Subscriber;
use App\Models\Users;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Rubric;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use App\Helpers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use View;

class DeliveryController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');

        $kind = DeliveryKind::where('delivery_kind_id','!=',1)->get();
        View::share('delivery_kind', $kind);
    }
    
    public function index()
    {
        $row = Delivery::orderBy('delivery_id','desc')
            ->select('delivery.*',
                DB::raw('DATE_FORMAT(delivery.created_at,"%d.%m.%Y") as date'))
            ->get();
        return  view('admin.delivery.delivery',[
            'row' => $row
        ]);
    }

    public function create()
    {
        $row = new Rubric();
        return  view('admin.delivery.delivery-edit', [
                'title' => 'Добавить рассылку',
                'row' => $row
            ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'title' => 'required',
            'delivery_text' => 'required'
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return  view('admin.delivery.delivery-edit', [
                'title' => 'Добавить рассылку',
                'row' => (object) $request->all(),
                'error' => $error[0]
            ]);
        }

        $delivery = new Delivery();
        $delivery->name = $request->name;
        $delivery->title = $request->title;
        $delivery->delivery_text = $request->delivery_text;
        $delivery->save();
        return redirect('/admin/delivery');
    }

    public function edit($id)
    {
        $row = Delivery::find($id);
        return  view('admin.delivery.delivery-edit', [
            'title' => 'Изменить рубрику',
            'row' => $row
        ]);
    }

    public function sendDeliveryShow($id)
    {
        if($id == 0){
            $row = new Delivery();
        }
        else {
            $row = Delivery::find($id);
        }


        return  view('admin.delivery.delivery-send', [
            'title' => 'Отправить рассылку',
            'row' => $row
        ]);
    }

    public function sendDelivery(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'delivery_kind'=>'required'
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return  view('admin.delivery.delivery-send', [
                'title' => 'Отправить рассылку',
                'row' => (object) $request->all(),
                'error' => $error[0]
            ]);
        }
        $delivery = new Delivery();
        $delivery->title = $request->title;
        $delivery->delivery_text = $request->delivery_text;
        $delivery->save();
        $delivery_kind_id = $request->delivery_kind;
        $emails = DB::table('subscriber')->whereIn('delivery_kind_id',$delivery_kind_id)->groupBy('email')->pluck('email');
        foreach ($emails as $email){
            \Mail::send('admin.message', ['data' => $delivery,'email'=>$email], function($m) use ($delivery,$email) {
                $m->from(env('MAIL_USERNAME'), 'Shopita.kz');
                $m->to($email , 'Nuraga')->subject('Shopita.kz');
            });
        }

        return redirect('/admin/delivery');
    }

    public function show(Request $request,$id){

    }

    public function update(Request $request,$id)
    {

    }

    public function destroy($id)
    {
        $delivery = Delivery::find($id);
        $delivery->delete();
    }
}
