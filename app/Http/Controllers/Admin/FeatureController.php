<?php

namespace App\Http\Controllers\Admin;

use App\Models\Feature;
use App\Models\Item;
use App\Models\Kind;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use App\Helpers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use View;

class FeatureController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');

        $feature_kind = Kind::where('kind_code','feature')->orderBy('kind_name','asc')->get();
        View::share('feature_kind', $feature_kind);
    }
    
    public function index(Request $request)
    {
        $search = '';
        if(isset($request->search)){
            $search = $request->search;
        }
        
        
        $row = Feature::leftJoin('kind','kind.kind_id','=','feature.kind_id')
                ->orderBy('feature_id','desc')
                ->where(function($query) use ($search){
                    $query->where('feature_name_ru','like','%' .$search .'%');
                })
                ->select('feature.*',
                         'kind.kind_name')
                ->paginate(10);

        return  view('admin.feature.feature',[
            'row' => $row,
            'request' => $request
        ]);
    }

    public function create()
    {
        $row = new Feature();

        return  view('admin.feature.feature-edit', [
                'title' => 'Добавить характеристику',
                'row' => $row
            ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'feature_name_ru' => 'required'
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return  view('admin.feature.feature-edit', [
                'title' => 'Добавить характеристику',
                'row' => (object) $request->all(),
                'error' => $error[0]
            ]);
        }

        $feature = new Feature();
        $feature->feature_name_ru = $request->feature_name_ru;
        $feature->feature_desc = $request->feature_desc;
        $feature->visual_kind = $request->visual_kind;
        $feature->kind_id = $request->kind_id;
        $feature->sort_num = $request->sort_num;
        $feature->save();
        
        $feature_item = Item::where('item_token',$request->_token)->get();
        
        foreach($feature_item as $val){
            $item = Item::find($val->item_id);
            $item->feature_id = $feature->feature_id;
            $item->item_token = null;
            $item->save();
        }
        
        return redirect('/admin/feature');
    }

    public function edit($id)
    {
        $row = Feature::find($id);
        return  view('admin.feature.feature-edit', [
            'title' => 'Изменить характеристику',
            'row' => $row
        ]);
    }

    public function show(Request $request,$id){

    }

    public function update(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'feature_name_ru' => 'required'
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return  view('admin.feature.feature-edit', [
                'title' => 'Изменить характеристику',
                'row' => (object) $request->all(),
                'error' => $error[0]
            ]);
        }

        $feature = Feature::find($id);
        $feature->feature_name_ru = $request->feature_name_ru;
        $feature->feature_desc = $request->feature_desc;
        $feature->visual_kind = $request->visual_kind;
        $feature->kind_id = $request->kind_id;
        $feature->sort_num = $request->sort_num;
        $feature->save();
        return redirect('/admin/feature');
    }

    public function destroy($id)
    {
        $feature = Feature::find($id);
        $feature->delete();
    }

    public function getItemListByFeature(Request $request){
        $feature = null;
        if($request->feature_id == 0 || $request->feature_id == ''){
            $feature_cond = 'item_token';
            $feature_id = $request->item_token;
        }
        else {
            $feature_cond = 'feature_id';
            $feature_id = $request->feature_id;
            $feature = Feature::find($feature_id);
        }

        $item = Item::where($feature_cond,'=',$feature_id)
            ->orderBy('item_id', 'desc')
            ->get();

        if(isset($request->is_category)){
            return  view('admin.category.feature-item-loop',[
                'item' => $item,
                'feature' => $feature,
                'category_id' => $request->category_id
            ]);
        }
        else if(isset($request->is_product_feature_loop)){
            return  view('admin.product.feature-item-loop',[
                'item' => $item,
                'feature' => $feature,
                'product_id' => $request->product_id
            ]);
        }
        else {
            return  view('admin.feature.feature-item-loop',[
                'item' => $item
            ]);
        }

    }

    public function deleteItem(Request $request){
        Item::where('item_id','=',$request->item_id)->delete();
        $result['success'] = true;
        return response()->json($result);
    }

    public function addItem(Request $request){
        $item = new Item();
        $item->item_name_ru = $request->item_name;
        $item->item_desc = $request->item_desc;
        if($request->feature_id > 0){
            $item->feature_id = $request->feature_id;
        }
        else {
            $item->item_token = $request->item_token;
        }

        $item->save();

        $result['success'] = true;
        return $result;
    }

    public function changeItem(Request $request,$item_id){
        $item = Item::find($item_id);
        $item->item_name_ru = $request->item_name;
        $item->item_desc = $request->item_desc;
        $item->save();

        $result['success'] = true;
        return $result;
    }
}
