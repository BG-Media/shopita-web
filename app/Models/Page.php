<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Page extends Model
{
    protected $table = 'page';
    protected $primaryKey = 'page_id';

    use SoftDeletes;
    protected $dates = ['deleted_at'];
    public $appends = ['share_link', 'preview'];

    public function getShareLinkAttribute() {
        return 'http://shopita.kz/blog/'.$this->page_url.'-u'.$this->page_id;
    }

    public function getPreviewAttribute() {
        return 'http://shopita.kz/image/blog/'.$this->page_image;
    }
}
