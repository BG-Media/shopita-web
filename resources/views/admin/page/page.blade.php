@extends('layout.admin-layout')

@section('content')

<div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title" style="margin-top: 5px">Статичные страницы</h3>
          <a href="/admin/page/create" style="float: right">
               <button class="btn btn-primary" style="margin-bottom: 10px; ">Добавить новую страницу</button>
           </a>
        </div>

        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr style="border: 1px">
                <th style="width: 30px">№</th>
                <th>Название</th>
                <th style="width: 30px"></th>
                <th style="width: 30px"></th>
              </tr>
            </thead>

            <tbody>

                  @foreach($row as $key => $val)

                     <tr>
                        <td> {{ $key + 1 }}</td>
                        <td>
                         <a href="/page/{{$val->page_url}}-u{{$val->page_id}}" target="_blank" class="redirect-url">
                             {{ $val->page_name_ru }}
                         </a>
                        </td>
                        <td style="text-align: center">
                            <a href="javascript:void(0)" onclick="delItem(this,{{ $val->page_id }})">
                                <li class="fa fa-trash-o" style="font-size: 20px; color: red;"></li>
                            </a>
                        </td>
                        <td style="text-align: center">
                            <a href="/admin/page/{{ $val->page_id }}/edit">
                                <li class="fa fa-pencil" style="font-size: 20px;"></li>
                            </a>
                        </td>
                     </tr>

                  @endforeach

            </tbody>

          </table>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
    </div><!-- /.row -->

    <meta name="csrf-token" content="{{ csrf_token() }}">

       <script>
           function delItem(ob,id){
               if(confirm('Действительно хотите удалить?')){
                   $(ob).closest('tr').remove();
                   $.ajax({
                       type: 'DELETE',
                       headers: {
                           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                       },
                       url: "/admin/page/" + id,
                       success: function(data){

                       }
                   });
               }
           }
       </script>

@endsection