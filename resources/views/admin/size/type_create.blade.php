@extends('layout.admin-layout')
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3>Общий название размеров</h3>
                </div>
                <div style="padding-top: 10px; border-top: 1px solid rgb(127, 127, 127);"></div>
            </div>
                <form action="/admin/size/type/store" method="POST">
                {{method_field('POST')}}
                {{csrf_field()}}
                <div class="box">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="form-froup">
                                <label for="allName">Заголовок</label>
                                <input id="allName" type="text" class="form-control" name="all_name">
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="box">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2>Тип размеров</h2>
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Тип</th>
                                    <th>Редактирование</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($type as $key => $item)
                                        <tr>
                                            <td>{{$item->type}}</td>
                                            <td><input type="checkbox" value="{{$item->id}}" id="{{$item->id}}" name="typeSize[]"></td>
                                        </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <br>
                            <a href="#" style="text-align: right;">
                                <button class="btn btn-primary" style="margin-bottom: 10px; ">Сохранить</button>
                            </a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection