<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Models\Product;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use App\Helpers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Carbon\Carbon;
use DB;

class BannerController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    public function index(Request $request)
    {
        $search = '';
        if(isset($request->search)){
            $search = $request->search;
        }

        $active = 0;

        if(!isset($request->active)){
            $active = 1;
            $request->active = 1;
        }

        $row = Page::orderBy('page_id','desc')
                ->where('page_kind',3)
                ->where('is_show','=',$active)
                ->where(function($query) use ($search){
                    $query->where('page_name_ru','like','%' .$search .'%');
                })
                ->paginate(10);

        return  view('admin.banner.banner',[
            'row' => $row,
            'request' => $request
        ]);
    }

    public function create()
    {
        $row = new Page();
        $row->page_image = 'default.jpg';

        return  view('admin.banner.banner-edit', [
                'title' => 'Добавить баннер',
                'row' => $row
            ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'page_name_ru' => 'required',
            'page_text_ru' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return  view('admin.banner.banner-edit', [
                'title' => 'Добавить баннер',
                'row' => (object) $request->all(),
                'error' => $error[0]
            ]);
        }

        $page = new Page();
        $page->page_name_ru = $request->page_name_ru;
        $page->page_text_ru = $request->page_text_ru;
        $page->page_image = $request->page_image;
        $page->page_position = $request->page_position;
        $page->page_kind = 3;
        $page->page_url = Helpers::getTranslatedSlugRu($request->page_name_ru);
        $page->save();
        return redirect('/admin/banner');
    }

    public function edit($id)
    {
        $row = Page::find($id);
        return  view('admin.banner.banner-edit', [
            'title' => 'Изменить баннер',
            'row' => $row
        ]);
    }

    public function show(Request $request,$id){

    }

    public function update(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'page_name_ru' => 'required',
            'page_text_ru' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return  view('admin.banner.banner-edit', [
                'title' => 'Изменить баннер',
                'row' => (object) $request->all(),
                'error' => $error[0]
            ]);
        }

        $page = Page::find($id);
        $page->page_name_ru = $request->page_name_ru;
        $page->page_text_ru = $request->page_text_ru;
        $page->page_image = $request->page_image;
        $page->page_position = $request->page_position;
        $page->page_url = Helpers::getTranslatedSlugRu($request->page_name_ru);
        $page->save();
        return redirect('/admin/banner');
    }

    public function destroy($id)
    {
        $page = Page::find($id);
        $page->delete();
    }

    public function uploadImage(Request $request){
        $file = $request->image;
        $file_name = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();

        if(substr($file->getClientMimeType(), 0, 5) != 'image') {
            $result['error'] = 'Загружайте только файлы форматов JPEG, PNG';
            $result['success'] = false;
            return $result;
        }

        if(Storage::disk('banner')->exists($file_name)){
            $file_name = date("YmdHisu").'.'.$extension;;
        }

        Storage::disk('banner')->put($file_name,  File::get($file));
        $result['success'] = true;
        $result['file_name'] = $file_name;
        return $result;
    }

    public function changeIsShow(Request $request){
        $user = Page::find($request->id);
        $user->is_show = $request->is_show;
        $user->save();
    }

    public function getStatus()
    {
        $profitCurrentMonth = DB::table('request')
            ->join('request_product', 'request.request_id', '=', 'request_product.request_id')
            ->where([
//                ['created_at', '>=', Carbon::now()->startOfMonth()],
                ['is_epay_success', '1']
            ])
            ->sum(DB::raw('request_product.price * unit'));

        $salesLeader = DB::table('request')
            ->join('request_product', 'request.request_id', '=', 'request_product.request_id')
            ->where([
//                ['created_at', '>=', Carbon::now()->startOfMonth()],
                ['is_epay_success', '1']
            ])
            ->get();

        $query =   DB::table('request_product')->rightJoin('request as r','request_product.request_id','=','r.request_id')
            ->where('r.is_epay_success',1)
            ->get(['request_product.product_id','request_product.unit']);
        $arr = array();
        foreach ($query as $i=>$item) {
            if (array_key_exists($item->product_id, $arr)){
                $arr[$item->product_id] = $arr[$item->product_id] +$item->unit;
            }else{
                $arr[$item->product_id] = $item->unit;
            }

        }
        $value = max($arr);
        $getIdsProduct =
            array_filter($arr, function($element) use($value){
                return isset($element) && $element == $value;
            });
        $products = [];
        foreach ($getIdsProduct as $key=>$item) {
            array_push($products, Product::find($key));
        }

//        dd($getIdsProduct);
//        dd($products);

        $numberOrders = DB::table('request')
            ->where([
                ['status_id', 2],
                ['created_at', '>=', Carbon::now()->startOfMonth()],
            ])
            ->count();

        $numberProducts = DB::table('product')
            ->where('product_count', '<=', 5)
            ->count();

        $quantity = DB::table('product')
            ->where('product_count', 0)
            ->count();

        return view('admin.status-of-sale', compact('profitCurrentMonth','getIdsProduct', 'products', 'numberOrders', 'numberProducts', 'quantity'));
    }
}
