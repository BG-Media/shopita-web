<?
$var = isset($_COOKIE['mob']);
if($var==true)
$cache =$_COOKIE['mob'];

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
        @if(($var == true) && $cache == 1  || $var == false)
        <meta name="viewport" content="width=device-width, initial-scale=1">
        @endif
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Интернет-магазин итальянской одежды shopita</title>
    <!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/font-awesome-4.5.0/css/font-awesome.css">
    <link rel="stylesheet" href="/css/tab.css">
    <link rel="stylesheet" href="/Slick/slick.css">
    <link rel="stylesheet" href="/Slick/slick-theme.css">
    <link rel="stylesheet" href="/css/horizontal_tabs.css">
    <link rel="stylesheet" type="text/css" href="/css/jquery.gritter.css" />
  </head>

<body onload="load()">

@if(!isset($sex))
    <?$sex = '';?>
@endif

<div class="header">
    <div class="container-fluid">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-5 top-header">
                        <div class="row">
                            <div class="top clearfix">
                                <div class="col-md-6 col-sm-6 ">
                                    <div class="row logotype">
                                        <a href="/">
                                            <img src="/image/logo.png" class="img-responsive" alt="">
                                        </a>
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-6 main_link ">
                                    <a href="javascript:void(0)" onclick="setSex(1)" class=" @if($sex == '1') active @endif">Мужчинам</a>
                                    <a href="javascript:void(0)" onclick="setSex(0)" class=" @if($sex == '0') active @endif">Женщинам</a>
                                </div>
                            </div>

                            <div class="line1 hidden-xs"></div>
                        </div>

                    </div>

                    <div class="col-md-offset-4 col-md-4 col-sm-6 col-xs-7">
                        <div class="row Left_header">
                            <div class="top clearfix">
                                <a href="/profile?page_tab=4">
                                    <img src="/image/Vector-Smart-Object.png" alt=""><span class="hidden-xs" id="favorite_count">&nbsp; &nbsp; @if(!Auth::check()) 0 @else {{\App\Models\ProductFavorite::where('user_id',Auth::user()->user_id)->count()}} @endif</span>
                                </a>
                                <a href="/basket">
                                    <img src="/image/Vector-Smart-Object1.png" alt=""><span class="hidden-xs" id="basket_count">&nbsp; &nbsp; <? $basket_count = new \App\Models\Request(); echo $basket_count->getProductCountInBasket();?></span>
                                </a>

                                @if(!Auth::check())
                                    <a href="#openModal">Вход</a>
                                @else
                                    <a href="/profile">Профиль</a>
                                    <a href="/logout">Выйти</a>
                                @endif



                            </div>
                            <div class="line2 hidden-xs"></div>
                        </div>
                    </div>
                </div>

                <div class="row megaMenu ">

                    <div class="col-md-8">
                        <div class="row">
                            <div class="menu">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>

                                @include('layout.category')

                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="row main_poisk">
                            <form action="/search" method="get" >
                                <input type="text" name="query" placeholder="Например: красная кофта со скидкой" class="poisk">
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

@yield('content')

<div class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-9">
                        <div class="row footer_menu">
                            <div class="col-md-4 col-sm-4 col-xs-6">
                                <p>Помощь</p>
                                <ul>
                                    <li><a href="/page/kak-sdelat-zakaz-u1">Как сделать заказ?</a></li>
                                    <li><a href="/page/kak-oplatit-zakaz-u3">Как оплатить заказ?</a></li>
                                    <li><a href="/page/usloviya-dostavki-u4">Условия доставки</a></li>
                                    <li><a href="/page/tablitsa-razmerov-u48">Таблица размеров</a></li>
                                    <li><a href="/page/vozvrat-u5">Возврат</a></li>
                                </ul>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-6">
                                <p>О нас</p>
                                <ul>
                                    <li><a href="/page/o-nas-u7"> О магазине </a></li>
                                    <li><a href="/page/kontakty-u6">Контакты</a></li>
                                </ul>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <p>Политика</p>
                                <ul>
                                    <li><a href="/page/publichnaya-oferta-u8">Публичная оферта</a></li>
                                    <li><a href="/page/konfidentsialnost-u9"> Конфиденциальность</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-3">
                        <div class="right_footer">
                            <a target="_blank" href="https://bugingroup.kz/">
                                <img src="/image/bg_logo.png" class="img-responsive" alt="">
                            </a>
                            <br>
                            <p>Shopita.kz © 2016</p>
                            <div id="mobile">
                                @if(isset($cache) && $cache == 1  || $var == false)
                                    <a href="/mobile?mobile=0" class="mobile" ><p>Версия для ПК</p></a>
                                @else
                                    <a href="/mobile?mobile=1" class="mobile" ><p>Мобильная версия</p></a>
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('layout.modal')

<i class="ajax-loader"></i>
<input type="hidden" value="@if(isset($_GET['url'])){{$_GET['url']}}@else /profile @endif" id="url"/>

@if(isset($menu) && $menu != 'product-detail')
    <script src="/js/tab.js"></script>
    <script src="/js/jquery_1.11.0.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/Slick/slick.js"></script>
@endif



<script src="/js/jquery.gritter.js" type="text/javascript"></script>
<script src="/js/jquery.maskedinput.js"></script>
<script type="text/javascript" src="/js/cookie.js"></script>
<script>
    $('.single-item').slick({
        fade: true,
        autoplay: true,
        autoplaySpeed: 2000,
        cssEase: 'linear',
        dots: false,
        arrows: false
    });
</script>

<script>
    $(document).ready(function() {
        $(".change_data").click(function() {
            $(".data3").css("display", "block");
            $(".data").css("display", "none");
            $(".change_code").css("display", "none");
        });
        $(".change_code").click(function() {
            $(".data2").css("display", "block");
            $(".data").css("display", "none");
            $(".change_code").css("display", "none");
        });

        var Timer = setInterval(function() {
            getBasketCount();
        }, 10000);
    });
</script>
<script>
    $('.standard-demo').select2Buttons();
    $('select[name=js-callback-select]').change(function() {
        alert('Changed to ' + $(this).val());
    });
    $('select[name=no-default-select]').select2Buttons({
        noDefault: true
    });
</script>

<script>
    $("#register_phone").mask("+7(999)999-99-99");
    $("#profile_phone").mask("+7(999)999-99-99");
    $("#order_phone").mask("+7(999)999-99-99");

    function setSex(sex){
        document.cookie = "site_sex=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
        document.cookie = "site_sex=" + sex + "; path=/";
        var str = window.location.href;
        str = str.replace('#','');
        document.location.href = str;
    }

    function showError(message){
        $.gritter.add({
            title: 'Ошибка',
            text: message
        });
        return false;
    }

    function showMessage(message){
        $.gritter.add({
            title: 'Успех',
            text: message,
            class_name: 'success-gritter'
        });
        return false;
    }

    $( "#login_password" ).keyup(function(event) {
        if (!event.ctrlKey && event.which == 13) {
            login();
        }
    });

    function login() {
        $('input').removeClass('error');
        $('.ajax-loader').fadeIn(100);
        $.ajax({
            url:'/login',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                email: $('#login_email').val(),
                password: $('#login_password').val(),
            },
            success: function (data) {
                $('.ajax-loader').fadeOut(100);
                if(data.success == 0) {
                    showError(data.error);
                    $(data.error_list).each(function(){
                        if(this[Object.keys(this)] != ""){
                            $('#' + Object.keys(this)).addClass('error');
                        }
                    });
                    return;
                }
                window.location.href = $('#url').val();
            }
        });
    }

    $( "#confirm_password" ).keyup(function(event) {
        if (!event.ctrlKey && event.which == 13) {
            registerUser();
        }
    });

    function registerUser() {
        $('input').removeClass('error');
        $('.ajax-loader').fadeIn(100);
        $.ajax({
            url:'/register',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                name: $('#register_name').val(),
                password: $('#register_password').val(),
                confirm_password: $('#confirm_password').val(),
                email: $('#register_email').val(),
                phone: $('#register_phone').val()
            },
            success: function (data) {
                $('.ajax-loader').fadeOut(100);
                if(data.success == 0){
                    showError(data.error);
                    $(data.error_list).each(function(){
                        if(this[Object.keys(this)] != ""){
                            $('#' + Object.keys(this)).addClass('error');
                        }
                        return;
                    });
                    return;
                }
                showMessage(data.message);
                window.location.href = 'profile';
            }
        });
    }

    $( "#reset_email" ).keyup(function(event) {
        if (!event.ctrlKey && event.which == 13) {
            resetPassword();
        }
    });

    function resetPassword() {
        $('.ajax-loader').fadeIn(100);
        $('input').removeClass('error');
        $.ajax({
            url:'/reset-password',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                email: $('#reset_email').val()
            },
            success: function (data) {
                $('.ajax-loader').fadeOut(100);
                if(data.success == 0){
                    showError(data.error);
                    $(data.error_list).each(function(){
                        if(this[Object.keys(this)] != ""){
                            $('#' + Object.keys(this)).addClass('error');
                        }
                    });
                    return;
                }
                showMessage(data.message);
            }
        });
    }

    $( "#subscribe_email" ).keyup(function(event) {
        if (!event.ctrlKey && event.which == 13) {
            subscribeNews();
        }
    });

    function subscribeNews() {
        $('input').removeClass('error');
        $.ajax({
            url:'/subscribe',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                email: $('#subscribe_email').val()
            },
            success: function (data) {
                if(data.success == 0){
                    showError(data.error);
                    $(data.error_list).each(function(){
                        if(this[Object.keys(this)] != ""){
                            $('#' + Object.keys(this)).addClass('error');
                        }
                        return;
                    });
                    return;
                }
                showMessage(data.message);
            }
        });
    }

    $('#profile_sex').change(function(){
       var val = $(this).val();
       $('#profile_hidden_sex').val(val);
    });

    function saveProfile() {
        $('input').removeClass('error');
        $('.ajax-loader').fadeIn(100);
        $.ajax({
            url:'/profile',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                name: $('#profile_name').val(),
                email: $('#profile_email').val(),
                phone: $('#profile_phone').val(),
                sex: $('#profile_hidden_sex').val()
            },
            success: function (data) {
                $('.ajax-loader').fadeOut(100);
                if(data.success == 0){
                    showError(data.error);
                    $(data.error_list).each(function(){
                        if(this[Object.keys(this)] != ""){
                            $('#' + Object.keys(this)).addClass('error');
                        }
                        return;
                    });
                    return;
                }
                showMessage(data.message);
                $('#user_name').html($('#profile_name').val());
                $('#user_email').html($('#profile_email').val());
                $('#user_phone').html($('#profile_phone').val());
                sex = $('#profile_hidden_sex').val();
                $('.user_male').removeClass('active-sex');
                $('#user_male_' + sex).addClass('active-sex');
                closeProfileEdit();
            }
        });
    }

    function closeProfileEdit(){
        $(".data2").css("display", "none");
        $(".data3").css("display", "none");
        $(".data").css("display", "block");
        $(".change_code").css("display", "block");
    }

    function changePassword() {
        $('input').removeClass('error');
        $('.ajax-loader').fadeIn(100);
        $.ajax({
            url:'/password',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                old_password: $('#profile_password').val(),
                new_password: $('#new_password').val(),
                confirm_password: $('#confirm_password').val()
            },
            success: function (data) {
                $('.ajax-loader').fadeOut(100);
                if(data.success == 0){
                    showError(data.error);
                    $(data.error_list).each(function(){
                        if(this[Object.keys(this)] != ""){
                            $('#' + Object.keys(this)).addClass('error');
                        }
                        return;
                    });
                    return;
                }
                showMessage(data.message);
                $('#profile_password').val('');
                $('#new_password').val('');
                $('#confirm_password').val('');
                closeProfileEdit();
            }
        });
    }

    function changeFavoriteProduct(ob,product_id) {
        $('.ajax-loader').fadeIn(100);
        $.ajax({
            url:'/product/favorite',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                product_id: product_id,
                url: '{{$_SERVER['REQUEST_URI']}}'
            },
            success: function (data) {
                $('.ajax-loader').fadeOut(100);
                if(data.success == 0){
                    showError(data.error);
                    return;
                }
                else {
                    showMessage(data.message);
                }
                $(ob).find('img').attr('src',data.image_url);
                getFavoriteProductCount();
            }
        });
    }

    function getFavoriteProductCount() {
        $.ajax({
            url:'/favorite-count',
            type: 'GET',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                $('#favorite_count').html('&nbsp; &nbsp; ' + data.count);
            }
        });
    }

    function getBasketCount() {
        $.ajax({
            url:'/basket-count',
            type: 'GET',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                $('#basket_count').html('&nbsp; &nbsp; ' + data.count);
                $('.basket_count').html(data.count);
                if(data.count == 0){
                    $('#ordering').fadeOut(100);
                }
            }
        });
    }

</script>
@if(isset($_GET['modal']))
<script>

    $(document).ready(function () {

        @if(isset($_GET['modal_msg']))
            $('#{{$_GET['modal']}}').find('.modal_msg').html('<?=$_GET['modal_msg'];?>')
        @endif

        $('#{{$_GET['modal']}}').modal(100);
    });

</script>

@endif



@stack('scripts')
<script>
    $('.change_data').click(function () {
        var form = $('#subscription');
            $.ajax( {
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/subscription-profile',
                data: form.serialize(),dat :'dde',
                success: function( response ) {
                    console.log(response);
                }
            } );
    });
</script>
</body>

</html>


<style>
    .tab_1 li.active a {
        text-decoration: underline;
    }
    .pagination > li.active-pagination a {
        z-index: 2;
        color: #fff;
        background-color: #d74f4b;
    }
    .ajax-loader {
        background-image: url("/image/ajax-loader.gif");
        background-size: 60px 60px;
        border-radius: 60px;
        display: none;
        height: 60px;
        left: 50%;
        margin-left: -30px;
        margin-top: -30px;
        position: fixed;
        top: 50%;
        width: 60px;
        z-index: 100000;
    }
    input.error {
        border: 1px solid red;
    }
    .modalDialog {
        z-index: 9999;
    }
    a.active-label {
        /*color: #d04d4a !important;*/
        text-decoration: underline !important;
    }
    .active-sex {
        color: #fff !important;
        background-color: #72b778 !important;
    }
    .success-gritter {
        background: #72b778 none repeat scroll 0 0 !important;
    }
    .profile .tab-pane div {
        margin-bottom: 0;
    }
    .content_basket .submits2.active {
        display: inline;
    }
    .modalDialog2 > div {
        pointer-events: auto;
    }
    .gritter-item input[type="button"] {
        border: medium none;
        border-radius: 3px;
        margin-top: 4px;
        padding: 1px 10px;
    }
</style>