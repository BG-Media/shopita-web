<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Feature extends Model
{
    protected $table = 'feature';
    protected $primaryKey = 'feature_id';

    use SoftDeletes;
    protected $dates = ['deleted_at'];
}
