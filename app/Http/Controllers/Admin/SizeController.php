<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Auth\Access\Response;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class SizeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $name= DB::table('size_name')->get();

        return view('admin.size.index',compact('name'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $name = DB::table('size_name')->get();
        return view('admin.size.size-edit',compact('name'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'size_title'=>'required',
            'size_type_id'=>'required',
            'size_name_id'=>'required'
        ]);
        $size_type_id = $request['size_type_id'];
        $size_name_id = $request['size_name_id'];
        $unique_size = $size_name_id[0];
        for($i=0;$i<count($size_type_id);$i++){
            DB::table('size')->insert([
                'size_name_id'=>$size_name_id[$i],
                'size_type_id'=>$size_type_id[$i],
                'unique_size' =>$unique_size,
                'size_name_title'=>$request['size_title']
            ]);
        }

        return redirect('admin/size');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $type = DB::table('size_name_type')
            ->where('name_size_id',$id)
            ->Leftjoin('size_type','size_name_type.type_size_id','=','size_type.id')
            ->get();
        $size = DB::table('size')
            ->where('size_name_title',$id)
            ->join('size_name','size.size_name_title','=','size_name.id')
            ->get();
        $count = DB::table('size_name_type')
            ->where('name_size_id',$id)
            ->count('*');
        //dd($size);
        return view('admin.size.show',compact('type','size','count'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('size')->where('unique_size',$id)->delete();
        return redirect()->back();
    }

    public function sizeCat(Request $request){
            $data="";
            $size_cat = $request['size_cat'];
            $option = DB::table('size_name_type')
                ->where('name_size_id',$size_cat)
                ->join('size_type','size_name_type.type_size_id','=','size_type.id')
                ->get(['size_type.id','size_type.type']);

        if($option){
            foreach ($option as $key=>$item){
                $data.='<div class="form-group">'.
                    '<p>Тип размера : '.$item->type.'</p>'.
                    '<input type="hidden" value="'.$item->id.'" name="size_type_id[]">'.
                    '<input type="text" name="size_name_id[]" class="form-control">'.
                    '</div>';
            }
        }
        return response()->json($data);
    }

    public function sizeValue(Request $request){

        $size_value = $request['size_val'];
           $size =  DB::table('size')
                ->where('size_name_title',$size_value)
               ->where('size_type_id',2)
               ->join('size_type','size.size_type_id','=','size_type.id')
                ->get();

        $data ="";
            foreach ( $size as $item) {
                $data.='
                <p><input type="checkbox" name="product_size_value[]" value="'.$item->size_name_title.','.$item->size_type_id.','.$item->unique_size.'">
                    <label>'.$item->unique_size.'->Казахстанский</label></p>';
            }

        return response()->json($data);
    }

    public function getType()
    {
        $type = DB::table('size_type')->get(['type','id']);
        return view('admin.size.type_create',compact('type'));
    }

    public function postType(Request $request)
    {
        $this->validate($request,[
            'all_name'=>'required',
            'typeSize'=>'required'
        ]);
        $typeSize = $request['typeSize'];
        DB::table('size_name')->insert([
            'title'=>$request['all_name']
        ]);
        $id = DB::table('size_name')->where('title',$request['all_name'])->first(['id']);
        for($i=0;$i<count($typeSize);$i++){
            DB::table('size_name_type')->insert([
                'name_size_id'=>$id->id,
                'type_size_id'=>$typeSize[$i]
            ]);
        }
        return redirect('admin/size');
    }

    public function getTypeEdit($id)
    {
        $type = DB::table('size_type')->get(['type','id']);
        $Updatetype = DB::table('size_name_type')
                    ->where('name_size_id',$id)
                    ->get(['type_size_id']);
        $name = DB::table('size_name')->where('id',$id)->first();
//        dd($name);
        return view('admin.size.type_edit',compact('Updatetype','name','type'));
    }

    public function postTypeUpdate(Request $request)
    {
        DB::table('size_name')->where('id',$request['id'])->update([
            'title'=>$request['all_name']
        ]);
        DB::table('size_name_type')->where('name_size_id',$request['id'])->delete();
        for($i=0; $i<count($request['typeSize']);$i++){
            DB::table('size_name_type')->insert([
                'name_size_id'=>$request['id'],
                'type_size_id'=>$request['typeSize'][$i]
            ]);
        }
        return redirect()->back();
    }

    public function typeDelete($id)
    {
        DB::table('size_name')->where('id',$id)->delete();
        DB::table('size_name_type')->where('name_size_id',$id)->delete();

        return redirect()->back();
    }

}
