<?php
    $product_recommended_db = new \App\Models\ProductRecommended();
    $product_recommended = $product_recommended_db->getProductRecommended($row->product_id);
?>

@foreach($product_recommended as $val)

    <div>
        <a href="/product/{{$val->product_url}}-u{{$val->product_id}}" target="_blank">
            <?php
                $image = \App\Models\Image::where('product_id','=',$val->product_id)
                        ->where('is_main','=','1')
                        ->first();

                $image_url = 'default.jpg';
                if(isset($image->image_url)){
                    $image_url = $image->image_url;
                }
            ?>

            <img src="/image/product/{{$image_url}}" class="img-responsive center-block" alt="">
        </a>
        <a href="/product/{{$val->product_url}}-u{{$val->product_id}}" target="_blank">
            <h4 class="name_product">{{$val->product_name_ru}}</h4>
        </a>
        <h6>{{$val->brand_name}}</h6>
        <h4><b>{{$val->product_price}} тг</b></h4>
    </div>

@endforeach
