@extends('layout.admin-layout')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title" style="margin-top: 5px">Текст главной страницы</h3>
                </div>

                <div class="box-body">
                    <table id="news_datatable" class="table table-bordered table-striped">
                        <thead>
                        <tr style="border: 1px">
                            <th style="width: 30px">№</th>
                            <th>Текст</th>
                            <th style="width: 30px"></th>
                        </tr>
                        </thead>

                        <tbody>

                        @foreach($text as $key => $val)

                            <tr>
                                <td> {{ $key + 1 }}</td>
                                <td>
                                    <a href="" target="_blank" class="redirect-url">
                                        {{ $val->page_text }}
                                    </a>
                                </td>
                                <td style="text-align: center">
                                    <a href="/admin/main-page-text/{{ $val->id }}/edit">
                                        <li class="fa fa-pencil" style="font-size: 20px;"></li>
                                    </a>
                                </td>
                            </tr>

                        @endforeach

                        </tbody>

                    </table>

                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->

    <script src="/admin/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <link href="/fancybox/jquery.fancybox.css" type="text/css" rel="stylesheet">
    <script src="/fancybox/jquery.fancybox.pack.js" type="text/javascript"></script>

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <script>
        function delItem(ob,id){
            if(confirm('Действительно хотите удалить?')){
                $(ob).closest('tr').remove();
                $.ajax({
                    type: 'DELETE',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "/admin/banner/" + id,
                    success: function(data){

                    }
                });
            }
        }
    </script>

@endsection