@extends('layout.admin-layout')
@section('content')

        <script src="/admin/plugins/jQuery/jQuery-2.1.4.min.js"></script>

        <section class="content-header">
            <h1>
              {{ $title }}
            </h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-8" style="padding-left: 0px">
                        <div class="box box-primary">
                    @if (isset($error))
                        <div class="alert alert-danger">
                           {{ $error }}
                        </div>
                    @endif

                    <div id="error_text" class="alert alert-danger" style="display: none"></div>

                    @if($row->product_id > 0)
                        <form action="/admin/product/{{$row->product_id}}" method="POST">
                            <input type="hidden" name="_method" value="PUT">
                    @else
                        <form action="/admin/product" method="POST">
                    @endif
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input id="product_id" type="hidden" name="product_id" value="{{ $row->product_id }}">

                            <div class="box-body">
                                <div class="form-group">
                                    <label>Название</label>
                                    <input value="{{ $row->product_name_ru }}" type="text" class="form-control" name="product_name_ru" placeholder="Введите">
                                </div>
                                <div class="form-group">
                                    <label>Артикул</label>
                                    <input value="{{ $row->product_code }}" type="text" class="form-control" name="product_code" placeholder="Введите">
                                </div>
                                <div class="form-group">
                                  <label>Описание товара</label>
                                  <textarea id="product_desc_ru" name="product_desc_ru" class="form-control"><?=$row->product_desc_ru?></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Цена</label>
                                    <input value="{{ $row->product_price }}" type="text" class="form-control" name="product_price" placeholder="Введите">
                                </div>
                                <div class="form-group">
                                    <label>Бренд</label>
                                    <select class="form-control" name="brand_id">

                                        @foreach($brand as $val)
                                            <option @if($row->brand_id == $val->brand_id) {{'selected'}} @endif value="{{$val->brand_id}}">{{$val['brand_name']}}</option>
                                        @endforeach

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Скидка</label>
                                    <select class="form-control" name="discount_id">
                                        <option value="0">Нет</option>
                                        @foreach($discounts as $discount)
                                            <option value="{{ $discount->id }}" @if($row->discount_id == $discount->id) selected @endif>{{ $discount->name }} -{{ $discount->percent }}%</option>
                                        @endforeach
                                    </select>
                                </div>
                                {{--<div class="form-group">
                                    <label>Акция</label>
                                    <select onchange="showDiscount(this)" class="form-control" name="is_sale">
                                        <option value="0" @if($row->is_sale == '0') selected @endif>Нет</option>
                                        <option value="1" @if($row->is_sale == '1') selected @endif>Да</option>
                                    </select>
                                </div>--}}
                                {{--<div class="form-group discount" style="display:none; @if($row->is_sale == '1') display:block; @endif">
                                    <label>Старая цена</label>
                                    <input value="{{ $row->product_old_price }}" type="text" class="form-control" name="product_old_price" placeholder="Введите">
                                </div>
                                <div class="form-group discount" style="display:none; @if($row->is_sale == '1') display:block; @endif">
                                    <label>Скидка %</label>
                                    <input value="{{ $row->discount }}" type="text" class="form-control" name="discount" placeholder="Введите">
                                </div>--}}
                                <div class="form-group">
                                    <label>Количество на складе</label>
                                    <input value="{{ $row->product_count }}" type="text" class="form-control" name="product_count" placeholder="Введите">
                                </div>
                                <div class="form-group">
                                    <label>Сортировка</label>
                                    <input value="{{ $row->sort_num }}" type="text" class="form-control" name="sort_num" placeholder="Введите">
                                </div>
                                <div class="form-group">
                                    <label>Тег (пишите через запятую)</label>
                                    <textarea name="tag" class="form-control"><?=$row->tag?></textarea>
                                </div>
                                <div class="category-list">


                                    @if(isset($row->product_category))

                                        @foreach($row->product_category as $key => $value)

                                            @if($key > 0)

                                                <div class="category-select-content">
                                                    <div class="form-group col-md-4" style="padding-left: 0px">
                                                        <label>Главная категория</label>
                                                        <select onchange="getCategoryListByMainCategory(this)" name="category" data-placeholder="Выберите категорию" class="form-control">
                                                            <option value="0"></option>

                                                            @foreach($main_category as $val)
                                                                <option @if($value->main_category_id == $val->category_id) {{'selected'}} @endif value="{{$val->category_id}}">{{$val['category_name_ru']}}</option>
                                                            @endforeach

                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label>Категория</label>
                                                        <select onchange="getSubCategoryListByCategory(this)" data-placeholder="Выберите категорию" class="form-control category-select">

                                                            <option></option>

                                                            <?  $category_row = \App\Models\Category::where('category_level','=',2)
                                                                ->where('main_category_id','=',$value->main_category_id)
                                                                ->orderBy('category_name_ru','asc')
                                                                ->get();
                                                            ?>

                                                            @foreach($category_row as $val)
                                                                <option <?if($value->category_id == $val->category_id) echo 'selected '; ?> value="{{$val->category_id}}">{{$val['category_name_ru']}}</option>
                                                            @endforeach

                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label>Подкатегория</label>
                                                        <select name="subcategory[]" data-placeholder="Выберите подкатегорию" class="form-control subcategory-select">

                                                            <option></option>

                                                            <?  $subcategory_row = \App\Models\Category::where('category_level','=',3)
                                                                ->where('main_category_id','=',$value->category_id)
                                                                ->orderBy('category_name_ru','asc')
                                                                ->get();
                                                            ?>

                                                            @foreach($subcategory_row as $val)
                                                                <option <?if($value->subcategory_id == $val->category_id) echo 'selected '; ?> value="{{$val->category_id}}">{{$val['category_name_ru']}}</option>
                                                            @endforeach

                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-1" style="padding-top: 32px">
                                                        <a href="javascript:void(0)" onclick="deleteCategory(this)">
                                                            <i class="fa fa-times" style="font-size: 19px"></i>
                                                        </a>
                                                    </div>
                                                    <div style="clear: both"></div>
                                                </div>

                                            @endif

                                        @endforeach

                                    @endif
                                </div>
                                <div class="category-list">
                                    <div class="category-select-content">
                                        <div class="form-group col-md-4" style="padding-left: 0px">
                                            <label>Главная категория</label>
                                            <select onchange="getCategoryListByMainCategory(this)" name="category" data-placeholder="Выберите категорию" class="form-control">
                                                <option></option>

                                                @foreach($main_category as $val)
                                                    <option @if(isset($row->product_category[0]) && $row->product_category[0]->main_category_id == $val->category_id) {{'selected'}} @endif value="{{$val->category_id}}">{{$val['category_name_ru']}}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label>Категория</label>
                                            <select onchange="getSubCategoryListByCategory(this)" data-placeholder="Выберите категорию" class="form-control category-select">

                                                <option></option>

                                                @foreach($category as $val)
                                                    <option @if(isset($row->product_category[0]) && $row->product_category[0]->category_id == $val->category_id) {{'selected'}} @endif value="{{$val->category_id}}">{{$val['category_name_ru']}}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>Подкатегория</label>
                                            <select name="subcategory[]" data-placeholder="Выберите подкатегорию" class="form-control subcategory-select">

                                                <option></option>

                                                @foreach($subcategory as $val)
                                                    <option @if(isset($row->product_category[0]) && $row->product_category[0]->subcategory_id == $val->category_id) {{'selected'}} @endif value="{{$val->category_id}}">{{$val['category_name_ru']}}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                        <div class="form-group col-md-1" style="padding-top: 32px">
                                            <a href="javascript:void(0)" onclick="deleteCategory(this)">
                                                <i class="fa fa-times" style="font-size: 19px"></i>
                                            </a>
                                        </div>
                                        <div style="clear: both"></div>
                                    </div>

                                    @if(isset($row->product_category))

                                        @foreach($row->product_category as $key => $value)

                                            @if($key > 0)

                                                <div class="category-select-content">
                                                    <div class="form-group col-md-4" style="padding-left: 0px">
                                                        <label>Главная категория</label>
                                                        <select onchange="getCategoryListByMainCategory(this)" name="category" data-placeholder="Выберите категорию" class="form-control">
                                                            <option value="0"></option>

                                                            @foreach($main_category as $val)
                                                                <option @if($value->main_category_id == $val->category_id) {{'selected'}} @endif value="{{$val->category_id}}">{{$val['category_name_ru']}}</option>
                                                            @endforeach

                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label>Категория</label>
                                                        <select onchange="getSubCategoryListByCategory(this)" data-placeholder="Выберите категорию" class="form-control category-select">

                                                            <option></option>

                                                            <?  $category_row = \App\Models\Category::where('category_level','=',2)
                                                                    ->where('main_category_id','=',$value->main_category_id)
                                                                    ->orderBy('category_name_ru','asc')
                                                                    ->get();
                                                            ?>

                                                            @foreach($category_row as $val)
                                                                <option <?if($value->category_id == $val->category_id) echo 'selected '; ?> value="{{$val->category_id}}">{{$val['category_name_ru']}}</option>
                                                            @endforeach

                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label>Подкатегория</label>
                                                        <select name="subcategory[]" data-placeholder="Выберите подкатегорию" class="form-control subcategory-select">

                                                            <option></option>

                                                            <?  $subcategory_row = \App\Models\Category::where('category_level','=',3)
                                                                    ->where('main_category_id','=',$value->category_id)
                                                                    ->orderBy('category_name_ru','asc')
                                                                    ->get();
                                                            ?>

                                                            @foreach($subcategory_row as $val)
                                                                <option <?if($value->subcategory_id == $val->category_id) echo 'selected '; ?> value="{{$val->category_id}}">{{$val['category_name_ru']}}</option>
                                                            @endforeach

                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-1" style="padding-top: 32px">
                                                        <a href="javascript:void(0)" onclick="deleteCategory(this)">
                                                            <i class="fa fa-times" style="font-size: 19px"></i>
                                                        </a>
                                                    </div>
                                                    <div style="clear: both"></div>
                                                </div>

                                            @endif

                                        @endforeach

                                    @endif
                                </div>
                                <div class="form-group">
                                    <a href="javascript:void(0)" onclick="addCategoryBlock()">
                                        <button type="button" class="btn btn-success">Добавить категорию</button>
                                    </a>
                                </div>
                                <div class="feature-list">
                                    <div class="feature-content" id="feature_content_<?if(isset($row->product_feature[0])){ echo $row->product_feature[0]->feature_id; }?>">
                                        <div class="feature-content-first">
                                            <div class="form-group col-md-4" style="padding-left: 0px">
                                                <label>Выберите характеристику</label>
                                                <select onchange="getItemListByFeature(this,0)" name="feature_id[]" data-placeholder="Выберите" class="form-control">
                                                    <option value="0"></option>

                                                    @foreach($feature as $val)
                                                        <option @if(isset($row->product_feature[0]) && $row->product_feature[0]->feature_id == $val->feature_id) {{'selected'}} @endif value="{{$val->feature_id}}">{{$val['feature_name_ru']}}</option>
                                                    @endforeach

                                                </select>
                                            </div>
                                            <div class="form-group col-md-1" style="padding-top: 32px">
                                                <a href="javascript:void(0)" onclick="deleteFeature(this)">
                                                    <i class="fa fa-times" style="font-size: 19px"></i>
                                                </a>
                                            </div>
                                            <div style="clear: both"></div>
                                        </div>

                                        <div class="feature-content-second">

                                            @if(isset($row->product_feature[0]))

                                                <script>
                                                    $(document).ready(function(){
                                                        getItemListByFeature(this,'{{$row->product_feature[0]->feature_id}}');
                                                    });
                                                </script>

                                            @endif

                                        </div>

                                    </div>

                                    @if(isset($row->product_feature))

                                        @foreach($row->product_feature as $key => $value)

                                            @if($key > 0)

                                                <div class="feature-content" id="feature_content_{{$value->feature_id}}">
                                                    <div class="feature-content-first">
                                                        <div class="form-group col-md-4" style="padding-left: 0px">
                                                            <label>Выберите характеристику</label>
                                                            <select onchange="getItemListByFeature(this,0)" name="feature_id[]" data-placeholder="Выберите" class="form-control">
                                                                <option></option>
                                                                @if(isset($name))
                                                                    @foreach($feature as $val)
                                                                        <option @if($value->feature_id == $val->feature_id) {{'selected'}} @endif value="{{$val->feature_id}}">{{$val['feature_name_ru']}}</option>
                                                                    @endforeach
                                                                @endif
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md-1" style="padding-top: 32px">
                                                            <a href="javascript:void(0)" onclick="deleteFeature(this)">
                                                                <i class="fa fa-times" style="font-size: 19px"></i>
                                                            </a>
                                                        </div>
                                                        <div style="clear: both"></div>
                                                    </div>

                                                    <div class="feature-content-second">


                                                            <script>
                                                                $(document).ready(function(){
                                                                    getItemListByFeature(this,'{{$value->feature_id}}');
                                                                });
                                                            </script>


                                                    </div>

                                                </div>

                                            @endif

                                        @endforeach

                                    @endif

                                </div>
                                <div class="form-group">
                                    <a href="javascript:void(0)" onclick="addFeatureBlock()">
                                        <button type="button" class="btn btn-success">Добавить характеристику</button>
                                    </a>
                                </div>
                                {{--color--}}
                                <div class="form-group">
                                    <label for="color">Цвет товара</label>
                                    <select name="color" id="color" class="form-control">
                                        @if(isset($color))
                                            @foreach($color as  $item)
                                                <option value="{{$item->id}}" style="color: {{$item->code_color}}">{{$item->title_color}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="id_select">Выберите категория размеров </label>
                                    <select class="form-control" name="size_title" id="selected">
                                        <option></option>
                                        @if(isset($name))
                                            @foreach($name as $item)
                                                <option @if(isset($selected_id)) @if($item->id == $selected_id) selected @endif  @endif value="{{$item->id}}">{{$item->title}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <label >Выберите размер</label>
                                <div class=" form-size form-control" style="height: auto;">
                                    @if(isset($size))
                                        @for($i=0;$i<count($size);$i++)
                                            @for($c=0;$c<count($checked_sizes); $c++)
                                                @if($checked_sizes[$c]->unique_size == $size[$i]->size_name_id)
                                                <p>
                                                    <input checked type="checkbox" name="product_size_value[]" value="{{$size[$i]->size_name_title}},{{$size[$i]->size_type_id}},{{$size[$i]->size_name_id}}">
                                                    <label>{{$size[$i]->size_name_id}}/{{$size[$i]->type}}</label>
                                                </p>
                                                    @break
                                                @elseif($c+1 == count($checked_sizes))
                                                    <p>
                                                        <input type="checkbox" name="product_size_value[]" value="{{$size[$i]->size_name_title}},{{$size[$i]->size_type_id}},{{$size[$i]->size_name_id}}">
                                                        <label>{{$size[$i]->size_name_id}}/{{$size[$i]->type}}</label>
                                                    </p>
                                                @endif
                                            @endfor
                                        @endfor
                                    @endif
                                </div>
                                <div class="form-group" style="margin-top: 40px">
                                    <label>Рекомендованные товары</label>
                                    <div class="product_recommended_list">
                                        @include('admin.product.product-recommended-loop')
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label><a href="javascript:void(0)" onclick="showProductList()" style="color: #3C8DBC">Выбрать продукт для рекомендации +</a></label>
                                    <div id="product_list_content" style="display: none">
                                        <div class="form-group col-md-4" style="padding-left: 0px">
                                            <label>Поиск</label>
                                            <input id="search_word" value="" type="text" class="form-control" name="search_word" placeholder="Введите">
                                        </div>
                                        <div class="form-group col-md-4" style="padding-top: 25px" >
                                            <a href="javascript:void(0)" onclick="getProductListBySearch()">
                                                <button type="button" class="btn btn-block btn-success">Поиск</button>
                                            </a>
                                        </div>
                                        <div id="product_list">
                                            @include('admin.product.product-list-for-recommended-loop')
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Сохранить</button>
                            </div>
                        </form>
                    </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box box-primary" style="padding: 30px; text-align: center">
                            <div style="border-radius: 5%; border: none">
                                <img id="avatar_img" src="/image/product/default.jpg" style="width: 100%; max-width: 200px; border-radius: 5%;"/>
                            </div>
                            <div style="background-color: #c2e2f0;height: 40px;margin: 0 auto;width: 2px;"></div>
                            <form id="product_image_form" enctype="multipart/form-data" method="post" class="avatar-form">
                                <i class="fa fa-plus"></i>
                                <input id="avatar-file" type="file" onchange="uploadImage()" name="image"/>
                            </form>
                        </div>
                        <div class="box box-primart">
                            <div id="photo_content">

                            </div>
                            <div style="clear: both"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <link href="/fancybox/jquery.fancybox.css" type="text/css" rel="stylesheet">
        <script src="/fancybox/jquery.fancybox.pack.js" type="text/javascript"></script>
        <script>
            $('#selected').change(function(){
                $.ajax({
                    url:'/admin/size_value/',
                    type:'GET',
                    headers:{
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        size_val : $(this).val()
                    },
                    success: function (data) {
//                        console.log(data);
                        if(data.no!=""){
                            $('.form-size').html(data);
                        }
                    }
                })
            });
        </script>
        <script>
            var cw = $('#avatar_img').width();
            $('#avatar_img').css('height',cw);

            function uploadImage(){
                $("#product_image_form").submit();
            }
        </script>

        <style>
            .ke-container {
                max-width: 100%;
            }
        </style>

        <script>
            $(document).ready(function(){
                getImageList(0);
            });
        </script>

        <script>

            function getImageList(last_id){
                if(last_id > 0){
                    last_id  = $('.image-id:first').val();
                }

                $.ajax({
                    url:'/admin/product/image',
                    type: 'GET',
                    data: {
                        product_id: $('#product_id').val(),
                        last_id: last_id,
                        product_token: $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                        list = data;
                        if(last_id == 0){
                            $('#photo_content').html(list);
                        }
                        else {
                            $('#photo_content').prepend(list);
                        }
                    }
                });
            }

            g_image_id = 0;
            function confirmDeleteImage(image_id){
                g_image_id = image_id;
                if(confirm('Действительно хотите удалить?')){
                    deleteImage();
                }

            }

            function deleteImage(){
                $.ajax({
                    url:'/admin/product/image',
                    type: 'DELETE',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        image_id: g_image_id
                    },
                    success: function (data) {
                        if(data.success == 0){
                            showError(data.error);
                            return;
                        }
                        $('#image_id_' + g_image_id).remove();
                    }
                });
            }

            function getProductListBySearch(){
                $.ajax({
                    url:'/admin/product/product-list-by-search',
                    type: 'GET',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        search: $('#search_word').val(),
                        product_id: $('#product_id').val()
                    },
                    success: function (data) {
                        $('#product_list').html(data);
                    }
                });
            }

            function getRecommendedProductList(){
                $.ajax({
                    url:'/admin/product/recommended',
                    type: 'GET',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        product_id: $('#product_id').val()
                    },
                    success: function (data) {
                        $('.product_recommended_list').html(data);
                    }
                });
            }

            function addRecommendedProduct(ob,other_product_id){
                $.ajax({
                    url:'/admin/product/recommended',
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        other_product_id: other_product_id,
                        token: $('meta[name="csrf-token"]').attr('content'),
                        product_id: $('#product_id').val()
                    },
                    success: function (data) {
                        $(ob).closest('tr').remove();
                        getRecommendedProductList();
                    }
                });
            }

            function deleteRecommendedProduct(ob,other_product_id){
                $.ajax({
                    url:'/admin/product/recommended',
                    type: 'DELETE',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        other_product_id: other_product_id,
                        token: $('meta[name="csrf-token"]').attr('content'),
                        product_id: $('#product_id').val()
                    },
                    success: function (data) {
                        $(ob).closest('.feature-item').remove();
                        getProductListBySearch();
                    }
                });
            }

        </script>

        <script>

            function showDiscount(ob){
                if(ob.value == 1){
                    $('.discount').css('display','block');
                }
                else {
                    $('.discount').css('display','none');
                }
            }

            function getItemListByFeature(ob,feature_id){
                if(feature_id == 0){
                    feature_id = ob.value;
                }
                else {
                    feature_id = feature_id;
                    ob = $('#feature_content_' + feature_id).find('.feature-content-second');
                }

                $.ajax({
                    url:'/admin/feature-item',
                    type: 'GET',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        feature_id: feature_id,
                        is_product_feature_loop: 1,
                        product_id: $('#product_id').val()
                    },
                    success: function (data) {
                        $(ob).closest('.feature-content').find('.feature-content-second').html(data);
                    }
                });
            }

            function getCategoryListByMainCategory(ob){
                $.get('/admin/category/category-list-by-main/' + ob.value, function(data){
                    $(ob).closest('.category-select-content').find('.category-select').html('<option></option>');
                    $(ob).closest('.category-select-content').find('.subcategory-select').html('<option></option>');
                    $(data.result).each(function(){
                        $(ob).closest('.category-select-content').find('.category-select').append('<option value="' + this.category_id + '">' + this.category_name_ru +'</option>');
                    });
                });
            }

            function getSubCategoryListByCategory(ob){
                $.get('/admin/category/subcategory-list-by-category/' + ob.value, function(data){
                    $(ob).closest('.category-select-content').find('.subcategory-select').html('<option></option>');
                    $(data.result).each(function(){
                        $(ob).closest('.category-select-content').find('.subcategory-select').append('<option value="' + this.category_id + '">' + this.category_name_ru +'</option>');
                    });
                });
            }

            function addCategoryBlock(){
                $('.category-list').append($('.category-select-content:first').clone());
                $('.category-select-content:last').find('select').val(0);
            }

            function deleteCategory(ob){
                if($('.category-select-content').length == 1){
                    return;
                }
                $(ob).closest('.category-select-content').remove();
            }

            function changeMainImage(image_id) {
                $.ajax({
                    url:'/admin/product/change-main-image',
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        image_id: image_id,
                        image_token: $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                        if(data.success == 0){
                            alert(data.error);
                            return;
                        }
                        getImageList(0);
                    }
                });
            }

            function addFeatureBlock(){
                $('.feature-list').append($('.feature-content:first').clone());
                $('.feature-content-second:last').html('');
                $('.feature-content-first:last').find('select').val(0);
            }

            function deleteFeature(ob){
                if($('.feature-content').length == 1){
                    return;
                }
                $(ob).closest('.feature-content').remove();
            }

        </script>

    <style>
        .image-item {
            float: left;
            padding: 10px;
            width: 33%;
        }
        .image-item img {
            width: 100%;
            border-radius: 10px;
            height: 100px;
        }
        .main-image-item img {
            border: 2px solid #db3839;
        }
        .icon.icon-times-blue-o {
            background-image: url("/image/times-blue-o.png");
            height: 9px;
            margin-left: 22%;
            margin-top: 7px;
            position: absolute;
            width: 9px;
        }
        .icon {
            background-position: center center;
            background-repeat: no-repeat;
            display: inline-block;
        }
        .change-main-image-icon {
            color: #db3839;
            font-size: 10px;
            font-weight: bold;
            margin-top: 80px;
            position: absolute;
            text-decoration: underline;
        }

        .category-list, .feature-list {
            border: 1px solid #7e7e7e;
            margin-bottom: 10px;
            padding-left: 20px;
            padding-top: 7px;
            padding-right: 20px;
        }
        .feature-list {
            margin-top: 50px;
        }
        .category-select-content {
            border-top: 1px solid #7e7e7e;
            padding-top: 8px;
        }

        .feature-content {
            border-top: 1px solid #7e7e7e;
            padding-bottom: 42px;
            padding-top: 45px;
        }

        .category-select-content:first-child, .feature-content:first-child {
            border: none;
            padding-top: 8px;
        }

    </style>


        <link href="/wysiwyg/default.css" rel="stylesheet"/>
        <script type="text/javascript" src="/wysiwyg/kindeditor.js"></script>
        <script type="text/javascript" src="/wysiwyg/ru_Ru.js"></script>

        <script type="text/javascript">

            KindEditor.ready(function(K) {
                K.create('#product_desc_ru', {

                    cssPath : [''],
                    autoHeightMode : true, // это автоматическая высота блока
                    afterCreate : function() {
                        this.loadPlugin('autoheight');
                    },
                    allowFileManager : true,
                    items : [// Вот здесь задаем те кнопки которые хотим видеть
                        'source', '|', 'undo', 'redo', '|', 'preview', 'print', 'template', 'code', 'cut', 'copy', 'paste',
                        'plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 'justifyright',
                        'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript',
                        'superscript', 'clearhtml', 'fullscreen', '/',
                        'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold',
                        'italic', 'underline', 'strikethrough', 'lineheight', 'removeformat', '|', 'image', 'multiimage',
                        'flash', 'media', 'insertfile', 'table', 'hr', 'emoticons','productbreak',
                        'link',  'unlink'
                    ]
                });
                //Ниже инициализируем доп. например выбор цвета или загрузка файла
                var colorpicker;
                K('#colorpicker').bind('click', function(e) {
                    e.stopPropagation();
                    if (colorpicker) {
                        colorpicker.remove();
                        colorpicker = null;
                        return;
                    }
                    var colorpickerPos = K('#colorpicker').pos();
                    colorpicker = K.colorpicker({
                        x : colorpickerPos.x,
                        y : colorpickerPos.y + K('#colorpicker').height(),
                        z : 19811214,
                        selectedColor : 'default',
                        noColor : 'Очистить',
                        click : function(color) {
                            K('#color').val(color);
                            colorpicker.remove();
                            colorpicker = null;
                        }
                    });
                });
                K(document).click(function() {
                    if (colorpicker) {
                        colorpicker.remove();
                        colorpicker = null;
                    }
                });

                var editor = K.editor({
                    allowFileManager : true
                });
            });

        </script>

        <script type="text/javascript">
            $('a.fancybox').fancybox({
                padding: 10
            });
        </script>

        <script src="/javascript/jquery-2.1.1.min.js"></script>
        <script src="/javascript/jquery.maskedinput.js"></script>
        <script src="/javascript/uploadfile.js"></script>

        <script>
            function showProductList(){
                $('#product_list_content').toggle();
            }
        </script>
@endsection

