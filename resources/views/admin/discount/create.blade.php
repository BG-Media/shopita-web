@extends('layout.admin-layout')

@section('content')

    <section class="content-header">
        <h1>
            Добавление скидки
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-12" style="padding-left: 0px">
                    <div class="box box-primary">
                        @if (isset($error))
                            <div class="alert alert-danger">
                                {{ $error }}
                            </div>
                        @endif

                        <form action="{{ route('admin.discount.store') }}" method="POST" enctype="multipart/form-data">

                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="box-body">
                                <div class="form-group">
                                    <label>Название</label>
                                    <input value="" type="text" class="form-control" name="name" placeholder="Введите"
                                           required>
                                </div>
                                <div class="form-group">
                                    <label>Процент скидки</label>
                                    <input value="10" type="number" class="form-control" name="percent"
                                           placeholder="Введите" required>
                                </div>
                                <div class="form-group">
                                    <label>Изображение</label>
                                    <input type="file" class="form-control" name="image" accept="image/jpeg;image/png"
                                           placeholder="Введите" required>
                                </div>
                            </div>

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Сохранить</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

