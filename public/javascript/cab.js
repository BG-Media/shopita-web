var press_or_jour = 1, tab = 2, loaded_press = 25, press_per_load = 25, loaded_subs = 25;

$('document').ready(function(){
	$('.tabsa > a').click(function(){
		$('.vask').css('display', 'none');		
		$('.tabsa a').css('color', '#000');
		
		$($(this).data('tab') + 'tab').css('display', 'block');
		$(this).css('color', '#A91717');
		
		location.hash = $(this).data('tab');
		return false;
	});
	
	
	if(location.hash != ''){
		$('a[data-tab="'+location.hash+'"]').trigger('click');
		
		$('.pagination li a').each(function(){
			$(this).attr('href', $(this).attr('href') + location.hash);
		});
	}
	
	$('.fleft a').click(function(){
		var _this = $(this);
		$('.fleft a').removeClass('act');
		_this.addClass('act');
		
		$('.fright_cont').removeClass('opened');
		$(_this.attr('href')).addClass('opened');
		tab = _this.attr('href');
		//friendsView();
		return false;
	});
	
	$('.fright_head a').click(function(){
		var _this = $(this);
		$('.fright_head a').removeClass('act');
		_this.addClass('act');
		
		press_or_jour = parseInt(_this.data('id'));
		
		friendsView();
		return false;
	});
	friendsView();
});

function subscribers(subs_view, account) {
	$.get('/profile',{'_token':TOKEN, 'ACTION':'subscribers', 'subs_view':subs_view, 'account_type':account})
		.success(function(data){
			$('#'+data.response.account_type).html(data.html);
			$('#'+data.response.account_type).fadeIn();
		});
}

function subscribe(id){
	
	$.get('/profile', {ACTION: 'subscribe', id: id}, function(data){
		if(parseInt(data) == 1){
			$('#user' + id).html('<div class="notify">Вы успешно подписались!</div>');
			setTimeout(function(){
				$('#user' + id).fadeOut(300)				
				
			}, 3000);
			
			if($('#puser' + id + ' .prow').hasClass('pod0'))
			{
				$('#puser' + id + ' .prow').attr('class', 'prow pod1');
				item = $('#puser' + id);
				$('#ya_4itaiu').prepend(item.get(0).outerHTML);
				$('#press_list').find('#puser' + id).remove();
			}
			else
			{
				item = $('#puser' + id);
				$('#press_list').prepend(item.get(0).outerHTML);
				$('#ya_4itaiu').find('#puser' + id).remove();
				$('#puser' + id + ' .prow').attr('class', 'prow pod0');
			}
			
			if($('.vleft .prow').hasClass('pod0'))
				$('.vleft .prow').attr('class', 'prow pod1');
			else
				$('.vleft .prow').attr('class', 'prow pod0');
			
		}
		else alert('Не удалось подписаться на журналиста');
			
	});
	return false;
}

function friendsView(){ // это функция для фильтра журнализдов и пресс-служб
	var _list = $('.fright_cont .user_item');	
	$('.fright_head').show();
	_list.removeClass('closed');
	
	_list.each(function(){
		var t = $(this);
		
		if(tab != 2 && t.hasClass('podpiska' + tab))
			t.addClass('closed');
		
		if(press_or_jour != 0 && t.hasClass('acc' + press_or_jour))
			t.addClass('closed');
	});
	
}

function loadpress(){
	$('#presslist .loadmore').addClass('loading');
	$.get('/profile', {ACTION: 'getpress', last: loaded_press}, function(data){
		if(parseInt(data) != -1){		
			loaded_press += press_per_load;
			$('#presslist .loadmore').before(data);
		}
		else
			$('#presslist .loadmore').remove();
		
		$('#presslist .loadmore').removeClass('loading');
	});
}

function loadsubs(){
	$('#mysublist .loadmore').addClass('loading');
	$.get('/profile', {ACTION: 'getsubs', last: loaded_subs}, function(data){
		if(parseInt(data) != -1){		
			loaded_subs += press_per_load;
			$('#mysublist .loadmore').before(data);
		}
		else
			$('#mysublist .loadmore').remove();
		
		$('#mysublist .loadmore').removeClass('loading');
	});
}

function searchPressList(){
	event.preventDefault();	
	$('#press_list').html('<div class="loadmore loading"><span>Идет поиск...</span></div>');
	
	$.get('/profile', {ACTION: 'press_search', s: $('#press_name').val()}, function(data){
		if(parseInt(data) != -1){		
			loaded_press += press_per_load;
			$('#press_list').html(data);
		}
		else
			$('#press_list').html('<div class="loadmore loading"><span>Ничего не найдено.</span></div>');
	});
	return false;
}

function searchJourList(){
	
	$('#jour_list').html('<div class="loadmore loading"><span>Идет поиск...</span></div>');
	var city = $('#city').val();
	
	if($('#city_div select').length > 0)
		city = $('#city_div select').val();
	
	$.get('/profile', {ACTION: 'jour_search', s: $('#jour_name').val(), city: city, raion: $('#raion').val()}, function(data){
		if(parseInt(data) != -1){		
			loaded_press += press_per_load;
			$('#jour_list').html(data);
		}
		else
			$('#jour_list').html('<div class="loadmore loading"><span>Ничего не найдено.</span></div>');
	});
	return false;
}

function loadCities(){
	var id = $('#city').val();	
	if(id == 47 || id == 51){
		$('#city_div').html('');
		return;
	}
	
	$.get('/profile', {ACTION: 'city', id: id}, function(data){
		$('#city_div').html(data);
	})
}
