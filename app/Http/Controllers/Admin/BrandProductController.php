<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Brand;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use App\Helpers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class BrandProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    public function index(Request $request)
    {
        $search = '';
        if(isset($request->search)){
            $search = $request->search;
        }

        $active = 0;

        if(!isset($request->active)){
            $active = 1;
            $request->active = 1;
        }

        $row = Brand::orderBy('brand_id','desc')
                ->where('brand.is_show','=',$active)
                ->where(function($query) use ($search){
                    $query->where('brand_name','like','%' .$search .'%');
                })
                ->paginate(10);

        return  view('admin.brand-product.brand',[
            'row' => $row,
            'request' => $request
        ]);
    }

    public function create()
    {
        $row = new Brand();
        return  view('admin.brand-product.brand-edit', [
                'title' => 'Добавить бренд',
                'row' => $row
            ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'brand_name' => 'required'
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return  view('admin.brand-product.brand-edit', [
                'title' => 'Добавить бренд',
                'row' => (object) $request->all(),
                'error' => $error[0]
            ]);
        }

        $brand = new Brand();
        $brand->brand_name = $request->brand_name;
        $brand->save();
        return redirect('/admin/brand-product');
    }

    public function edit($id)
    {
        $row = Brand::find($id);
        return  view('admin.brand-product.brand-edit', [
            'title' => 'Изменить бренд',
            'row' => $row
        ]);
    }

    public function show(Request $request,$id){

    }

    public function update(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'brand_name' => 'required'
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return  view('admin.brand-product.brand-edit', [
                'title' => 'Изменить бренд',
                'row' => (object) $request->all(),
                'error' => $error[0]
            ]);
        }

        $brand = Brand::find($id);
        $brand->brand_name = $request->brand_name;
        $brand->save();
        return redirect('/admin/brand-product');
    }

    public function destroy($id)
    {
        $brand = Brand::find($id);
        $brand->delete();
    }

    public function changeIsShow(Request $request){
        $brand = Brand::find($request->id);
        $brand->is_show = $request->is_show;
        $brand->save();
    }
}
