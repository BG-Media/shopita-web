<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ProductFeature extends Model
{
    protected $table = 'product_feature';
    protected $primaryKey = 'product_feature_id';

    public function checkExistProductItem($product_id,$item_id){
        $row = ProductFeature::where('product_id','=',$product_id)
            ->where('item_id','=',$item_id)
            ->count();
        return $row;
    }

    public function checkExistProductFeature($product_id,$feature_id){
        $row = ProductFeature::where('product_id','=',$product_id)
            ->where('feature_id','=',$feature_id)
            ->count();
        return $row;
    }
    
}
