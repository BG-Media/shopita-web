<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class CategoryFeature extends Model
{
    protected $table = 'category_feature';
    protected $primaryKey = 'category_feature_id';

    public function checkExistCategoryItem($category_id,$item_id){
        $row = CategoryFeature::where('category_id','=',$category_id)
            ->where('item_id','=',$item_id)
            ->count();
        return $row;
    }

    public function checkExistCategoryFeature($category_id,$feature_id){
        $row = CategoryFeature::where('category_id','=',$category_id)
            ->where('feature_id','=',$feature_id)
            ->count();
        return $row;
    }
    
}
