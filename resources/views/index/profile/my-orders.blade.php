<ol class="breadcrumb">
    <li><a href="/">Главная</a></li>
    <li>Профиль</li>
    <li class="active">Мои заказы</li>
</ol>
<div class="list_sort dropdowns_top hidden-xs">
    <div class="dropdown">
        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            Сортировка
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
            <li>
                <a href="/profile?page_tab=2%{{\App\Helpers::replaceGetUrl('sort,product_price,sort_desc,asc')}}">По возрастанию цены</a>
            </li>
            <li>
                <a href="/profile?page_tab=2%{{\App\Helpers::replaceGetUrl('sort,product_price,sort_desc,desc')}}">По убыванию цены</a>
            </li>
            <li>
                <a href="/profile?page_tab=2%{{\App\Helpers::replaceGetUrl('sort,created_at,sort_desc,desc')}}">По дате</a>
            </li>
        </ul>
    </div>
</div>
<h2>Мои заказы</h2>

<div id="">
    @include('index.profile.my-orders-loop')
</div>

