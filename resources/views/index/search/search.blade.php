@extends('layout.layout')

@section('content')


    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="container">
                    <div class="row">
                        @include('index.search.left-menu')
                        <div class="col-md-10 col-sm-10">
                            <div class="clearfix hidden-xs">
                                <ol class="breadcrumb">
                                    <li>
                                        <a href="/">Главная</a>
                                    </li>
                                    <li>
                                        <a href="/catalog">@if($sex == '1') Мужчинам @else Женщинам @endif</a>
                                    </li>
                                </ol>
                            </div>
                            <div class="clearfix title_page_block">
                                <h3 class="title_page">Результат поиска по запросу "{{isset($query) && count($query)>0 ? $query : ''}}"</h3>
                                <div class="list_sort dropdowns_top hidden-xs">
                                    <div class="right_link hidden-xs pull-right">
                                        <a href="/catalog?{{\App\Helpers::getResetProduct()}}" class="sort_reset">Сбросить все</a>
                                    </div>
                                </div>
                                <p class="results_number">Найдено {{count($product)}} товаров</p>

                            </div>

                            <div class="clearfix">

                                <div class="catalogMainProducts clearfix">
                                    @include('index.search.product-list-loop')

                                </div>

                            </div>
                        </div>
                    </div>

                    @include('layout.footer')

                </div>
            </div>
        </div>
    </div>
@endsection