@if(!isset($product[0]))

        <p style="font-size: 19px">По вашему запросу ничего не найдено!</p>
@endif
@foreach($product as $val)
    <div class="col-md-3 col-sm-4 col-xs-6 elect search_results_products">
        <div class="row clearfix elect_product">
            <div class="images_pod">
                <a href="/product/{{$val->product_url}}-u{{$val->product_id}}" target="_blank">
                    <?
                    $image = \App\Models\Image::where('product_id','=',$val->product_id)
                        ->where('is_main','=','1')
                        ->first();
                    $image_url = 'default.jpg';
                    if(isset($image->image_url)){
                        $image_url = $image->image_url;
                    }
                    ?>
                    <img src="/image/product/{{$image_url}}" class="img-responsive center-block product_img" alt="">
                </a>
            </div>
            <div class="red_like">
                <?
                $favorite_db = new \App\Models\ProductFavorite();
                $check = $favorite_db->checkExistProductFavorite($val->product_id);
                $image_favorite = 'black_s.png';
                if($check > 0){
                    $image_favorite = 'red_s.png';
                }
                ?>

                @if($val->is_sale == 1)
                    <span class="salePros">-{{$val->discount}}%</span>
                    <a href="javascript:void(0)" onclick="changeFavoriteProduct(this,'{{$val->product_id}}')" class="like">
                        <img src="/image/{{$image_favorite}}" alt="">
                    </a>
                @else
                    <a href="javascript:void(0)" onclick="changeFavoriteProduct(this,'{{$val->product_id}}')" class="this_time">
                        <img src="/image/{{$image_favorite}}" alt="">
                    </a>
                @endif
            </div>
            <a href="/product/{{$val->product_url}}-u{{$val->product_id}}" target="_blank">
                <div class="more_informaion">
                    <p><b>Подробнее</b></p>
                </div>
            </a>
        </div>
        <h4 class="name_product">{{$val->product_name_ru}}</h4>

        <?
        $product_category = new \App\Models\ProductCategory();
        $product_category = $product_category->getProductCategory($val->product_id);
        ?>

        <h6>@if(isset($product_category[0])){{$product_category[0]->subcategory_name_ru}}@endif</h6>
        <h4>@if($val->is_sale == 1)<b class="sale1">{{$val->product_old_price}} тг</b>@endif <b class="sale2">{{$val->product_price}} тг</b></h4>
        
    </div>

@endforeach

<?$row = $product;?>

<?
$page_url = preg_replace('~(\?|&)page=[^&]*~','',http_build_query($_GET)) .'&';
?>




