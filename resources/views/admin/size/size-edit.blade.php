@extends('layout.admin-layout')

@section('content')

    <section class="content-header">
        <h1>Размер товаров</h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
                <form action="/admin/size" method="POST">
                    <input type="hidden" name="_token" value=" {{csrf_token()}}">
                    <div class="form-group">
                        <div class="form-group">
                            <label for="id_select">Выберите размер категории</label>
                            <select class="form-control" name="size_title" id="selected">
                                <option></option>
                                @foreach($name as $item)
                                    <option value="{{$item->id}}">{{$item->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <div id="form-ajax"></div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $('#selected').change(function(){
            $.ajax({
                url:'/admin/size_cat/',
                type:'GET',
                headers:{
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    size_cat : $(this).val()
                },
                success: function (data) {
                    console.log(data);
                    if(data.no!=""){
                        $('#form-ajax').html(data);
                    }
                }
            })
        });
    </script>
@endsection

