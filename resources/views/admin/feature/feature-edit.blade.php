@extends('layout.admin-layout')

@section('content')

        <section class="content-header">
            <h1>
              {{ $title }}
            </h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                    @if (isset($error))
                        <div class="alert alert-danger">
                           {{ $error }}
                        </div>
                    @endif

                    <div id="error_text" class="alert alert-danger" style="display: none"></div>

                    @if($row->feature_id > 0)
                        <form action="/admin/feature/{{$row->feature_id}}" method="POST">
                            <input type="hidden" name="_method" value="PUT">
                    @else
                        <form action="/admin/feature" method="POST">
                    @endif
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="feature_id" value="{{ $row->feature_id }}" id="feature_id">
                            <input type="hidden" value="{{ $row->feature_image }}" name="feature_image" id="feature_image">

                            <div class="box-body">
                                <div class="form-group">
                                    <label>Название</label>
                                    <input value="{{ $row->feature_name_ru }}" type="text" class="form-control" name="feature_name_ru" placeholder="Введите">
                                </div>
                                <div class="form-group">
                                    <label>Вид</label>
                                    <select onchange="showElementBtn(this)" class="form-control" name="kind_id">

                                        @foreach($feature_kind as $val)
                                            <option @if($row->kind_id == $val->kind_id) {{'selected'}} @endif value="{{$val->kind_id}}">{{$val['kind_name']}}</option>
                                        @endforeach

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Визуальный вид</label>
                                    <select class="form-control" name="visual_kind">
                                        <option value="1" @if($row->visual_kind == '1') selected @endif>Круглый вид</option>
                                        <option value="2" @if($row->visual_kind == '2') selected @endif>Выборка</option>
                                        <option value="3" @if($row->visual_kind == '3') selected @endif>Цвет</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                  <label>Описание</label>
                                  <textarea name="feature_desc" class="form-control"><?=$row->feature_desc?></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Сортировка</label>
                                    <input value="{{ $row->sort_num }}" type="text" class="form-control" name="sort_num" placeholder="Введите">
                                </div>
                                <div class="form-group" id="element_btn" style="@if($row->kind_id == '3') display:none; @endif">
                                    <a href="javascript:void(0)" onclick="showItemForm()">
                                        <button type="button" class="btn btn-success">Добавить элемент</button>
                                    </a>
                                </div>
                                <div id="feature_item_content">

                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Сохранить</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

        <div class="box box-info modal-form" id="item_modal">
            <div class="box-header">
                <h3 class="box-title">Элемент</h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <label>Название:</label>
                    <input id="item_name" class="form-control" type="text">
                </div>
                <div class="form-group">
                    <label>Доп. инфо:</label>
                    <input id="item_desc" class="form-control" type="text">
                </div>
                <div class="box-footer" style="text-align: center">
                    <a href="javascript:void(0)" onclick="addFeatureItem()">
                        <button type="submit" class="btn btn-primary" style="width: 200px">Сохранить</button>
                    </a>
                </div>
            </div>
        </div>

        <script src="/admin/plugins/jQuery/jQuery-2.1.4.min.js"></script>

        <script>
            $(document).ready(function(){
                getFeatureItemList();
            });
        </script>

        <script>

            function showElementBtn(ob) {
                if(ob.value != 3){
                    $('#element_btn').fadeIn(100);
                }
                else {
                    $('#element_btn').fadeOut(100);
                }
            }
            function showItemForm() {
                $('#item_name').val('');
                $('#item_desc').val('');
                $('#item_modal').fadeIn(100);
                $('.blur').fadeIn(100);
                g_item_id = 0;
            }

            function addFeatureItem(){
                if(g_item_id != 0){
                    editFeatureItem();
                    return;
                }

                if($('#item_name').val() == ''){
                    alert('Напишите название');
                    return;
                }

                $.ajax({
                    url:'/admin/feature-item',
                    type: 'POST',
                    data: {
                        feature_id: $('#feature_id').val(),
                        item_name: $('#item_name').val(),
                        item_desc: $('#item_desc').val(),
                        item_token: $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                        $('#item_name').val('');
                        $('.modal-form').fadeOut(100);
                        $('.blur').fadeOut(100);
                        getFeatureItemList();
                    }
                });
            }

            g_item_id = 0;

            function showEditItemForm(item_id,item_name,item_desc) {
                $('#item_name').val(item_name);
                $('#item_desc').val(item_desc);
                $('#item_modal').fadeIn(100);
                $('.blur').fadeIn(100);
                g_item_id = item_id;
            }

            function editFeatureItem(){
                if($('#item_name').val() == ''){
                    alert('Напишите название');
                    return;
                }

                $.ajax({
                    url:'/admin/feature-item/' + g_item_id,
                    type: 'POST',
                    data: {
                        feature_id: $('#feature_id').val(),
                        item_name: $('#item_name').val(),
                        item_desc: $('#item_desc').val(),
                        item_token: $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                        $('#item_name').val('');
                        $('.modal-form').fadeOut(100);
                        $('.blur').fadeOut(100);
                        getFeatureItemList();
                    }
                });
            }

            function getFeatureItemList(){
                $.ajax({
                    url:'/admin/feature-item',
                    type: 'GET',
                    data: {
                        feature_id: $('#feature_id').val(),
                        item_token: $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                        $('#feature_item_content').html(data);
                    }
                });
            }


            function confirmDeleteFeatureItem(item_id){
                g_item_id = item_id;
                if(confirm('Действительно хотите удалить?')){
                    deleteFeatureItem();
                }

            }

            function deleteFeatureItem(){
                $.ajax({
                    url:'/admin/feature-item',
                    type: 'DELETE',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        item_id: g_item_id
                    },
                    success: function (data) {
                        if(data.success == 0){
                            alert(data.error);
                            return;
                        }
                        getFeatureItemList();
                    }
                });
            }


        </script>




@endsection

