<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Helpers\HttpHelper;
use App\Models\Discount;
use Illuminate\Http\Request;
use App\Models\Page;
class MainController extends Controller
{
    public function contacts()
    {
        return HttpHelper::showSuccess([
            'email' => 'support@shopita.kz',
            'phones' => [
                '+77011234567',
            ],
            'coords' => [
                'string' => '43.25654,76.92848',
                'array' => [
                    'lat' => '43.25654',
                    'longitude' => '76.92848',
                ],
            ]
        ]);
    }

    public function main()
    {
        $row = Page::where('page_kind', 5)
            ->orderBy('page_id', 'desc')->select(['page_id', 'page_name_ru', 'page_image',])->get();

        return HttpHelper::showSuccess([
            'discount' => Discount::where('active', 1)->get(),
            'coupons' => $row
        ]);
    }

    public function blog(Request $request)
    {
        $row = Page::where('page_kind', 2)
            ->orderBy('page_id', 'desc')->select(['page_id', 'page_name_ru', 'page_image', 'page_url']);

        if ($request->has('search')) {
            $row = $row->where(function($query) use ($request)
            {
                $query->where('page_name_ru', 'like', '%'.$request->input('search').'%');
                $query->orWhere('page_text_ru', 'like', '%'.$request->input('search').'%');
            })->get();
        } else {
            $row = $row->paginate(8);
        }

        return HttpHelper::showSuccess($row);
    }

    public function blogShow($id) {
        $blog = Page::where('page_id', $id)->select(['page_id', 'page_name_ru', 'page_image', 'page_url', 'page_text_ru', 'created_at'])->first();
        return HttpHelper::showSuccess($blog);
    }
}