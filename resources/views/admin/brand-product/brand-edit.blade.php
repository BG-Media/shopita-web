@extends('layout.admin-layout')

@section('content')

        <section class="content-header">
            <h1>
              {{ $title }}
            </h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-12" style="padding-left: 0px">
                        <div class="box box-primary">
                    @if (isset($error))
                        <div class="alert alert-danger">
                           {{ $error }}
                        </div>
                    @endif

                    <div id="error_text" class="alert alert-danger" style="display: none"></div>

                    @if($row->brand_id > 0)
                        <form action="/admin/brand-product/{{$row->brand_id}}" method="POST">
                            <input type="hidden" name="_method" value="PUT">
                    @else
                        <form action="/admin/brand-product" method="POST">
                    @endif
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="brand_id" value="{{ $row->brand_id }}">

                            <div class="box-body">
                                <div class="form-group">
                                    <label>Название</label>
                                    <input value="{{ $row->brand_name }}" type="text" class="form-control" name="brand_name" placeholder="Введите">
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Сохранить</button>
                            </div>
                        </form>
                    </div>
                    </div>
                </div>
            </div>
        </section>

@endsection

