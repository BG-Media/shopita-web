<?php

namespace App\Http\Controllers\Admin;

use App\Models\CategoryFeature;
use App\Models\Feature;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use App\Helpers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use View;

class SubCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');

        $category = Category::where('category_level','=',1)
            ->orderBy('sort_num','asc')
            ->get();

        View::share('main_category', $category);

        $feature = Feature::orderBy('sort_num','asc')
            ->where('kind_id','!=',3)
            ->get();
        View::share('feature', $feature);
    }
    
    public function index(Request $request)
    {
        $search = '';
        if(isset($request->search)){
            $search = $request->search;
        }

        $active = 0;

        if(!isset($request->active)){
            $active = 1;
            $request->active = 1;
        }

        $row = Category::leftJoin('category as category2','category2.category_id','=','category.main_category_id')
                ->leftJoin('category as main_category','main_category.category_id','=','category2.main_category_id')
                ->orderBy('category.category_id','desc')
                ->where('category.category_level','3')
                ->where('category.is_show','=',$active)
                ->where('category2.has_child',1)
                ->where(function($query) use ($search){
                    $query->where('category.category_name_ru','like','%' .$search .'%')
                        ->orWhere('main_category.category_name_ru','like','%' .$search .'%')
                        ->orWhere('category2.category_name_ru','like','%' .$search .'%');
                })
                ->select('category.*',
                         'main_category.category_name_ru as main_category_name',
                         'category2.category_name_ru as category_name'
                        )
                ->paginate(10);

        return  view('admin.category.subcategory',[
            'row' => $row,
            'request' => $request
        ]);
    }

    public function create()
    {
        $row = new Category();
        $row->sort_num = 0;
        $row->category2_id = 0;

        return  view('admin.category.subcategory-edit', [
                'title' => 'Добавить категорию',
                'row' => $row
            ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_name_ru' => 'required',
            'sort_num' => 'required',
            'main_category_id' => 'required',
        ]);
        
        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return  view('admin.category.subcategory-edit', [
                'title' => 'Добавить категорию',
                'row' => (object) $request->all(),
                'error' => $error[0]
            ]);
        }

        $category = new Category();
        $category->category_name_ru = $request->category_name_ru;
        $category->category_level = 3;
        $category->main_category_id = $request->main_category_id;
        $category->sort_num = $request->sort_num;
        $category->category_url = Helpers::getTranslatedSlugRu($request->category_name_ru);
        $category->save();

        if(isset($request->feature_id)){
            foreach($request->feature_id as $val){
                $check = new CategoryFeature();
                if($val != ''){
                    $check = $check->checkExistCategoryFeature($category->category_id,$val);
                    if($check == 0 && $val != 0){
                        $category_feature = new CategoryFeature();
                        $category_feature->category_id = $category->category_id;
                        $category_feature->feature_id = $val;
                        $category_feature->save();
                    }
                }
            }
        }

        if(isset($request->item_id)){
            foreach($request->item_id as $val){
                $check = new CategoryFeature();
                if($val != ''){
                    $check = $check->checkExistCategoryItem($category->category_id,$val);
                    if($check == 0 && $val != 0){
                        $category_item = new CategoryFeature();
                        $category_item->category_id = $category->category_id;
                        $category_item->item_id = $val;
                        $category_item->save();
                    }
                }
            }
        }

        return redirect('/admin/subcategory');
    }

    public function edit($id)
    {
        $row = Category::find($id);
        
        $category = Category::orderBy('category_id','desc')
            ->where('category_level','2')
            ->where('category_id',$row->main_category_id)
            ->first();

        $row->category2_id = $category->main_category_id;

        $product_feature = CategoryFeature::where('category_id',$id)
            ->where('feature_id','>',0)->orderBy('category_feature_id','asc')->get();
        $row->category_feature = $product_feature;

        return  view('admin.category.subcategory-edit', [
            'title' => 'Изменить категорию',
            'row' => $row
        ]);
    }

    public function show(Request $request,$id){

    }

    public function update(Request $request,$id)
    {
        $product_feature = CategoryFeature::where('category_id',$id)
            ->where('feature_id','>',0)->orderBy('category_feature_id','asc')->get();
        $request->category_feature = $product_feature;

        $validator = Validator::make($request->all(), [
            'category_name_ru' => 'required',
            'sort_num' => 'required',
            'main_category_id' => 'required'
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return  view('admin.category.subcategory-edit', [
                'title' => 'Изменить категорию',
                'row' => (object) $request->all(),
                'error' => $error[0]
            ]);
        }

        $category = Category::find($id);
        $category->category_name_ru = $request->category_name_ru;
        $category->main_category_id = $request->main_category_id;
        $category->sort_num = $request->sort_num;
        $category->category_url = Helpers::getTranslatedSlugRu($request->category_name_ru);
        $category->save();

        CategoryFeature::where('category_id','=',$id)->delete();

        if(isset($request->feature_id)){
            foreach($request->feature_id as $val){
                $check = new CategoryFeature();
                if($val != ''){
                    $check = $check->checkExistCategoryFeature($category->category_id,$val);
                    if($check == 0 && $val != 0){
                        $category_feature = new CategoryFeature();
                        $category_feature->category_id = $category->category_id;
                        $category_feature->feature_id = $val;
                        $category_feature->save();
                    }
                }
            }
        }

        if(isset($request->item_id)){
            foreach($request->item_id as $val){
                $check = new CategoryFeature();
                if($val != ''){
                    $check = $check->checkExistCategoryItem($category->category_id,$val);
                    if($check == 0 && $val != 0){
                        $category_item = new CategoryFeature();
                        $category_item->category_id = $category->category_id;
                        $category_item->item_id = $val;
                        $category_item->save();
                    }
                }
            }
        }

        return redirect('/admin/subcategory');
    }

    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();
    }

    public function changeIsShow(Request $request){
        $user = Category::find($request->id);
        $user->is_show = $request->is_show;
        $user->save();
    }
}
