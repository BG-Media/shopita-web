<div class="collapse navbar-collapse" id="navbar-menu">
    <ul class="nav nav-pills  nav-justified">
        <li class="dropdown"><a href="#" class="dropbtn"> Каталог</a>
            <div class="col-md-12 inside_menu dropdown-content">
                <div class="row">

                    <?
                        $main_category = \App\Models\Category::where('category_level',1)
                                        ->where('is_male',$sex)
                                        ->where('is_show_main','1')
                                        ->orderBy('sort_num','asc')
                                        ->get();
                    ?>

                    @foreach($main_category as $val)

                            <div class="col-md-4 col-sm-4">
                                <span>
                                    <a href="/catalog?main={{$val->category_url}}-u{{$val->category_id}}">{{$val->category_name_ru}}</a>
                                </span>
                                <div class="category">
                                    <p>Категории</p>
                                    <ul>

                                        <?
                                            $category = \App\Models\Category::where('category_level',2)
                                                    ->where('main_category_id',$val->category_id)
//                                                    ->where('is_show_main','1')
                                                    ->where('is_show','!=','0')
                                                    ->orderBy('sort_num','asc')
                                                    ->take(6)
                                                    ->get();
//                                            dump($category);
                                        ?>

                                        @foreach($category as $value)

                                             <li>
                                                 <a href="/catalog?category={{$value->category_url}}-u{{$value->category_id}}">{{$value->category_name_ru}}</a>
                                             </li>

                                        @endforeach
                                    </ul>
                                </div>
                            </div>

                    @endforeach

                </div>

            </div>
        </li>
        <li><a href="/catalog?sale=1" class="@if(isset($menu) && $menu == 'discount') red_menu @endif">Распродажа % </a></li>
        <li><a href="/blog" class="@if(isset($menu) && $menu == 'blog') red_menu @endif">Стильный лук </a></li>
        <li><a href="/sale" class="@if(isset($menu) && $menu == 'sale') red_menu @endif"> Скидочные купоны   </a></li>
        <li><a href="/help" class="@if(isset($menu) && $menu == 'help') red_menu @endif">Помощь</a></li>
        <li><a href="/about" class="@if(isset($menu) && $menu == 'about') red_menu @endif">Контакты</a></li>
    </ul>
</div>