{{--{{dd($size)}}--}}
@extends('layout.admin-layout')
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title" style="margin-top: 5px">@if(isset($size[0])){{$size[0]->title}}@endif</h3>
                </div>
                <div style="padding-top: 10px; border-top: 1px solid rgb(127, 127, 127);"></div>
            </div>
            <div class="box">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <table class="table table-condensed">
                            <thead>
                            <tr>
                                @foreach($type as $item)
                                    <th>{{$item->type}}</th>
                                @endforeach
                                <th>Управление</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($size))
                            <?$a=0?>
                                @for($i=0;$i<count($size)/$count;$i++)
                                    <tr>
                                        @for($j=$a;$j<count($size);$j++)
                                            <td>{{$size[$j]->size_name_id}}</td>
                                            @if($j == $count-1+$a) <?$a=$a+$count?>@break; @endif
                                        @endfor
                                        <td>
                                            <form action="/admin/size/{{$size[$j]->unique_size}}" method="POST">
                                                {{csrf_field()}}
                                                {{method_field('DELETE')}}
                                                <button  class="btn btn-danger">Удалить</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endfor
                            @endif
                            </tbody>
                        </table>
                    </div>

                    <a href="/admin/size" style="float: right">
                        <button class="btn btn-primary" style="margin-bottom: 10px; ">Назад</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection