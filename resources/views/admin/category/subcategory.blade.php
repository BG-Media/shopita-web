@extends('layout.admin-layout')

@section('content')

<div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
            <h3 class="box-title" style="margin-top: 5px">Подкатегория</h3>
            </br>
            </br>
            <h3 class="box-title" style="margin-top: 5px; margin-right: 30px">
                <a href="?" style="color: #333333; @if(!isset($request->active) || $request->active == '1') {{ 'color: #367FA9; text-decoration:underline' }}@endif">Активные</a>
            </h3>
            <h3 class="box-title" style="margin-top: 5px">
                <a href="?active=0" style="color: #333333; @if($request->active == '0') {{ 'color: #367FA9; text-decoration:underline' }}@endif">Скрытые</a>
            </h3>
            <a href="/admin/subcategory/create" style="float: right">
               <button class="btn btn-primary" style="margin-bottom: 10px; ">Добавить новую категорию</button>
            </a>
        </div>
          <div style="padding-top: 10px; border-top: 1px solid rgb(127, 127, 127);">
              <div class="form-group col-md-3" >
                  <label>Поиск</label>
                  <input id="search_word" value="{{$request->search}}" type="text" class="form-control" name="search_word" placeholder="Введите">
              </div>
              <div class="form-group col-md-3" style="padding-top: 25px" >
                  <a href="javascript:void(0)" onclick="searchBySort()">
                      <button type="button" class="btn btn-block btn-success">Поиск</button>
                  </a>
              </div>
          </div>
          <div style="clear: both"></div>
          <div>
              <div style="text-align: left" class="form-group col-md-6" >

                  @if(!isset($request->active) || $request->active == '1')

                      <h4 class="box-title" style="margin-top: 5px; margin-right: 30px; margin-bottom: 0px">
                          <a href="javascript:void(0)" onclick="isShowDisabledAll()" style="text-decoration: underline; font-size: 17px; color: rgb(248, 23, 23);">Скрыть отмеченные</a>
                      </h4>

                  @else

                      <h4 class="box-title" style="margin-top: 5px; margin-right: 30px; margin-bottom: 0px">
                          <a href="javascript:void(0)" onclick="isShowEnabledAll()" style="text-decoration: underline; font-size: 17px; color: rgb(248, 23, 23);">Сделать активным отмеченные</a>
                      </h4>

                  @endif

              </div>
              <div style="text-align: right" class="form-group col-md-6" >
                  <h4 class="box-title" style="margin-top: 5px; margin-bottom: 0px">
                      <a href="javascript:void(0)" onclick="deleteAll()" style="text-decoration: underline; font-size: 17px; color: rgb(248, 23, 23);">Удалить отмеченные</a>
                  </h4>
              </div>
          </div>
        <div class="box-body">
          <table id="news_datatable" class="table table-bordered table-striped">
            <thead>
              <tr style="border: 1px">
                <th style="width: 30px">№</th>
                <th>Название</th>
                <th>Сортировка</th>
                <th>Главная категория</th>
                <th>Категория</th>
                <th style="width: 30px"></th>
                <th style="width: 30px"></th>
                <th class="no-sort" style="width: 0px; text-align: center; padding-right: 16px; padding-left: 14px;" >
                  <input onclick="selectAllCheckbox(this)" style="font-size: 15px" type="checkbox" value="1"/>
                </th>
              </tr>
            </thead>

            <tbody>

                  @foreach($row as $key => $val)

                     <tr>
                        <td> {{ $key + 1 }}</td>
                        <td>
                            {{ $val->category_name_ru }}
                        </td>
                        <td>
                             {{ $val->sort_num }}
                        </td>
                        <td>
                             {{ $val->main_category_name }}
                        </td>
                         <td>
                             {{ $val->category_name }}
                         </td>
                        <td style="text-align: center">
                            <a href="javascript:void(0)" onclick="delItem(this,{{ $val->category_id }})">
                                <li class="fa fa-trash-o" style="font-size: 20px; color: red;"></li>
                            </a>
                        </td>
                        <td style="text-align: center">
                            <a href="/admin/subcategory/{{ $val->category_id }}/edit">
                                <li class="fa fa-pencil" style="font-size: 20px;"></li>
                            </a>
                        </td>
                        <td style="text-align: center;">
                             <input class="select-all" style="font-size: 15px" type="checkbox" value="{{$val->category_id}}"/>
                        </td>
                     </tr>

                  @endforeach

            </tbody>

          </table>

            @if($row->lastPage() > 1)

                <div class="category_number" style="text-align: center">
                    <ul class="pagination">
                        <li>
                            <a @if($row->currentPage() > 1) href="?{{preg_replace('~(\?|&)page=[^&]*~','',http_build_query($_GET))}}&page={{ $row->currentPage() - 1 }}" @endif >&laquo;</a>
                        </li>
                        <?
                        $start = 1;
                        if($row->currentPage() >= 10){
                            $start = $row->currentPage();
                        }
                        $finish = $start + 9;
                        if($finish > $row->lastPage()){
                            $finish = $row->lastPage();
                            $start = $finish - 10;
                            if($start <= 0){
                                $start = 1;
                            }
                        }
                        ?>
                        @for ($i = $start; $i <= $finish; $i++)
                            <li class="{{ ($row->currentPage() == $i) ? 'active-paginator' : '' }}">
                                <a href="?{{preg_replace('~(\?|&)page=[^&]*~','',http_build_query($_GET))}}&page={{ $i }}">{{ $i }}</a>
                            </li>
                        @endfor
                        <li>
                            <a @if($row->currentPage() < $row->lastPage()) href="?{{preg_replace('~(\?|&)page=[^&]*~','',http_build_query($_GET))}}&page={{ $row->currentPage() + 1 }}" @endif>&raquo;</a>
                        </li>
                    </ul>
                </div>

            @endif

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
    </div><!-- /.row -->

<script src="/admin/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<link href="/fancybox/jquery.fancybox.css" type="text/css" rel="stylesheet">
<script src="/fancybox/jquery.fancybox.pack.js" type="text/javascript"></script>

<meta name="csrf-token" content="{{ csrf_token() }}">

<script>
    function delItem(ob,id){
        if(confirm('Действительно хотите удалить?')){
            $(ob).closest('tr').remove();
            $.ajax({
                type: 'DELETE',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/admin/subcategory/" + id,
                success: function(data){

                }
            });
        }
    }

    function selectAllCheckbox(ob) {
        if ($(ob).is(':checked')) {
            $('.select-all').prop('checked', true);
            console.log('ok');
        }
        else {
            $('.select-all').prop('checked', false);
            console.log('ok1');
        }

    }

    function deleteAll() {
        if(confirm('Действительно хотите удалить?')){
            $('.ajax-loader').fadeIn(100);
            $('.select-all').each(function(){
                if ($(this).is(':checked')) {
                    $.ajax({
                        type: 'DELETE',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: "/admin/subcategory/" + $(this).val(),
                        success: function(){

                        }
                    });
                    $(this).closest('tr').remove();
                }
            });
            $('.ajax-loader').fadeOut(100);
        }
    }

    function isShowDisabledAll() {
        if(confirm('Действительно хотите скрыть?')){
            $('.ajax-loader').fadeIn(100);
            $('.select-all').each(function(){
                if ($(this).is(':checked')) {
                    $.ajax({
                        type: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data :{
                            is_show: 0,
                            id: $(this).val()
                        },
                        url: "/admin/subcategory/is_show",
                        success: function(data){

                        }
                    });
                    $(this).closest('tr').remove();
                }
            });
            $('.ajax-loader').fadeOut(100);
        }
    }

    function isShowEnabledAll() {
        if(confirm('Действительно хотите сделать активным?')){
            $('.ajax-loader').fadeIn(100);
            $('.select-all').each(function(){
                if ($(this).is(':checked')) {
                    $.ajax({
                        type: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data :{
                            is_show: 1,
                            id: $(this).val()
                        },
                        url: "/admin/subcategory/is_show",
                        success: function(data){

                        }
                    });
                    $(this).closest('tr').remove();
                }
            });
            $('.ajax-loader').fadeOut(100);
        }
    }

    $( "#search_word" ).keyup(function(event) {
        if (!event.ctrlKey && event.which == 13) {
            searchBySort();
        }
    });

    function searchBySort() {
        href = '?{{preg_replace('~(\?|&)search=[^&]*~','',http_build_query($_GET))}}&search=' + $('#search_word').val();
        window.location.href = href;
    }

</script>

<script type="text/javascript">
    $('a.fancybox').fancybox({
        padding: 10
    });
</script>

@endsection