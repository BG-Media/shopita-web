<?php

namespace App\Http\Controllers\Index;

use App\Models\Category;
use App\Models\Page;
use App\Models\Users;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use App\Helpers;
use Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use View;
use DB;

class AuthController extends Controller
{
    public function __construct()
    {
        $sex = Helpers::getSessionSex();
        View::share('sex', $sex);
    }

    public function registerAjax(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|unique:users|email',
            'name' => 'required',
            'password' => 'required|min:4',
            'confirm_password' => 'required|same:password'
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            $result['error_list'][0]['register_name'] = $messages->first('name');
            $result['error_list'][1]['register_email'] = $messages->first('email');
            $result['error_list'][2]['register_password'] = $messages->first('password');
            $result['error_list'][3]['confirm_password'] = $messages->first('confirm_password');
            $result['error'] = $error[0];
            $result['success'] = false;
            return response()->json($result);
        }

        $user = new Users();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->mobile_phone = $request->phone;
        $user->password = Hash::make($request->password);
        $user->save();
        $delivery_kind = DB::table('delivery_kind')->pluck('delivery_kind_id');
        foreach ($delivery_kind as $item) {
            Db::table('subscriber')->insert([
               'email'=>$user->email,
                'email_is_send'=>1,
                'delivery_kind_id'=>$item,
                'name'=>$user->name,
                'phone'=>$user->phone
            ]);
        }
        
        $userdata = array(
            'email' => $request->email,
            'password' => $request->password
        );

        Auth::attempt($userdata);

        $email = $request->email;
        dd($email);

        \Mail::send('admin.registration', ['data' => $userdata,'email'=>$email], function($m) use ($userdata,$email) {
            $m->from(env('MAIL_USERNAME'), 'Shopita.kz');
            $m->to($email , 'info@shopita.kz')->subject('Успешная регистрация');
        });

        $delivery = DB::table('delivery_kind')->get();

        foreach ($delivery as $item) {
            DB::table('delivery_kind_user')->insert([
                'user_id'=>$user->user_id,
                'delivery_kind_id'=>$item->delivery_kind_id
            ]);
        }


        $result['success'] = true;
        $result['message'] = "Вы успешно зарегистрировались!";
        return response()->json($result);
    }

    public function loginAjax(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|min:4',
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();

            $result['error_list'][0]['login_email'] = $messages->first('email');
            $result['error_list'][1]['login_password'] = $messages->first('password');

            $result['error'] = $error[0];
            $result['success'] = false;
            return response()->json($result);
        }

        $userdata = array(
            'email' => $request->email,
            'password' => $request->password
        );

        if (!Auth::attempt($userdata))
        {
            $result['error'] = "Неправильный логин или пароль!";
            $result['success'] = false;
            return response()->json($result);
        }

        if(Auth::user()->is_ban == '1'){
            $result['error'] = "Вы не можете зайти на сайт, потому что вас забанили!";
            $result['success'] = false;
            Auth::logout();
            return response()->json($result);
        }

        $userdata = array(
            'email' => $request->email,
            'password' => $request->password
        );

        Auth::attempt($userdata);
        
        $result['success'] = true;
        return response()->json($result);
    }

    public function loginWithSocial(Request $request){
        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $user_info = json_decode($s, true);
        $user = Users::where('social_link','=',$user_info['identity'])->first();
        if($user === null){
            $user = new Users();
            $name = '';
            if(isset($user_info['last_name'])){
                $name = $user_info['last_name'] .' ';
            }
            if(isset($user_info['first_name'])){
                $name .= $user_info['first_name'];
            }
            $user->name = $name;

            if(isset($user_info['email'])){
                $user_check = Users::where('email','=',$user_info['email'])->first();
                if($user_check == null){
                    $user->email = $user_info['email'];
                }
            }

            if(isset($user_info['photo_big'])){
                $user->avatar = $user_info['photo_big'];
            }
            else if(isset($user_info['photo'])){
                $user->avatar = $user_info['photo'];
            }
            if(isset($user_info['phone'])){
                $user->phone = $user_info['phone'];
            }
            $user->password = Hash::make('123456');
            $user->role_id = 2;
            $user->is_social = 1;
            $user->social_link = $user_info['identity'];
            $user->save();
            $redirect = '/auth-social?social_link=' .$user_info['identity'];
        }

        $redirect = '/auth-social?social_link=' .$user_info['identity'];
        return redirect($redirect);

    }

    public function authSocial(Request $request){
        $userdata = array(
            'social_link' => $request->social_link,
            'password' => '123456'
        );
        if(Auth::attempt($userdata));
        {
            return redirect('/profile');
        }
    }

    public function changePassword(Request $request){

        if(isset($request->old_password)){
            $user = Users::where('user_id','=',Auth::user()->user_id)->first();
            $count = Hash::check($request->old_password, $user->password);
            if($count == false){
                $result['error_list'][0]['old_password'] = 'Неправильный старый пароль';
                $result['error'] = 'Неправильный старый пароль';
                $result['success'] = false;
                return response()->json($result);
            }

            $validator = Validator::make($request->all(), [
                'old_password' => 'required',
                'new_password' => 'required|different:old_password',
                'confirm_password' => 'required|same:new_password',
            ]);

            if ($validator->fails()) {
                $messages = $validator->errors();
                $error = $messages->all();
                $result['error_list'][0]['old_password'] = $messages->first('old_password');
                $result['error_list'][1]['new_password'] = $messages->first('new_password');
                $result['error_list'][2]['confirm_password'] = $messages->first('confirm_password');
                $result['error'] = $error[0];
                $result['success'] = false;
                return response()->json($result);
            }

            $user = Users::where('user_id','=',Auth::user()->user_id)->first();
            $user->password = Hash::make($request->new_password);
            $user->save();

            $result['message'] = 'Успешно изменен';
            $result['success'] = true;
            return response()->json($result);

        }
    }

    public function logout(){
        Auth::logout();
        return redirect('/');
    }

    public function resetPassword(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|email'
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            $result['error'] = $error[0];
            $result['success'] = false;
            return response()->json($result);
        }

        $user = Users::where('email','=',$request->email)->first();
        if($user === null){
            $result['error'] = 'Пользователь с указанной почтой не существует';
            $result['success'] = false;
            return response()->json($result);
        }

        $new_password = str_random(8);
        $password = Hash::make($new_password);
        $user->password = $password;
        $user->save();

        $email = $request->email;
        $userdata['user'] = $user->email;
        $userdata['password'] = $new_password;
        \Mail::send('admin.reset', ['data' => $userdata,'email'=>$email], function($m) use ($userdata,$email) {
            $m->from(env('MAIL_USERNAME'), 'Shopita.kz');
            $m->to($email , 'info@shopita.kz')->subject(' Восстановление пароля');
        });

        $result['success'] = true;
        $result['message'] = 'Ваш новый пароль успешно отправлен на почту';
        return response()->json($result);
    }
}
