<?php

namespace App\Http\Controllers\Admin;

use App\Models\CategoryFeature;
use App\Models\Feature;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use App\Helpers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use View;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');

        $category = Category::where('category_level','=',1)
            ->orderBy('sort_num','asc')
            ->get();

        View::share('main_category', $category);

        $feature = Feature::orderBy('sort_num','asc')
                    ->where('kind_id','!=',3)
                    ->get();
        View::share('feature', $feature);
    }
    
    public function index(Request $request)
    {
        $search = '';
        if(isset($request->search)){
            $search = $request->search;
        }

        $active = 0;

        if(!isset($request->active)){
            $active = 1;
            $request->active = 1;
        }

        $row = Category::leftJoin('category as main_category','main_category.category_id','=','category.main_category_id')
                ->orderBy('category.category_id','desc')
                ->where('category.category_level','2')
                ->where('category.is_show','=',$active)
                ->where(function($query) use ($search){
                    $query->where('category.category_name_ru','like','%' .$search .'%')
                        ->orWhere('main_category.category_name_ru','like','%' .$search .'%');
                })
                ->select('category.*',
                         'main_category.category_name_ru as main_category_name'
                        )
                ->paginate(10);

        return  view('admin.category.category',[
            'row' => $row,
            'request' => $request
        ]);
    }

    public function create()
    {
        $row = new Category();
        $row->sort_num = 0;
        return  view('admin.category.category-edit', [
                'title' => 'Добавить категорию',
                'row' => $row
            ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_name_ru' => 'required',
            'sort_num' => 'required'
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return  view('admin.category.category-edit', [
                'title' => 'Добавить категорию',
                'row' => (object) $request->all(),
                'error' => $error[0]
            ]);
        }

        $category = new Category();
        $category->category_name_ru = $request->category_name_ru;
        $category->category_level = 2;
        $category->is_show_main = $request->is_show_main;
        $category->main_category_id = $request->main_category_id;
        $category->sort_num = $request->sort_num;
        $category->has_child = $request->has_child;
        $category->category_url = Helpers::getTranslatedSlugRu($request->category_name_ru);
        $category->save();

        if($request->has_child == 0){
            $subcategory = new Category();
            $subcategory->category_name_ru = $request->category_name_ru;
            $subcategory->category_level = 3;
            $subcategory->is_show_main = $request->is_show_main;
            $subcategory->main_category_id = $category->category_id;
            $subcategory->sort_num = -1;
            $subcategory->category_url = Helpers::getTranslatedSlugRu($request->category_name_ru);
            $subcategory->save();
        }

        if(isset($request->feature_id)){
            foreach($request->feature_id as $val){
                $check = new CategoryFeature();
                if($val != ''){
                    $check = $check->checkExistCategoryFeature($category->category_id,$val);
                    if($check == 0 && $val != 0){
                        $category_feature = new CategoryFeature();
                        $category_feature->category_id = $category->category_id;
                        $category_feature->feature_id = $val;
                        $category_feature->save();
                    }
                }
            }
        }

        if(isset($request->item_id)){
            foreach($request->item_id as $val){
                $check = new CategoryFeature();
                if($val != ''){
                    $check = $check->checkExistCategoryItem($category->category_id,$val);
                    if($check == 0 && $val != 0){
                        $category_item = new CategoryFeature();
                        $category_item->category_id = $category->category_id;
                        $category_item->item_id = $val;
                        $category_item->save();
                    }
                }
            }
        }

        return redirect('/admin/category');
    }

    public function edit($id)
    {
        $row = Category::find($id);

        $product_feature = CategoryFeature::where('category_id',$id)
            ->where('feature_id','>',0)->orderBy('category_feature_id','asc')->get();
        $row->category_feature = $product_feature;
        
        return  view('admin.category.category-edit', [
            'title' => 'Изменить категорию',
            'row' => $row
        ]);
    }

    public function show(Request $request,$id){

    }

    public function update(Request $request,$id)
    {
        $product_feature = CategoryFeature::where('category_id',$id)
            ->where('feature_id','>',0)->orderBy('category_feature_id','asc')->get();
        $request->category_feature = $product_feature;

        $validator = Validator::make($request->all(), [
            'category_name_ru' => 'required',
            'sort_num' => 'required'
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return  view('admin.category.category-edit', [
                'title' => 'Изменить категорию',
                'row' => (object) $request->all(),
                'error' => $error[0]
            ]);
        }

        $category = Category::find($id);
        $category->category_name_ru = $request->category_name_ru;
        $category->is_show_main = $request->is_show_main;
        $category->main_category_id = $request->main_category_id;
        $category->sort_num = $request->sort_num;
        $category->has_child = $request->has_child;
        $category->category_url = Helpers::getTranslatedSlugRu($request->category_name_ru);
        $category->save();

        if($request->has_child == 0){
            $subcategory = Category::where('main_category_id',$category->category_id)
                ->where('category_level',3)->first();
            if($subcategory == null){
                $subcategory = new Category();
            }
            $subcategory->category_name_ru = $request->category_name_ru;
            $subcategory->category_level = 3;
            $subcategory->is_show_main = $request->is_show_main;
            $subcategory->main_category_id = $category->category_id;
            $subcategory->sort_num = -1;
            $subcategory->category_url = Helpers::getTranslatedSlugRu($request->category_name_ru);
            $subcategory->save();
        }

        CategoryFeature::where('category_id','=',$id)->delete();

        if(isset($request->feature_id)){
            foreach($request->feature_id as $val){
                $check = new CategoryFeature();
                if($val != ''){
                    $check = $check->checkExistCategoryFeature($category->category_id,$val);
                    if($check == 0 && $val != 0){
                        $category_feature = new CategoryFeature();
                        $category_feature->category_id = $category->category_id;
                        $category_feature->feature_id = $val;
                        $category_feature->save();
                    }
                }
            }
        }

        if(isset($request->item_id)){
            foreach($request->item_id as $val){
                $check = new CategoryFeature();
                if($val != ''){
                    $check = $check->checkExistCategoryItem($category->category_id,$val);
                    if($check == 0 && $val != 0){
                        $category_item = new CategoryFeature();
                        $category_item->category_id = $category->category_id;
                        $category_item->item_id = $val;
                        $category_item->save();
                    }
                }
            }
        }

        return redirect('/admin/category');
    }

    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();
    }

    public function changeIsShow(Request $request){
        $user = Category::find($request->id);
        $user->is_show = $request->is_show;
        $user->save();
    }

    public function getCategoryListByMainCategory($category_id){
        $category = Category::where('main_category_id','=',$category_id)
            ->where('category_level','=',2)
            ->orderBy('category_name_ru', 'asc')->get();

        $row = array();
        $count = 0;
        foreach ($category as $val)
        {
            $row[$count]['category_id'] = $val['category_id'];
            $row[$count]['category_name_ru'] = $val['category_name_ru'];
            $count++;
        }
        $result['result'] = $row;
        $result['success'] = true;
        return response()->json($result);
    }

    public function getSubCategoryListByCategory($category_id){
        $category = Category::leftJoin('category as main_category','main_category.category_id','=','category.main_category_id')
            ->where('category.main_category_id','=',$category_id)
            ->where('category.category_level','=',3)
            ->orderBy('category.category_name_ru', 'asc')
            ->select('category.*','main_category.has_child as has_child_check')
            ->get();

        $row = array();
        $count = 0;
        foreach ($category as $val)
        {
            $row[$count]['category_id'] = $val['category_id'];
            $name = $val['category_name_ru'];
            if($val['has_child_check'] == 0){
                $name = 'Нет';
            }
            $row[$count]['category_name_ru'] = $name;
            $count++;
        }
        $result['result'] = $row;
        $result['success'] = true;
        return response()->json($result);
    }
}
