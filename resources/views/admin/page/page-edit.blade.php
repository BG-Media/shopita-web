@extends('layout.admin-layout')

@section('content')

        <section class="content-header">
            <h1>
              {{ $title }}
            </h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                    @if (isset($error))
                        <div class="alert alert-danger">
                           {{ $error }}
                        </div>
                    @endif

                    <div id="error_text" class="alert alert-danger" style="display: none"></div>

                    @if($row->page_id > 0)
                        <form action="/admin/page/{{$row->page_id}}" method="POST">
                            <input type="hidden" name="_method" value="PUT">
                    @else
                        <form action="/admin/page" method="POST">
                    @endif
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="page_id" value="{{ $row->page_id }}">
                            <input type="hidden" value="{{ $row->page_image }}" name="page_image" id="page_image">

                            <div class="box-body">
                                <div class="form-group">
                                    <label>Название</label>
                                    <input value="{{ $row->page_name_ru }}" type="text" class="form-control" name="page_name_ru" placeholder="Введите">
                                </div>
                                <div class="form-group">
                                  <label>Текст страницы</label>
                                  <textarea id="page_text_ru" name="page_text_ru" class="form-control"><?=$row->page_text_ru?></textarea>
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Сохранить</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

            <link href="/wysiwyg/default.css" rel="stylesheet"/>
            <script type="text/javascript" src="/wysiwyg/kindeditor.js"></script>
            <script type="text/javascript" src="/wysiwyg/ru_Ru.js"></script>

            <script type="text/javascript">

                KindEditor.ready(function(K) {
                    K.create('#page_text_kz,#page_text_ru,#page_text_en', {

                        cssPath : [''],
                        autoHeightMode : true, // это автоматическая высота блока
                        afterCreate : function() {
                            this.loadPlugin('autoheight');
                        },
                        allowFileManager : true,
                        items : [// Вот здесь задаем те кнопки которые хотим видеть
                            'source', '|', 'undo', 'redo', '|', 'preview', 'print', 'template', 'code', 'cut', 'copy', 'paste',
                            'plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 'justifyright',
                            'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript',
                            'superscript', 'clearhtml', 'quickformat', 'selectall', '|', 'fullscreen', '/',
                            'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold',
                            'italic', 'underline', 'strikethrough', 'lineheight', 'removeformat', '|', 'image', 'multiimage',
                            'flash', 'media', 'insertfile', 'table', 'hr', 'emoticons','pagebreak',
                            'anchor', 'link',  'unlink','map', '|', 'about'
                        ]
                    });
                    //Ниже инициализируем доп. например выбор цвета или загрузка файла
                    var colorpicker;
                    K('#colorpicker').bind('click', function(e) {
                        e.stopPropagation();
                        if (colorpicker) {
                            colorpicker.remove();
                            colorpicker = null;
                            return;
                        }
                        var colorpickerPos = K('#colorpicker').pos();
                        colorpicker = K.colorpicker({
                            x : colorpickerPos.x,
                            y : colorpickerPos.y + K('#colorpicker').height(),
                            z : 19811214,
                            selectedColor : 'default',
                            noColor : 'Очистить',
                            click : function(color) {
                                K('#color').val(color);
                                colorpicker.remove();
                                colorpicker = null;
                            }
                        });
                    });
                    K(document).click(function() {
                        if (colorpicker) {
                            colorpicker.remove();
                            colorpicker = null;
                        }
                    });

                    var editor = K.editor({
                        allowFileManager : true
                    });
                });

            </script>

@endsection

