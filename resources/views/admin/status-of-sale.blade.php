@extends('layout.admin-layout')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title" style="margin-top: 5px">Статус продажи</h3>
                    </br>
                </div>
                <div class="box-body">
                    <p class="inform"><span class="info">{{ $profitCurrentMonth }}</span> - чистых продаж в этом месяце</p> <br>
                    <p class="inform">
                        <span class="info">@foreach($products as $prod) {{ $prod->product_name_ru }}, @endforeach</span> - лидер продаж этого месяца
                    </p> <br>
                    <p class="inform"><span class="info">{{ $numberOrders }} заказ </span> - ожидает обработки</p> <br>
                    <p class="inform"><span class="info">{{ $numberProducts }} товара </span> - заканчивается</p> <br>
                    <p class="inform"><span class="info">{{ $quantity }} товара </span> - нет в наличии</p>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
@endsection