@extends('layout.admin-layout')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title" style="margin-top: 5px">Цвет</h3>
                    <a href="/admin/color/create" style="float: right">
                        <button class="btn btn-primary" style="margin-bottom: 10px; ">Добавить новый цвет</button>
                    </a>
                </div>
                <div style="padding-top: 10px; border-top: 1px solid rgb(127, 127, 127);">
                    <div class="form-group col-md-3" >
                        <label>Поиск</label>
                        <input id="search_word" value="#" type="text" class="form-control" name="search_word" placeholder="Введите">
                    </div>
                    <div class="form-group col-md-3" style="padding-top: 25px" >
                        <a href="javascript:void(0)" onclick="searchBySort()">
                            <button type="button" class="btn btn-block btn-success">Поиск</button>
                        </a>
                    </div>
                </div>
                <div style="clear: both"></div>
                <div class="box-body">
                    <table id="news_datatable" class="table table-bordered table-striped">
                        <thead>
                        <tr style="border: 1px">
                            <th style="width: 30px">№</th>
                            <th style="width: 30px">Заголовок</th>
                            <th style="width: 30px"> Цвет</th>
                            <th style="width: 30px">Редактирование</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($colors as $item)
                            <tr>
                                <td>{{$item->id}}</td>
                                <td style="color: {{$item->code_color}};">{{$item->title_color}}</td>
                                <td > {{$item->code_color}}</td>
                                <td style="display: flex;">
                                    <a href="/admin/color/{{$item->id}}/edit" class="btn btn-info">Изменить</a>
                                    <form action="/admin/color/{{$item->id}}" method="post">
                                        {{method_field('Delete')}}
                                        {{csrf_field()}}
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <input type="submit" value="Удалить" class="btn btn-danger">
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>

                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
@endsection