
<?
$product_recommended_db = new \App\Models\ProductRecommended();
    if($row->product_id > 0){
        $product_recommended = $product_recommended_db->getProductRecommended($row->product_id);
    }
    else {
        $product_recommended = $product_recommended_db->getProductRecommendedByToken(csrf_token());
    }
?>

@foreach($product_recommended as $key => $val)

    <div class="feature-item col-md-4">
        <div class="form-group feature-item-left">
            <a href="/product/{{$val->product_url}}-u{{$val->product_id}}" target="_blank">
                <label>{{$val->product_name_ru}}</label>
            </a>
        </div>
        <div class="form-group feature-item-right">
            <a href="javascript:void(0)" onclick="deleteRecommendedProduct(this,'{{$val->product_id}}')">
                <i class="fa fa-times" style="font-size: 19px; color: #ff3b3b"></i>
            </a>
        </div>
        <div style="clear: both"></div>
    </div>
    <div style="clear: both"></div>

@endforeach