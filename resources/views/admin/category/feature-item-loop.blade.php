
<?

$product_item = \App\Models\CategoryFeature::where('category_id',$category_id)
        ->where('item_id','>',0)->orderBy('category_feature_id','asc')->get();

$product_feature = \App\Models\CategoryFeature::where('category_id',$category_id)
        ->where('feature_id',$feature->feature_id)
        ->orderBy('category_feature_id','asc')->first();
?>


<div class="form-group">

    @foreach($item as $key => $val)

        <?$selected = '';?>

        @foreach($product_item as $item_val)

            @if($val->item_id == $item_val->item_id)
                <?$selected = 'checked';?>
                @break;
            @endif

        @endforeach

        <div>
            <div class="left-float delivery-checkbox">
                <input name="item_id[]" {{ $selected }} type="checkbox" value="{{ $val->item_id }}"/>
            </div>
            <div class="left-float">
                <span>{{ $val->item_name_ru }}</span>
            </div>
            <div class="clear-float"></div>
        </div>

    @endforeach

</div>





