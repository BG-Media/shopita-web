@extends('layout.admin-layout')

@section('content')

        <section class="content-header">
            <h1>
              {{ $title }}
            </h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-10">
                    <div class="box box-primary">
                    @if (isset($error))
                        <div class="alert alert-danger">
                           {{ $error }}
                        </div>
                    @endif

                    <div id="error_text" class="alert alert-danger" style="display: none"></div>

                    @if($row->subscriber_id > 0)
                        <form action="/admin/contact-delivery/{{$row->subscriber_id}}" method="POST">
                            <input type="hidden" name="_method" value="PUT">
                    @else
                        <form action="/admin/contact-delivery" method="POST">
                    @endif
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="subscriber_id" value="{{ $row->subscriber_id }}">

                            <div class="box-body">
                                <div class="form-group">
                                    <label>Название </label>
                                    <input value="{{ $row->name }}" type="text" class="form-control" name="name" placeholder="Введите">
                                </div>
                                <div class="form-group">
                                    <label>Почта </label>
                                    <input value="{{ $row->email }}" type="text" class="form-control" name="email" placeholder="Введите">
                                </div>
                                <div class="form-group">
                                    <label>Телефон </label>
                                    <input id="phone" value="{{ $row->phone }}" type="text" class="form-control" name="phone" placeholder="Введите">
                                </div>
                                <div class="form-group">
                                    <label>Инфо </label>
                                    <input value="{{ $row->info }}" type="text" class="form-control" name="info" placeholder="Введите">
                                </div>
                                <div class="form-group">
                                    <label>Выберите базу</label>
                                    <select class="form-control" name="delivery_kind_id">

                                        <? foreach($delivery_kind as $value)
                                        {
                                            if (in_array($value['delivery_kind_id'], $arr))
                                                continue;

                                            $selected = '';
                                            if ($value['delivery_kind_id'] == $row->delivery_kind_id)
                                            {
                                                $selected=' selected ';
                                            }
                                            if (isset($request->delivery_kind_id) && $value['delivery_kind_id'] == $request->delivery_kind_id)
                                            {
                                                $selected=' selected ';
                                            }
                                        ?>
                                        <option value="<?=$value['delivery_kind_id']?>" <?=$selected?>><?=$value['delivery_kind_name']?></option>
                                        <?
                                        }
                                        ?>

                                    </select>
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Сохранить</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

        <script src="/javascript/jquery-2.1.1.min.js"></script>
        <script src="/javascript/jquery.maskedinput.js"></script>
        <script src="/javascript/uploadfile.js"></script>

        <script>
            $("#phone").mask("+7(999)999-99-99");

            var cw = $('#avatar_img').width();
            $('#avatar_img').css('height',cw);
        </script>

       {{-- <script>
            $( "input" ).keyup(function(event) {
                if (!event.ctrlKey && event.which == 13) {
                    $(this).next('input').focus();
                }
            });
        </script>--}}
@endsection

