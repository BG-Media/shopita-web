@extends('layout.admin-layout')

@section('content')

        <section class="content-header">
            <h1>
              {{ $title }}
            </h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-12" style="padding-left: 0px">
                        <div class="box box-primary">
                    @if (isset($error))
                        <div class="alert alert-danger">
                           {{ $error }}
                        </div>
                    @endif

                    <div id="error_text" class="alert alert-danger" style="display: none"></div>

                    @if($row->category_id > 0)
                        <form action="/admin/main-category/{{$row->category_id}}" method="POST">
                            <input type="hidden" name="_method" value="PUT">
                    @else
                        <form action="/admin/main-category" method="POST">
                    @endif
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="category_id" value="{{ $row->category_id }}">
                            <input type="hidden" value="{{ $row->category_image }}" name="category_image" id="category_image">

                            <div class="box-body">
                                <div class="form-group">
                                    <label>Название</label>
                                    <input value="{{ $row->category_name_ru }}" type="text" class="form-control" name="category_name_ru" placeholder="Введите">
                                </div>
                                <div class="form-group">
                                    <label>Пол</label>
                                    <select class="form-control" name="is_male">
                                        <option value="1" @if($row->is_male == '1') selected @endif>Мужской</option>
                                        <option value="0" @if($row->is_male == '0') selected @endif>Женский</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Отбражать на главной странице</label>
                                    <select class="form-control" name="is_show_main">
                                        <option value="0" @if($row->is_show_main == '0') selected @endif>Нет</option>
                                        <option value="1" @if($row->is_show_main == '1') selected @endif>Да</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Сортировка</label>
                                    <input value="{{ $row->sort_num }}" type="text" class="form-control" name="sort_num" placeholder="Введите">
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Сохранить</button>
                            </div>
                        </form>
                    </div>
                    </div>
                </div>
            </div>
        </section>

@endsection

