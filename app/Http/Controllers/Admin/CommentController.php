<?php

namespace App\Http\Controllers\Admin;

use App\Models\Comment;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use App\Helpers;
use Auth;
use DB;
use Illuminate\Support\Facades\Redirect;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    public function index(Request $request)
    {
        $active = 0;
        if(isset($request->active)){
            $active = 1;
        }

        $search = '';
        if(isset($request->search)){
            $search = $request->search;
        }
        
        $row = Comment::leftJoin('product','product.product_id','=','comment.product_id')
            ->where('comment.is_show',$active)
            ->where('comment.message','like','%' .$search .'%')
            ->orderBy('comment_id','desc')
            ->select('comment.*',
                     'product.*',
                     'comment.is_show as is_show',
                      DB::raw('DATE_FORMAT(comment.created_at,"%d.%m.%Y %H:%i") as date'))
            ->paginate(10);
        
        return  view('admin.comment.comment',[
            'row' => $row,
            'active' => $active,
            'request' => $request
        ]);
    }

    public function destroy($id)
    {
        $page = Comment::find($id);
        $page->delete();
    }

    public function getCommentCount()
    {
        $count = Comment::where('is_show','=','0')->count();
        $result['count'] = $count;
        return $result;
    }

    public function changeIsShow(Request $request){
        $comment = Comment::find($request->comment_id);
        $comment->is_show = $request->is_show;
        $comment->save();
    }
}
