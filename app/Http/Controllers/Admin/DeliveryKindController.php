<?php

namespace App\Http\Controllers\Admin;

use App\Models\DeliveryKind;
use App\Models\Delivery;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use App\Helpers;
use Auth;

class DeliveryKindController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        $row = DeliveryKind::where('delivery_kind_id','>',0)
            ->orderBy('delivery_kind_id','desc')
            ->get();
        return  view('admin.delivery-kind.delivery-kind',[
            'row' => $row
        ]);
    }

    public function create()
    {
        $row = new DeliveryKind();
        return  view('admin.delivery-kind.delivery-kind-edit', [
                'title' => 'Добавить базу',
                'row' => $row
            ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'delivery_kind_name' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return  view('admin.delivery-kind.delivery-kind-edit', [
                'title' => 'Добавить базу',
                'row' => (object) $request->all(),
                'error' => $error[0]
            ]);
        }

        $deliver = new DeliveryKind();
        $deliver->delivery_kind_name = $request->delivery_kind_name;
        $deliver->save();
        return redirect('/admin/delivery-kind');
    }

    public function edit($id)
    {
        $row = DeliveryKind::find($id);
        return  view('admin.delivery-kind.delivery-kind-edit', [
            'title' => 'Изменить базу',
            'row' => $row
        ]);
    }

    public function show(Request $request,$id){

    }

    public function update(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'delivery_kind_name' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return  view('admin.delivery-kind.delivery-kind-edit', [
                'title' => 'Изменить базу',
                'row' => (object) $request->all(),
                'error' => $error[0]
            ]);
        }

        $deliver = DeliveryKind::find($id);
        $deliver->delivery_kind_name = $request->delivery_kind_name;
        $deliver->save();
        return redirect('/admin/delivery-kind');
    }

    public function destroy($id)
    {
        $deliver = DeliveryKind::find($id);
        $deliver->delete();
    }
}
