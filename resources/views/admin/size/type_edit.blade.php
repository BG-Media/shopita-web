{{--{{dd($type)}}--}}
@extends('layout.admin-layout')
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3>Общий название размеров</h3>
                </div>
                <div style="padding-top: 10px; border-top: 1px solid rgb(127, 127, 127);"></div>
            </div>
                <form action="/admin/size/type/update/{{$name->id}}" method="POST">
                            {{method_field('POST')}}
                            {{csrf_field()}}
                            <div class="box">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <div class="form-froup">
                                            <label for="allName">Заголовок</label>
                                            <input id="allName" type="text" value="{{$name->title}}" class="form-control" name="all_name">
                                            <input type="hidden" value="{{$name->id}}" name="id">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="box">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h2>Тип размеров</h2>
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th>Тип</th>
                                                <th>Редактирование</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                @for($c=0;$c<count($type);$c++)
                                                    @for($i=0;$i<count($Updatetype);$i++)
                                                        @if($Updatetype[$i]->type_size_id == $type[$c]->id)
                                                            <tr>
                                                                <td>{{$type[$c]->type}}</td>
                                                                <td><input type="checkbox" value="{{$type[$c]->id}}" id="{{$type[$c]->id}}" checked  name="typeSize[]"></td>
                                                            </tr>
                                                            @break
                                                        @elseif($i==count($Updatetype)-1)
                                                            <tr>
                                                                <td>{{$type[$c]->type}}</td>
                                                                <td><input type="checkbox" value="{{$type[$c]->id}}" id="{{$type[$c]->id}}"  name="typeSize[]"></td>
                                                            </tr>
                                                        @endif
                                                    @endfor
                                                @endfor
                                            </tbody>
                                        </table>
                                        <br>
                                        <a href="#" style="text-align: right;">
                                            <button class="btn btn-primary" style="margin-bottom: 10px; ">Сохранить</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </form>
        </div>
    </div>
@endsection