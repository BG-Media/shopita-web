<ol class="breadcrumb">
    <li><a href="/">Главная</a></li>
    <li>Профиль</li>
    <li class="active">Мои подписки</li>
</ol>
<h2>Мои подписки</h2>
<div class="col-md-12">
    <div class="row">
        <form action="javascript:void(0);" id="subscription" >
            <p>Каталог</p>
            @if(count($all_subscription))
                @foreach($all_subscription as $item)
                    @for($i=0;$i<count($user_subscription);$i++)
                        @if($item->delivery_kind_id == $user_subscription[$i]->delivery_kind_id)
                            <input type="checkbox" name="check_input[]" value="{{$item->delivery_kind_id}}" class="check_input" checked> {{$item->delivery_kind_name}}
                            <br>
                            @break
                        @elseif(count($user_subscription) == $i+1)
                            <input type="checkbox" name="check_input[]" value="{{$item->delivery_kind_id}}" class="check_input" > {{$item->delivery_kind_name}}
                            <br>
                        @endif
                    @endfor
                @endforeach
            @endif
        </form>
    </div>
    <div class="row">
        <a href="#">
            <button type="button" class="change_data">Сохранить</button>
        </a>
    </div>
</div>