
@foreach($item as $key => $val)

    <div class="feature-item col-md-4">
        <div class="form-group feature-item-left">
            <label>{{$val->item_name_ru}}</label>
        </div>
        <div class="form-group feature-item-right">
            <a href="javascript:void(0)" onclick="showEditItemForm('{{$val->item_id}}','{{$val->item_name_ru}}','{{$val->item_desc}}')">
                <i class="fa fa-pencil" style="font-size: 18px"></i>
            </a>
            <a href="javascript:void(0)" onclick="confirmDeleteFeatureItem('{{$val->item_id}}')">
                <i class="fa fa-times" style="font-size: 19px; color: #ff3b3b"></i>
            </a>
        </div>
        <div style="clear: both"></div>
    </div>
    <div style="clear: both"></div>

@endforeach