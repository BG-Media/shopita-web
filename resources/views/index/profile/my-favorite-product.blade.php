<ol class="breadcrumb">
    <li><a href="/">Главная</a></li>
    <li>Профиль</li>
    <li class="active">Избранные</li>
</ol>
<h2>Избранные</h2>

<div class="clearfix favorite-product-list">
    @include('index.catalog.product-list-loop')
</div>

<style>
    .favorite-product-list > .row{
        margin: 0px;
    }
</style>