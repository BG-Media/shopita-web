@extends('layout.admin-layout')

@section('content')

<div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
            <h3 class="box-title" style="margin-top: 5px;font-weight: 600">Заказы</h3>
            </br>
            </br>
            <h3 class="box-title" style="margin-top: 5px; margin-right: 30px">
                <a href="?" style="color: #333333; @if(!isset($request->status_id)) {{ 'color: #367FA9; text-decoration:underline' }}@endif">Все заказы({{ $all }})</a>
            </h3>
            <h3 class="box-title" style="margin-top: 5px; margin-right: 30px">
                <a href="?status_id=2" style="color: #333333; @if($request->status_id == 2) {{ 'color: #367FA9; text-decoration:underline' }}@endif">Ожидает({{ $waiting }})</a>
            </h3>
            <h3 class="box-title" style="margin-top: 5px; margin-right: 30px">
                <a href="?status_id=4" style="color: #333333; @if($request->status_id == 4) {{ 'color: #367FA9; text-decoration:underline' }}@endif">Доставлено({{ $delivered }})</a>
            </h3>
            <h3 class="box-title" style="margin-top: 5px; margin-right: 30px">
                <a href="?status_id=5" style="color: #333333; @if($request->status_id == 5) {{ 'color: #367FA9; text-decoration:underline' }}@endif">Отменено({{ $canceled }})</a>
            </h3>
        </div>
        <div style="padding-top: 10px; border-top: 1px solid rgb(127, 127, 127);">
            <div class="form-group col-md-3" >
              <label>Поиск</label>
              <input id="search_word" value="{{$request->search}}" type="text" class="form-control" name="search_word" placeholder="Введите">
            </div>
            <div class="form-group col-md-3" style="padding-top: 25px" >
              <a href="javascript:void(0)" onclick="searchBySort()">
                  <button type="button" class="btn btn-block btn-success">Поиск</button>
              </a>
            </div>
            </div>
        <div style="clear: both"></div>
        <div class="box-body">
          <table id="news_datatable" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>№</th>
                    <th>Имя, Email и телефон</th>
                    <th>Товары</th>
                    <th>Дата/Коммент</th>
                    <th>Статус</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                  @foreach($row as $key => $val)

                      <tr class="request_id_{{$val->request_id}}">
                          <input type="hidden" class="request_id" value="<?=$val['request_id']?>">
                          <td><?=$val['request_id']?></td>
                          <td>
                              <span style="font-weight: 600"><?=$val['user_name']?></span></br>
                              <?=$val['email']?></br>
                              <?=$val['phone']?>
                          </td>
                          <?
                          $product_list = \App\Models\RequestProduct::leftJoin('product','product.product_id','=','request_product.product_id')
                                  ->where('request_id',$val->request_id)
                                  ->select('request_product.*','product.*')
                                  ->get();
//                              dd($product_list);

                          $product_sum = \App\Models\RequestProduct::leftJoin('product','product.product_id','=','request_product.product_id')
                                  ->where('request_id',$val->request_id)
                                  ->sum(DB::raw('price * unit'));
                          ?>

                          <td>
                              <table class="table-list-request">
                                  <tr class="table-head">
                                      <td style="width: 250px">Товар</td>
                                      <td style="width: 300px">Адрес</td>
                                      <td>Цена</td>
                                      <td>Размер</td>
                                      <td>Кол.</td>
                                      <td>Сумма</td>
                                  </tr>
                                  @foreach($product_list as $count => $value)

                                      <tr style="<?if($count % 2 == 0){ echo 'background-color: white';}?>">
                                          <td>
                                              <a target="_blank" style="text-decoration: underline; color: #3c5988" href="/product/{{$value->product_url}}-u{{$value->product_id}}">
                                                  <?=$value['product_name_ru']?>
                                              </a>
                                              </br><span style="font-weight: 600">Артикул:</span> <?=$value['product_code']?>
                                          </td>
                                          <td>
                                              {{$value->getAddress($value->request_id)}}
                                          </td>
                                          <td>
                                              <?=$value['price']?>тг
                                          </td>
                                          <td>
                                           @if(isset($value['product_size'])){{$value['product_size']}}@else Пустой @endif
                                          </td>
                                          <td>
                                              <?=$value['unit']?>
                                          </td>
                                          <td>
                                              <?=($value['price'] * $value['unit'])?>тг
                                          </td>
                                      </tr>

                                  @endforeach

                                  <tr class="table-request-sum" style="text-align: right; background-color: #d8d8d8;">
                                      <td colspan="5" >
                                          Общая сумма:<span style="font-weight: bold"> <?=$product_sum?>тг</span>
                                      </td>
                                  </tr>

                              </table>
                          </td>
                          <td>
                              <div style="margin-bottom: 3px">
                                  <?=$val['date']?>
                              </div>
                              <div style="font-weight: 600">
                                  <span class="request_comment"><?=$val['comment']?></span>
                              </div>
                          </td>
                          <td>
                              <input type="hidden" value="{{$val->status_id}}" class="status_id">
                              @if($val->status_id == 4)
                                  <div class="label-status label label-success">Доставлено</div>
                              @elseif($val->status_id == 2 && $val->is_pickup == 0)
                                  <div class="label-status label label-warning">Ожидает доставки</div>
                              @elseif($val->status_id == 5)
                                  <div class="label-status label label-danger">Отменено</div>
                              @elseif($val->status_id == 2 && $val->is_pickup == 1)
                                  <div class="label-status label label-warning">Ожидает</div>
                              @endif

                              @if($val->is_epay == 1)
                                  <div style="margin-top: 3px">
                                      <span class="label label-primary">Онлайн оплата</span>
                                  </div>

                                  @if($val->is_epay_success == 1)
                                      <div style="margin-top: 3px">
                                          <span class="label label-success">Оплачено</span>
                                      </div>
                                  @else
                                      <div style="margin-top: 3px">
                                          <span class="label label-danger">Не оплачено</span>
                                      </div>
                                  @endif


                              @endif


                              @if($val->is_pickup == 1)
                                  <div style="margin-top: 3px">
                                      <span class="label label-primary" style="background-color: #00C0EF !important;">Самовывоз</span>
                                  </div>
                              @endif

                          </td>
                          <td style="text-align: center">
                              <a href="javascript:void(0)" onclick="showEditForm('{{$val->request_id}}',this)">
                                  <li class="fa fa-pencil" style="font-size: 20px;"></li>
                              </a>
                              <a href="javascript:void(0)" onclick="delItem(this,'{{ $val->request_id }}')">
                                  <li class="fa fa-trash-o" style="font-size: 20px; color: red;"></li>
                              </a>
                          </td>
                      </tr>

                  @endforeach

            </tbody>

          </table>

            @if($row->lastPage() > 1)

                <div class="page_number" style="text-align: center">
                    <ul class="pagination">
                        <li>
                            <a @if($row->currentPage() > 1) href="?{{preg_replace('~(\?|&)page=[^&]*~','',http_build_query($_GET))}}&page={{ $row->currentPage() - 1 }}" @endif >&laquo;</a>
                        </li>
                        <?
                        $start = 1;
                        if($row->currentPage() >= 10){
                            $start = $row->currentPage();
                        }
                        $finish = $start + 9;
                        if($finish > $row->lastPage()){
                            $finish = $row->lastPage();
                            $start = $finish - 10;
                            if($start <= 0){
                                $start = 1;
                            }
                        }
                        ?>
                        @for ($i = $start; $i <= $finish; $i++)
                            <li class="{{ ($row->currentPage() == $i) ? 'active-paginator' : '' }}">
                                <a href="?{{preg_replace('~(\?|&)page=[^&]*~','',http_build_query($_GET))}}&page={{ $i }}">{{ $i }}</a>
                            </li>
                        @endfor
                        <li>
                            <a @if($row->currentPage() < $row->lastPage()) href="?{{preg_replace('~(\?|&)page=[^&]*~','',http_build_query($_GET))}}&page={{ $row->currentPage() + 1 }}" @endif>&raquo;</a>
                        </li>
                    </ul>
                </div>

            @endif

        </div>
      </div>
    </div>
    </div>

    <div class="box box-info modal-form" id="item_modal">
        <div class="box-header">
            <h3 class="box-title">Изменить</h3>
        </div>
        <div class="box-body">
            <input type="hidden" value="0" id="request_id">
            <div class="form-group">
                <label>Комментарий</label>
                <textarea id="request_comment" class="form-control"></textarea>
            </div>
            <div class="form-group">
                <label>Статус</label>
                <select class="form-control" id="request_status_id" style="resize: none">
                    <option value="4">Доставлено</option>
                    <option value="2">Ожидает доставки</option>
                    <option value="5">Отменено</option>
                </select>
            </div>
            <div class="box-footer" style="text-align: center">
                <a href="javascript:void(0)" onclick="editRequest()">
                    <button type="submit" class="btn btn-primary" style="width: 200px">Сохранить</button>
                </a>
            </div>
        </div>
    </div>


<script src="/admin/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<link href="/fancybox/jquery.fancybox.css" type="text/css" rel="stylesheet">
<script src="/fancybox/jquery.fancybox.pack.js" type="text/javascript"></script>

<meta name="csrf-token" content="{{ csrf_token() }}">

<script>
    function delItem(ob,id){
        if(confirm('Действительно хотите удалить?')){
            $(ob).closest('tr').remove();
            $.ajax({
                type: 'DELETE',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/admin/order/" + id,
                success: function(data){

                }
            });
        }
    }

    function selectAllCheckbox(ob) {
        if ($(ob).is(':checked')) {
            $('.select-all').prop('checked', true);
        }
        else {
            $('.select-all').prop('checked', false);
        }

    }

    function deleteAll() {
        if(confirm('Действительно хотите удалить?')){
            $('.ajax-loader').fadeIn(100);
            $('.select-all').each(function(){
                if ($(this).is(':checked')) {
                    $.ajax({
                        type: 'DELETE',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: "/admin/banner/" + $(this).val(),
                        success: function(){

                        }
                    });
                    $(this).closest('tr').remove();
                }
            });
            $('.ajax-loader').fadeOut(100);
        }
    }

    function isShowDisabledAll() {
        if(confirm('Действительно хотите скрыть?')){
            $('.ajax-loader').fadeIn(100);
            $('.select-all').each(function(){
                if ($(this).is(':checked')) {
                    $.ajax({
                        type: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data :{
                            is_show: 0,
                            id: $(this).val()
                        },
                        url: "/admin/banner/is_show",
                        success: function(data){

                        }
                    });
                    $(this).closest('tr').remove();
                }
            });
            $('.ajax-loader').fadeOut(100);
        }
    }

    function isShowEnabledAll() {
        if(confirm('Действительно хотите сделать активным?')){
            $('.ajax-loader').fadeIn(100);
            $('.select-all').each(function(){
                if ($(this).is(':checked')) {
                    $.ajax({
                        type: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data :{
                            is_show: 1,
                            id: $(this).val()
                        },
                        url: "/admin/banner/is_show",
                        success: function(data){

                        }
                    });
                    $(this).closest('tr').remove();
                }
            });
            $('.ajax-loader').fadeOut(100);
        }
    }

    $( "#search_word" ).keyup(function(event) {
        if (!event.ctrlKey && event.which == 13) {
            searchBySort();
        }
    });

    function searchBySort() {
        href = '?{{preg_replace('~(\?|&)search=[^&]*~','',http_build_query($_GET))}}&search=' + $('#search_word').val();
        window.location.href = href;
    }

    function showEditForm(request_id,ob) {
        $('#request_comment').html($(ob).closest('tr').find('.request_comment').html());
        $('#request_status_id').val($(ob).closest('tr').find('.status_id').val());
        $('#request_id').val(request_id);
        $('#item_modal').fadeIn(100);
        $('.blur').fadeIn(100);
    }

    function editRequest(){
        $.ajax({
            url:'/admin/order/status',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                status_id: $('#request_status_id').val(),
                request_id: $('#request_id').val(),
                comment: $('#request_comment').val()
            },
            success: function () {
                $('.request_id_' + $('#request_id').val()).find('.request_comment').html($('#request_comment').val());
                $('.request_id_' + $('#request_id').val()).find('.label-status').removeClass('label-success');
                $('.request_id_' + $('#request_id').val()).find('.label-status').removeClass('label-warning');
                $('.request_id_' + $('#request_id').val()).find('.label-status').removeClass('label-danger');

                if($('#request_status_id').val() == 4){
                    $('.request_id_' + $('#request_id').val()).find('.label-status').addClass('label-success');
                    $('.request_id_' + $('#request_id').val()).find('.label-status').html('Доставлено');
                }
                else if($('#request_status_id').val() == 2){
                    $('.request_id_' + $('#request_id').val()).find('.label-status').addClass('label-warning');
                    $('.request_id_' + $('#request_id').val()).find('.label-status').html('Ожидает доставки');
                }
                else if($('#request_status_id').val() == 5){
                    $('.request_id_' + $('#request_id').val()).find('.label-status').addClass('label-danger');
                    $('.request_id_' + $('#request_id').val()).find('.label-status').html('Отменено');
                }
                $('.request_id_' + $('#request_id').val()).find('.status_id').val($('#request_status_id').val());

                $('#item_modal').fadeOut(100);
                $('.blur').fadeOut(100);
            }
        });
    }

</script>

<script type="text/javascript">
    $('a.fancybox').fancybox({
        padding: 10
    });
</script>

<style>
    .table-list-request {
        border: 1px solid #565656;
        width: 500px;
    }
    .table-list-request td {
        padding: 5px;
        border-right: 1px solid #565656;
        border-bottom: 1px solid #565656 !important;
        border-bottom-width: 1px;
    }
    .table-head {
        background-color: #3C8DBC;
        color: white;
        border-color: black;
        text-align: center;
    }
    .label {
        border-radius: 6px;
        display: block;
        padding: 6px;
        width: 115px;
    }
</style>


@endsection