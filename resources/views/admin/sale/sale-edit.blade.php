@extends('layout.admin-layout')

@section('content')

        <section class="content-header">
            <h1>
              {{ $title }}
            </h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-8" style="padding-left: 0px">
                        <div class="box box-primary">
                    @if (isset($error))
                        <div class="alert alert-danger">
                           {{ $error }}
                        </div>
                    @endif

                    <div id="error_text" class="alert alert-danger" style="display: none"></div>

                    @if($row->page_id > 0)
                        <form action="/admin/sale/{{$row->page_id}}" method="POST">
                            <input type="hidden" name="_method" value="PUT">
                    @else
                        <form action="/admin/sale" method="POST">
                    @endif
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="page_id" value="{{ $row->page_id }}">
                            <input type="hidden" value="{{ $row->page_image }}" name="page_image" id="page_image">

                            <div class="box-body">
                                <div class="form-group">
                                    <label>Название</label>
                                    <input value="{{ $row->page_name_ru }}" type="text" class="form-control" name="page_name_ru" placeholder="Введите">
                                </div>
                                <div class="form-group">
                                  <label>Текст</label>
                                  <textarea id="page_text_ru" name="page_text_ru" class="form-control"><?=$row->page_text_ru?></textarea>
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Сохранить</button>
                            </div>
                        </form>
                    </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box box-primary" style="padding: 30px; text-align: center">
                            <div style="border-radius: 5%; padding: 20px; border: 1px solid #c2e2f0">
                                <img id="avatar_img" src="/image/sale/{{ $row->page_image }}" style="width: 100%; border-radius: 5%;"/>
                            </div>
                            <div style="background-color: #c2e2f0;height: 40px;margin: 0 auto;width: 2px;"></div>
                            <form id="sale_image_form" enctype="multipart/form-data" method="post" class="avatar-form">
                                <i class="fa fa-plus"></i>
                                <input id="avatar-file" type="file" onchange="uploadImage()" name="image"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <link href="/wysiwyg/default.css" rel="stylesheet"/>
        <script type="text/javascript" src="/wysiwyg/kindeditor.js"></script>
        <script type="text/javascript" src="/wysiwyg/ru_Ru.js"></script>

        <script type="text/javascript">

            KindEditor.ready(function(K) {
                K.create('#page_text_kz,#page_text_ru,#page_text_en', {

                    cssPath : [''],
                    autoHeightMode : true, // это автоматическая высота блока
                    afterCreate : function() {
                        this.loadPlugin('autoheight');
                    },
                    allowFileManager : true,
                    items : [// Вот здесь задаем те кнопки которые хотим видеть
                        'source', '|', 'undo', 'redo', '|', 'preview', 'print', 'template', 'code', 'cut', 'copy', 'paste',
                        'plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 'justifyright',
                        'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript',
                        'superscript', 'clearhtml', 'quickformat', 'selectall', '|', 'fullscreen', '/',
                        'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold',
                        'italic', 'underline', 'strikethrough', 'lineheight', 'removeformat', '|', 'image', 'multiimage',
                        'flash', 'media', 'insertfile', 'table', 'hr', 'emoticons','pagebreak',
                        'anchor', 'link',  'unlink','map', '|', 'about'
                    ]
                });
                //Ниже инициализируем доп. например выбор цвета или загрузка файла
                var colorpicker;
                K('#colorpicker').bind('click', function(e) {
                    e.stopPropagation();
                    if (colorpicker) {
                        colorpicker.remove();
                        colorpicker = null;
                        return;
                    }
                    var colorpickerPos = K('#colorpicker').pos();
                    colorpicker = K.colorpicker({
                        x : colorpickerPos.x,
                        y : colorpickerPos.y + K('#colorpicker').height(),
                        z : 19811214,
                        selectedColor : 'default',
                        noColor : 'Очистить',
                        click : function(color) {
                            K('#color').val(color);
                            colorpicker.remove();
                            colorpicker = null;
                        }
                    });
                });
                K(document).click(function() {
                    if (colorpicker) {
                        colorpicker.remove();
                        colorpicker = null;
                    }
                });

                var editor = K.editor({
                    allowFileManager : true
                });
            });

        </script>

        <script src="/javascript/jquery-2.1.1.min.js"></script>
        <script src="/javascript/jquery.maskedinput.js"></script>
        <script src="/javascript/uploadfile.js"></script>


        <script>
            var cw = $('#avatar_img').width();
            $('#avatar_img').css('height',cw);

            function uploadImage(){
                $("#sale_image_form").submit();
            }
        </script>
@endsection

