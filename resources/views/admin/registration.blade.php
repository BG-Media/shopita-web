<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Priviau</title>
</head>
<body>
<img src="http://shopita.kz/image/logo.png" alt=""><br>
Здравствуйте!
Вы зарегистрировались на сайте shopita.kz
Сохраните Ваш логин и пароль для входа в личный кабинет, при повторном входе на сайт рекомендуем Вам сменить пароль в личном кабинете.

Ваши регистрационные данные:
------------------------------------------------------------------
Логин: {{$data['email']}}
Пароль: {{$data['password']}}
------------------------------------------------------------------

<p>+7 (727) 452 369</p>
<p>support@shopita.kz</p>
<p>24 часов / 7 дней</p>

Не отвечайте на это письмо! Если у Вас есть вопросы, посетите раздел Помощь и Контакты.

</body>
</html>