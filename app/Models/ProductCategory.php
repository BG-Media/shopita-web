<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ProductCategory extends Model
{
    protected $table = 'product_category';
    protected $primaryKey = 'product_category_id';

    public function checkExistProductCategory($product_id,$category_id){
        $product_category = ProductCategory::where('product_id','=',$product_id)
            ->where('category_id','=',$category_id)
            ->count();
        return $product_category;
    }

    public function getProductCategory($id){
        $product_category = ProductCategory::leftJoin('category as subcategory','subcategory.category_id','=','product_category.category_id')
            ->leftJoin('category','subcategory.main_category_id','=','category.category_id')
            ->leftJoin('category as main_category','category.main_category_id','=','main_category.category_id')
            ->where('product_category.product_id','=',$id)
            ->select('subcategory.category_id as subcategory_id',
                     'subcategory.category_name_ru as subcategory_name_ru',
                     'category.category_id as category_id',
                     'category.category_name_ru as category_name_ru',
                     'main_category.category_id as main_category_id',
                     'main_category.category_name_ru as main_category_name_ru',
                     'category.has_child'
            )
            ->get();
        
        return $product_category;
    }
}
