@extends('layout.admin-layout')

@section('content')

    <script src="/admin/plugins/jQuery/jQuery-2.1.4.min.js"></script>

        <section class="content-header">
            <h1>
              {{ $title }}
            </h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-12" style="padding-left: 0px">
                        <div class="box box-primary">
                    @if (isset($error))
                        <div class="alert alert-danger">
                           {{ $error }}
                        </div>
                    @endif

                    <div id="error_text" class="alert alert-danger" style="display: none"></div>

                    @if($row->category_id > 0)
                        <form action="/admin/category/{{$row->category_id}}" method="POST">
                            <input type="hidden" name="_method" value="PUT">
                    @else
                        <form action="/admin/category" method="POST">
                    @endif
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" id="category_id" name="category_id" value="{{ $row->category_id }}">
                            <input type="hidden" value="{{ $row->category_image }}" name="category_image" id="category_image">

                            <div class="box-body">
                                <div class="form-group">
                                    <label>Название</label>
                                    <input value="{{ $row->category_name_ru }}" type="text" class="form-control" name="category_name_ru" placeholder="Введите">
                                </div>
                                <div class="form-group">
                                    <label>Главная категория</label>
                                    <select class="form-control" name="main_category_id">

                                        @foreach($main_category as $val)
                                            <option @if($row->main_category_id == $val->category_id) {{'selected'}} @endif value="{{$val->category_id}}">{{$val['category_name_ru']}}</option>
                                        @endforeach

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Отбражать на главной странице</label>
                                    <select class="form-control" name="is_show_main">
                                        <option value="0" @if($row->is_show_main == '0') selected @endif>Нет</option>
                                        <option value="1" @if($row->is_show_main == '1') selected @endif>Да</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Имеет подкатегорию</label>
                                    <select class="form-control" name="has_child">
                                        <option value="1" @if($row->has_child == '1') selected @endif>Да</option>
                                        <option value="0" @if($row->has_child == '0') selected @endif>Нет</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Сортировка</label>
                                    <input value="{{ $row->sort_num }}" type="text" class="form-control" name="sort_num" placeholder="Введите">
                                </div>
                                <div class="feature-list">
                                    <div class="feature-content" id="feature_content_<?if(isset($row->category_feature[0])){ echo $row->category_feature[0]->feature_id; }?>">
                                        <div class="feature-content-first">
                                            <div class="form-group col-md-4" style="padding-left: 0px">
                                                <label>Выберите характеристику для сортировки</label>
                                                <select onchange="getItemListByFeature(this,0)" name="feature_id[]" data-placeholder="Выберите" class="form-control">
                                                    <option value="0"></option>

                                                    @foreach($feature as $val)
                                                        <option @if(isset($row->category_feature[0]) && $row->category_feature[0]->feature_id == $val->feature_id) {{'selected'}} @endif value="{{$val->feature_id}}">{{$val['feature_name_ru']}}</option>
                                                    @endforeach

                                                </select>
                                            </div>
                                            <div class="form-group col-md-1" style="padding-top: 32px">
                                                <a href="javascript:void(0)" onclick="deleteFeature(this)">
                                                    <i class="fa fa-times" style="font-size: 19px"></i>
                                                </a>
                                            </div>
                                            <div style="clear: both"></div>
                                        </div>

                                        <div class="feature-content-second">

                                            @if(isset($row->category_feature[0]))

                                                <script>
                                                    $(document).ready(function(){
                                                        getItemListByFeature(this,'{{$row->category_feature[0]->feature_id}}');
                                                    });
                                                </script>

                                            @endif

                                        </div>

                                    </div>

                                    @if(isset($row->category_feature))

                                        @foreach($row->category_feature as $key => $value)

                                            @if($key > 0)

                                                <div class="feature-content" id="feature_content_{{$value->feature_id}}">
                                                    <div class="feature-content-first">
                                                        <div class="form-group col-md-4" style="padding-left: 0px">
                                                            <label>Выберите характеристику</label>
                                                            <select onchange="getItemListByFeature(this,0)" name="feature_id[]" data-placeholder="Выберите" class="form-control">
                                                                <option></option>

                                                                @foreach($feature as $val)
                                                                    <option @if($value->feature_id == $val->feature_id) {{'selected'}} @endif value="{{$val->feature_id}}">{{$val['feature_name_ru']}}</option>
                                                                @endforeach

                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md-1" style="padding-top: 32px">
                                                            <a href="javascript:void(0)" onclick="deleteFeature(this)">
                                                                <i class="fa fa-times" style="font-size: 19px"></i>
                                                            </a>
                                                        </div>
                                                        <div style="clear: both"></div>
                                                    </div>

                                                    <div class="feature-content-second">


                                                        <script>
                                                            $(document).ready(function(){
                                                                getItemListByFeature(this,'{{$value->feature_id}}');
                                                            });
                                                        </script>


                                                    </div>

                                                </div>

                                            @endif

                                        @endforeach

                                    @endif

                                </div>
                                <div class="form-group">
                                    <a href="javascript:void(0)" onclick="addFeatureBlock()">
                                        <button type="button" class="btn btn-success">Добавить характеристику</button>
                                    </a>
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Сохранить</button>
                            </div>
                        </form>
                    </div>
                    </div>
                </div>
            </div>
        </section>

        <script>
            function addFeatureBlock(){
                $('.feature-list').append($('.feature-content:first').clone());
                $('.feature-content-second:last').html('');
                $('.feature-content-first:last').find('select').val(0);
            }

            function deleteFeature(ob){
                if($('.feature-content').length == 1){
                    return;
                }
                $(ob).closest('.feature-content').remove();
            }

            function getItemListByFeature(ob,feature_id){
                if(feature_id == 0){
                    feature_id = ob.value;
                }
                else {
                    feature_id = feature_id;
                    ob = $('#feature_content_' + feature_id).find('.feature-content-second');
                }

                $.ajax({
                    url:'/admin/feature-item',
                    type: 'GET',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        feature_id: feature_id,
                        is_product_feature_loop: 1,
                        is_category: 1,
                        category_id: $('#category_id').val()
                    },
                    success: function (data) {
                        $(ob).closest('.feature-content').find('.feature-content-second').html(data);
                    }
                });
            }

        </script>
    <style>
        .category-list, .feature-list {
            border: 1px solid #7e7e7e;
            margin-bottom: 10px;
            padding-left: 20px;
            padding-top: 7px;
            padding-right: 20px;
        }
        .feature-list {
            margin-top: 50px;
        }
        .category-select-content {
            border-top: 1px solid #7e7e7e;
            padding-top: 8px;
        }

        .feature-content {
            border-top: 1px solid #7e7e7e;
            padding-bottom: 42px;
            padding-top: 45px;
        }

        .category-select-content:first-child, .feature-content:first-child {
            border: none;
            padding-top: 8px;
        }

    </style>
@endsection

