@extends('layout.admin-layout')

@section('content')

<div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title" style="margin-top: 5px">Подписка</h3>
          <a href="/admin/subscriber/create" style="float: right">
              <button class="btn btn-primary" style="margin-bottom: 10px; ">Добавить новую подписку</button>
          </a>
        </div>

        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr style="border: 1px">
                <th style="width: 30px">№</th>
                <th>Почта</th>
                <th>Дата</th>
                <th style="width: 30px"></th>
              </tr>
            </thead>

            <tbody>

                  @foreach($row as $key => $val)

                     <tr>
                        <td> {{ $key + 1 }}</td>
                        <td>
                           {{ $val->email }}
                        </td>
                        <td>
                           {{ $val->date }}
                        </td>
                        <td style="text-align: center">
                            <a href="javascript:void(0)" onclick="delItem(this,{{ $val->subscriber_id }})">
                                <li class="fa fa-trash-o" style="font-size: 20px; color: red;"></li>
                            </a>
                        </td>
                     </tr>

                  @endforeach

            </tbody>

          </table>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
    </div><!-- /.row -->

    <meta name="csrf-token" content="{{ csrf_token() }}">

       <script>
           function delItem(ob,id){
               if(confirm('Действительно хотите удалить?')){
                   $(ob).closest('tr').remove();
                   $.ajax({
                       type: 'DELETE',
                       headers: {
                           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                       },
                       url: "/admin/subscriber/" + id,
                       success: function(data){

                       }
                   });
               }
           }
       </script>

@endsection