<?php

namespace App\Http\Controllers\Api;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\Users;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\Http\Controllers\Controller;
use App\Helpers\HttpHelper;
use Auth;
use Storage;
use File;

class AccountController extends Controller
{
    public function signUp(Request $request)
    {
        $input = $request->all();
        $valid = Validator::make($input, [
            'email' => 'required|email|unique:users,email|max:255',
            'password' => 'required|min:4',
            'name' => 'required',
            'surname' => 'required',
        ]);

        if ($valid->fails()) {
            return HttpHelper::showErrors($valid->errors());
        }

        $data = Users::create([
            'email' => $input['email'],
            'password' => bcrypt($input['password']),
            'name' => $input['name'],
            'surname' => $input['surname'],
            'api_token' => bcrypt($input['email']),
        ]);

        return HttpHelper::showSuccess($data);
    }

    public function signIn(Request $request)
    {
        $input = $request->all();
        $valid = Validator::make($input, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($valid->fails()) {
            return HttpHelper::showErrors($valid->errors());
        }

        $user = Users::where('email', $input['email'])->first();

        if (count($user) == 0) {
            return HttpHelper::showError('Пользователь не найден');
        }

        if (!Hash::check($input['password'], $user->password)) {
            return HttpHelper::showError('Логин или пароль неверен');
        }

        return HttpHelper::showSuccess($user);
    }

    public function resetPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email'
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return HttpHelper::showErrors($error);
        }

        $user = Users::where('email', $request->email)->first();
        if ($user === null) {
            $result['error'] = 'Пользователь с указанной почтой не существует';
            return HttpHelper::showError('Пользователь с указанной почтой не существует');
        }

        $new_password = str_random(8);
        $password = Hash::make($new_password);
        $user->password = $password;
        $user->save();

        $email = $request->email;
        $userdata['user'] = $user->email;
        $userdata['password'] = $new_password;
        \Mail::send('admin.reset', ['data' => $userdata, 'email' => $email], function ($m) use ($userdata, $email) {
            $m->from(env('MAIL_USERNAME'), 'Shopita.kz');
            $m->to($email, 'info@shopita.kz')->subject(' Восстановление пароля');
        });

        return HttpHelper::showSuccess('Ваш новый пароль успешно отправлен на почту');
    }

    public function edit(Request $request)
    {
        $input = $request->all();
        $valid = Validator::make($input, [
            'email' => 'email|unique:users,email,' . $request->safeUser->user_id . ',user_id|max:255',
            'mobile_phone' => 'unique:users,mobile_phone,' . $request->safeUser->user_id . ',user_id|max:255',
        ]);

        if ($valid->fails()) {
            return HttpHelper::showErrors($valid->errors());
        }

        if ($request->hasFile('avatar')) {
            $file = $request->avatar;
            $file_name = time() . "_user.";
            $file_extension = $file->extension($file_name);
            $file_name = $file_name . $file_extension;
            Storage::disk('avatar')->put($file_name, File::get($file));
            $input['avatar'] = '/image/avatar/' . $file_name;
        }

        $request->safeUser->update($input);

        return HttpHelper::showSuccess($request->safeUser);
    }

    public function changePassword(Request $request)
    {
        $count = Hash::check($request->old_password, $request->safeUser->password);

        if ($count == false) {
            return HttpHelper::showError('Неправильный старый пароль');
        }

        $valid = Validator::make($request->all(), [
            'old_password' => 'required',
            'new_password' => 'required|different:old_password',
            'confirm_password' => 'required|same:new_password',
        ]);

        if ($valid->fails()) {
            return HttpHelper::showErrors($valid->errors());
        }

        $request->safeUser->password = Hash::make($request->new_password);
        $request->safeUser->save();

        return HttpHelper::showSuccess('Успешно изменен');
    }

    public function profile(Request $request)
    {
        return HttpHelper::showSuccess([
            'name' => $request->safeUser->name,
            'surname' => $request->safeUser->surname,
            'email' => $request->safeUser->email,
            'sex' => $request->safeUser->sex,
            'mobile_phone' => $request->safeUser->mobile_phone,
            'avatar' => $request->safeUser->avatar,
            'address' => $request->safeUser->address,
            'myOrders' => [],
        ]);
    }

    public function favorites(Request $request)
    {
        Auth::loginUsingId($request->safeUser->user_id);
        $products = $request->safeUser->favorites()->get();
        return HttpHelper::showSuccess($products);
    }

    public function myOrders(Request $request)
    {
        $row = \App\Models\Request::where('user_id', $request->safeUser->user_id)
            ->orderBy('created_at','desc')
            ->get(['request_id', 'status_id', 'price', 'created_at', 'address', 'is_pickup', 'is_epay', 'is_epay_success']);

        $result = [];
        foreach($row as $item)
        {
            $items = \App\Models\RequestProduct::where('request_id', $item->request_id)->get();
            $products = [];
            foreach($items as $product)
            {
                $products[] = [
                    'id' => $product->product_id,
                    'unit' => $product->unit,
                    'name' => Product::find($product->product_id)->product_name_ru,
                ];
            }
            $result[] = [
                'id' => $item->request_id,
                'status_id' => $item->status_id,
                'total_price' => $item->price,
                'address' => $item->address,
                'created_at' => date('d.m.Y, H:i', $item->created_at->getTimestamp()),
                'pickup' => $item->is_pickup,
                'paymentOnline' => $item->is_epay,
                'paymentSuccess' => $item->is_epay_success,
                'products' => $products,
            ];
        }

        return HttpHelper::showSuccess($result);
    }

    public function fromSocial(Request $request)
    {
        $user_info = $request->all();
        $user = Users::where('social_link','=',$user_info['identity'])->first();
        if($user === null){
            $user = new Users();
            $name = '';
            if(isset($user_info['last_name'])){
                $user->surname = $user_info['last_name'] .' ';
            }
            if(isset($user_info['first_name'])){
                $user->name = $user_info['first_name'];
            }
            //$user->name = $name;

            if(isset($user_info['email'])){
                $user_check = Users::where('email','=',$user_info['email'])->first();
                if($user_check == null){
                    $user->email = $user_info['email'];
                }
            }

            if(isset($user_info['photo_big'])){
                $user->avatar = $user_info['photo_big'];
            }
            else if(isset($user_info['photo'])){
                $user->avatar = $user_info['photo'];
            }
            if(isset($user_info['phone'])){
                $user->phone = $user_info['phone'];
            }
            $user->password = Hash::make('123456');
            $user->role_id = 2;
            $user->is_social = 1;
            $user->social_link = $user_info['identity'];
            $user->api_token = bcrypt($user_info['identity']);
            $user->save();
        }

        return HttpHelper::showSuccess($user);

    }
}