@extends('layout.admin-layout')

@section('content')
    {{--{{ dd($text) }}--}}

    <section class="content-header">
        <h1>
            {{ $title }}
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-9" style="padding-left: 0px">
                    <div class="box box-primary">
                        @if (isset($error))
                            <div class="alert alert-danger">
                                {{ $error }}
                            </div>
                        @endif

                        <div id="error_text" class="alert alert-danger" style="display: none"></div>

                        @if($text->id > 0)
                            <form action="/admin/main-page-text/{{$text->id}}" method="POST">
                                <input type="hidden" name="_method" value="PUT">
                        @else
                            <form action="/admin/main-page-text" method="POST">
                        @endif
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="page_text_id" value="{{ $text->id }}">

                                <div class="box-body">
                                    <div class="form-group">
                                        <label>Текст</label>
                                        <textarea id="main-page-text" name="page_text" class="form-control"><?= $text->page_text ?></textarea>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <button type="submit" class="btn btn-primary">Сохранить</button>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <link href="/wysiwyg/default.css" rel="stylesheet"/>
    <script type="text/javascript" src="/wysiwyg/kindeditor.js"></script>
    <script type="text/javascript" src="/wysiwyg/ru_Ru.js"></script>

    <script type="text/javascript">

        KindEditor.ready(function(K) {
            K.create('#main-page-text', {

                cssPath : [''],
                autoHeightMode : true, // это автоматическая высота блока
                afterCreate : function() {
                    this.loadPlugin('autoheight');
                },
                allowFileManager : true,
                items : [// Вот здесь задаем те кнопки которые хотим видеть
                    'source', '|', 'undo', 'redo', '|', 'preview', 'print', 'template', 'code', 'cut', 'copy', 'paste',
                    'plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 'justifyright',
                    'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript',
                    'superscript', 'clearhtml', 'quickformat', 'selectall', '|', 'fullscreen', '/',
                    'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold',
                    'italic', 'underline', 'strikethrough', 'lineheight', 'removeformat', '|', 'image', 'multiimage',
                    'flash', 'media', 'insertfile', 'table', 'hr', 'emoticons','pagebreak',
                    'anchor', 'link',  'unlink','map', '|', 'about'
                ]
            });
            //Ниже инициализируем доп. например выбор цвета или загрузка файла
            var colorpicker;
            K('#colorpicker').bind('click', function(e) {
                e.stopPropagation();
                if (colorpicker) {
                    colorpicker.remove();
                    colorpicker = null;
                    return;
                }
                var colorpickerPos = K('#colorpicker').pos();
                colorpicker = K.colorpicker({
                    x : colorpickerPos.x,
                    y : colorpickerPos.y + K('#colorpicker').height(),
                    z : 19811214,
                    selectedColor : 'default',
                    noColor : 'Очистить',
                    click : function(color) {
                        K('#color').val(color);
                        colorpicker.remove();
                        colorpicker = null;
                    }
                });
            });
            K(document).click(function() {
                if (colorpicker) {
                    colorpicker.remove();
                    colorpicker = null;
                }
            });

            var editor = K.editor({
                allowFileManager : true
            });
        });

    </script>

    <script src="/javascript/jquery-2.1.1.min.js"></script>
    <script src="/javascript/jquery.maskedinput.js"></script>
    <script src="/javascript/uploadfile.js"></script>
@endsection