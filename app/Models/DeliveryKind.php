<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class DeliveryKind extends Model
{
    protected $table = 'delivery_kind';
    protected $primaryKey = 'delivery_kind_id';

    use SoftDeletes;
    protected $dates = ['deleted_at'];
}
