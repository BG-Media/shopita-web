<?
if(isset($row->search)){
    $search = $row->search;
}
else {
    $search = '';
}
$product_row = \App\Models\Product::leftJoin('brand','brand.brand_id','=','product.brand_id')
        ->orderBy('product_id','desc')
        ->where('product.is_show','=','1')
        ->where(function($query) use ($search){
            $query->where('product_name_ru','like','%' .$search .'%')
                    ->orWhere('brand_name','like','%' .$search .'%')
                    ->orWhere('tag','like','%' .$search .'%')
                    ->orWhere('product_code','like','%' .$search .'%');
        })
        ->select('brand.*',
                'product.*'
        )
        ->take(10)
        ->get();
?>

<table class="table table-bordered table-striped">
    <thead>
    <tr style="border: 1px">
        <th style="width: 30px">№</th>
        <th>Название</th>
        <th>Категория</th>
        <th>Бренд</th>
        <th style="width: 30px"></th>
    </tr>
    </thead>

    <tbody>

    @foreach($product_row as $key => $val)

        <tr>
            <td> {{ $key + 1 }}</td>
            <td>
                <a href="/product/{{$val->product_url}}-u{{$val->product_id}}" target="_blank" class="redirect-url">
                    {{ $val->product_name_ru }}
                </a>
            </td>
            <td>
                <?
                $product_category = new \App\Models\ProductCategory();
                $product_category = $product_category->getProductCategory($val->product_id);
                ?>
                @foreach($product_category as $key => $value)
                    <div class="product-category-item">
                        <p style="margin: 0"><span class="num">{{$key + 1}}. </span>{{$value->main_category_name_ru}}</p>
                        <p style="padding-left: 13px; margin:0">{{$value->category_name_ru}}</p>
                        <p style="padding-left: 13px; margin:0">{{$value->subcategory_name_ru}}</p>
                    </div>
                @endforeach
            </td>
            <td>
                {{ $val->brand_name }}
            </td>
            <td style="text-align: center">
                <?
                $product_db = new \App\Models\ProductRecommended();
                $check = $product_db->checkExistProductRecommended($row->product_id,$val->product_id);
                ?>
                @if($check == 0 && $row->product_id != $val->product_id)
                    <a href="javascript:void(0)" onclick="addRecommendedProduct(this,'{{$val->product_id}}')">
                        <li class="fa fa-plus" style="font-size: 20px;"></li>
                    </a>
                @endif
            </td>
        </tr>

    @endforeach

    </tbody>

</table>