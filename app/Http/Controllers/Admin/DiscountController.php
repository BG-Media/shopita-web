<?php

namespace App\Http\Controllers\Admin;

use App\Models\Discount;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Faker\Provider\File;
class DiscountController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');

    }
    
    public function index()
    {
        $list = Discount::paginate(12);
        return view('admin.discount.index', compact('list'));
    }

    public function create()
    {
        return view('admin.discount.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'percent' => 'required|numeric',
            'image' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return  view('admin.discount.create', [
                'error' => $error[0]
            ]);
        }


        $file = $request->file('image');

            $directory = public_path('image/discount/');
            $filename = date("YmdHisu").'--'.uniqid().'.'.$file->extension();


            //$upload_success = $file->storeAs($directory, $filename);
            $upload_success = $file->move($directory, $filename);

            $request->image = '/image/discount/'.$filename;


        Discount::create([
            'name' => $request->name,
            'percent' => $request->percent,
            'image' => $request->image,
        ]);

        return redirect('/admin/discount');
    }

    public function edit($id)
    {
        $item = Discount::find($id);
        $products = Product::where('discount_id', $item->id)->get();
        return view('admin.discount.edit', compact('item', 'products'));
    }

    public function show(Request $request,$id){

    }

    public function update(Request $request,$id)
    {
        $item = Discount::find($id);
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'percent' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return  view('admin.discount.edit', [
                'item' => $item,
                'error' => $error[0]
            ]);
        }


        if ($request->hasFile('image'))
        {
            $file = $request->file('image');

            $directory = public_path('image/discount/');
            $filename = date("YmdHisu").'--'.uniqid().'.'.$file->extension();


            //$upload_success = $file->storeAs($directory, $filename);
            $upload_success = $file->move($directory, $filename);

            $request->image = '/image/discount/'.$filename;
            $item->image = $request->image;
        }

        $item->name = $request->name;
        $item->percent = $request->percent;
        $item->active = $request->active;
        $item->save();

        return redirect('/admin/discount');
    }

    public function destroy($id)
    {

    }

}
