<?php

namespace App\Http\Middleware;

use App\Models\Users;
use Closure;

class AccountMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (empty($request->input('api_token'))) {
            return response()->json([
                'success' => false,
                'error' => 'api token не найден',
            ]);
        }
        $request->safeUser = Users::where('api_token', $request->input('api_token'))->first();
        if (count($request->safeUser) == -0) {
            return response()->json([
                'success' => false,
                'error' => 'Пользователь по api token не найден',
            ]);
        }
        return $next($request);
    }
}
