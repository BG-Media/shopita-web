@extends('layout.admin-layout')

@section('content')

    <section class="content-header">
        <h1>
            Редактирование скидки {{ $item->name }}
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-12" style="padding-left: 0px">
                    <div class="box box-primary">
                        @if (isset($error))
                            <div class="alert alert-danger">
                                {{ $error }}
                            </div>
                        @endif

                        <form action="{{ route('admin.discount.update', ['id' => $item->id]) }}" method="POST"
                              enctype="multipart/form-data">
                            {{ method_field('put') }}

                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="active" value="0"/>

                            <div class="box-body">
                                <div class="form-group">
                                    <label>Название</label>
                                    <input value="{{ $item->name }}" type="text" class="form-control" name="name"
                                           placeholder="Введите">
                                </div>
                                <div class="form-group">
                                    <label>Процент скидки</label>
                                    <input value="{{ $item->percent }}" type="number" class="form-control"
                                           name="percent" placeholder="Введите">
                                </div>
                                <div class="form-group">
                                    <label>Изображение</label>
                                    <div><img style="max-width: 100%" src="{{ $item->image }}"/></div>
                                    <input type="file" class="form-control" name="image" accept="image/jpeg;image/png"
                                           placeholder="Введите">
                                </div>

                                <div class="form-group">
                                    <label>Статус</label><br/>
                                    <input value="1" type="checkbox" name="active" @if ($item->active) checked @endif />
                                    Активен
                                </div>
                            </div>


                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Сохранить</button>
                            </div>
                        </form>
                        <div class="form-group container-fluid" style="margin-top: 40px">
                            <label>Товары</label>
                            <div class="row product_recommended_list">
                                @foreach($products as $val)
                                    <div class="feature-item col-md-4">
                                        <div class="form-group feature-item-left">
                                            <a href="/product/{{$val->product_url}}-u{{$val->product_id}}"
                                               target="_blank">
                                                <label>{{$val->product_name_ru}}</label>
                                            </a>
                                        </div>
                                        <div style="clear: both"></div>
                                    </div>
                                    <div style="clear: both"></div>
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

