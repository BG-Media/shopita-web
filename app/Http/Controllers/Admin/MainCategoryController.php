<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use App\Helpers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class MainCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    public function index(Request $request)
    {
        $search = '';
        if(isset($request->search)){
            $search = $request->search;
        }

        $active = 0;

        if(!isset($request->active)){
            $active = 1;
            $request->active = 1;
        }

        $row = Category::orderBy('category_id','desc')
                ->where('category_level','1')
                ->where('is_show','=',$active)
                ->where(function($query) use ($search){
                    $query->where('category_name_ru','like','%' .$search .'%');
                })
                ->paginate(10);

        return  view('admin.category.main-category',[
            'row' => $row,
            'request' => $request
        ]);
    }

    public function create()
    {
        $row = new Category();
        $row->sort_num = 1;
        return  view('admin.category.main-category-edit', [
                'title' => 'Добавить категорию',
                'row' => $row
            ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_name_ru' => 'required',
            'sort_num' => 'required'
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return  view('admin.category.main-category-edit', [
                'title' => 'Добавить категорию',
                'row' => (object) $request->all(),
                'error' => $error[0]
            ]);
        }

        $category = new Category();
        $category->category_name_ru = $request->category_name_ru;
        $category->category_level = 1;
        $category->is_show_main = $request->is_show_main;
        $category->is_male = $request->is_male;
        $category->sort_num = $request->sort_num;
        $category->category_url = Helpers::getTranslatedSlugRu($request->category_name_ru);
        $category->save();
        return redirect('/admin/main-category');
    }

    public function edit($id)
    {
        $row = Category::find($id);
        return  view('admin.category.main-category-edit', [
            'title' => 'Изменить категорию',
            'row' => $row
        ]);
    }

    public function show(Request $request,$id){

    }

    public function update(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'category_name_ru' => 'required',
            'sort_num' => 'required'
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return  view('admin.category.main-category-edit', [
                'title' => 'Изменить категорию',
                'row' => (object) $request->all(),
                'error' => $error[0]
            ]);
        }

        $category = Category::find($id);
        $category->category_name_ru = $request->category_name_ru;
        $category->is_show_main = $request->is_show_main;
        $category->is_male = $request->is_male;
        $category->sort_num = $request->sort_num;
        $category->category_url = Helpers::getTranslatedSlugRu($request->category_name_ru);
        $category->save();
        return redirect('/admin/main-category');
    }

    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();
    }

    public function changeIsShow(Request $request){
        $user = Category::find($request->id);
        $user->is_show = $request->is_show;
        $user->save();
    }
}
