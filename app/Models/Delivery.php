<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Delivery extends Model
{
    protected $table = 'delivery';
    protected $primaryKey = 'delivery_id';

    use SoftDeletes;
    protected $dates = ['deleted_at'];
}
