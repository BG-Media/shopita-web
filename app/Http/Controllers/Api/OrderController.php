<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Helpers\HttpHelper;
use App\Models\Product;
use App\Models\PromoCode;
use App\Models\RequestProduct;
use Illuminate\Http\Request;
use Auth;

use Illuminate\Support\Facades\Validator;
class OrderController extends Controller
{
    public function addProduct(Request $request, $id)
    {
        $session = csrf_token();
        $request_db = new \App\Models\Request();

        $request_session = \App\Models\Request::where('session', $session)->get();
        foreach ($request_session as $val) {
            $request_row = \App\Models\Request::find($val->request_id);
            $request_row->user_id = $request->safeUser->user_id;
            $request_row->session = null;
            $request_row->save();
        }
        $request_result = $request_db->checkExistRequestByUser($request->safeUser->user_id);
        if ($request_result == null) {
            $request_db->user_id = $request->safeUser->user_id;
            $request_db->status_id = 1;
            $request_db->save();
            $request_id = $request_db->request_id;
        } else {
            $request_id = $request_result->request_id;
        }

        $product = Product::find($id);

        if ($product == null || $product->is_show == '0' || $product->product_count < 1) {
            return HttpHelper::showError('К сожалению, такого товара нет на складе!');
        }

        if (empty($request->size)) {
            return HttpHelper::showError('Выберите размер');
        }

        if ($product->product_count < $request->unit) {
            return HttpHelper::showError('К сожалению, на складе нет такого количества товара!');
        }

        $request_product = new RequestProduct();
        $request_product->product_id = $id;
        $request_product->request_id = $request_id;
        $request_product->price = $product->product_price;
        $request_product->product_size = $request['size'];
        $request_product->unit = $request->unit;
        $request_product->product_feature_desc = 'ok';
        $request_product->save();

        return HttpHelper::showSuccess('Товар успешно добавлен в корзину');
    }

    public function deleteProduct(Request $request, $id)
    {
        $row = \App\Models\Request::where('user_id', '=', $request->safeUser->user_id)
            ->where('status_id', '=', 1)
            ->first();


        $request_id = 0;
        if (isset($row->request_id)) {
            $request_id = $row->request_id;
        }

        RequestProduct::where('request_product_id', $id)->delete();
        Auth::loginUsingId($request->safeUser->user_id);
        $basket = $this->getBasket($request_id);

        return HttpHelper::showSuccess($basket);
    }

    public function setUnit(Request $request, $id)
    {
        $row = \App\Models\Request::where('user_id', '=', $request->safeUser->user_id)
            ->where('status_id', '=', 1)
            ->first();


        $request_id = 0;
        if (isset($row->request_id)) {
            $request_id = $row->request_id;
        }

        $item = RequestProduct::where('request_product_id', $id)->first();
        $product = Product::find($item->product_id);

        $result = $product->isAvailableProduct($product->product_id, $request->unit);
        if ($result['result'] == false) {
            return HttpHelper::showError($result['error']);
        }

        $item->unit = $request->unit;
        $item->save();


        Auth::loginUsingId($request->safeUser->user_id);
        $basket = $this->getBasket($request_id);

        return HttpHelper::showSuccess($basket);
    }

    public function basket(Request $request)
    {
        $row = \App\Models\Request::where('user_id', '=', $request->safeUser->user_id)
            ->where('status_id', '=', 1)
            ->first();


        $request_id = 0;
        if (isset($row->request_id)) {
            $request_id = $row->request_id;
        }
        Auth::loginUsingId($request->safeUser->user_id);

        $basket = $this->getBasket($request_id);

        return HttpHelper::showSuccess($basket);
    }

    public function getBasket($request_id)
    {
        $basket = [];
        $items = RequestProduct::where('request_id', $request_id)
            ->select([
                'request_product_id',
                'product_id',
                'product_size',
                'unit',
            ])
            ->get();

        $totalPrice = 0;

        foreach ($items as $item) {
            $product = Product::find($item->product_id);
            $basket[] = [
                'item' => $item,
                'product' => $product,
            ];

            $totalPrice += $product->current_price['price'] * $item->unit;
        }

        return [
            'basket' => $basket,
            'total_price' => $totalPrice,
        ];
    }

    public function promocode(Request $request)
    {
        $pc = PromoCode::where('code', $request->input('code'))->first();
        if ($pc == null) {
            return HttpHelper::showError('Промокод не найден');
        }

        $result = [
            'type' => ($pc->code_type == 1 ? 'тенге' : '%'),
            'value' => $pc->promo_code,
            'name' => $pc->code_name,
        ];

        return HttpHelper::showSuccess($result);
    }

    public function payment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_name' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
            //'city' => 'required',
            'address' => 'required'
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            $result['error_list'][0]['order_user_name'] = $messages->first('user_name');
            $result['error_list'][1]['order_phone'] = $messages->first('phone');
            $result['error_list'][2]['order_email'] = $messages->first('email');
            //$result['error_list'][3]['order_city'] = 'Укажите город';
            $result['error_list'][3]['order_address'] = 'Укажите адрес';
            return HttpHelper::showError($error[0]);
        }


        $request_row = \App\Models\Request::where('user_id', '=', $request->safeUser->user_id)
            ->where('status_id', '=', 1)
            ->first();


        $request_id = 0;
        if (isset($request_row->request_id)) {
            $request_id = $request_row->request_id;
        }

        $product_list = RequestProduct::leftJoin('product', 'product.product_id', '=', 'request_product.product_id')
            ->where('request_id', $request_id)
            ->select('request_product.*', 'product.product_name_ru')
            ->get();

        $price = 0;
/*
        foreach ($product_list as $item) {
            $product = Product::find($item->product_id);

            $price += $product->current_price['price'] * $item->unit;
        }*/


        $basket = $this->getBasket($request_id);
        $price = $basket['total_price'];
        if ($request->has('promocode'))
        {
            $pc = PromoCode::where('code', $request->input('code'))->first();
            if ($pc == null) {
                return HttpHelper::showError('Промокод не найден');
            }

            if ($pc->code_type == 1) {
                $price -= $pc->promo_code;
            } else {
                $discount = ($pc->promo_code / 100) * $price;
                $price -= $discount;
            }
        }

        if ($request->input('is_pickup') == 1){
            $price += 3000;
        }

        $str_message = '';


        $request_row->status_id = 2;
        $request_row->user_name = $request->user_name;
        $request_row->email = $request->email;
        $request_row->city = $request->city;
        $request_row->address = $request->address;
        $request_row->phone = $request->phone;
        $request_row->is_pickup = $request->is_pickup;
        $request_row->user_id = $request->safeUser->user_id;
        $request_row->session = null;
        $request_row->size = 0;
        $request_row->is_epay = $request->is_pay;
        $request_row->price = $price;
        $request_row->save();

        $result['success'] = true;

        foreach ($product_list as $val) {
            $product = Product::where('product_id', '=', $val->product_id)->first();
            $count = $product->product_count - $val->unit;
            $product->product_count = $count;
            $product->save();
        }

        if ($request_row->is_epay > 0) {
            require_once("kkb_cert/kkb.utils.php");
            $path1 = 'kkb_cert/config.txt';	// Путь к файлу настроек config.dat
            $order_id = $request_row->request_id;				// Порядковый номер заказа - преобразуется в формат "000001"
            $currency_id = "398"; 			// Шифр валюты  - 840-USD, 398-Tenge
            $amount = $price;				// Сумма платежа
            $content = process_request($order_id, $currency_id, $amount, $path1); // Возвращает подписанный и base64 кодированный XML документ для отправки в банк
            $appendix = "";

            $xml = '<document><item number="1" name="Товар" quantity="1" amount="'.$price.'"/></document>';
            $appendix = base64_encode($xml);

            return HttpHelper::showSuccess([
                'msg' => 'Заказ принят',
                'online' => 1,
                'appendix' => $appendix,
                'content' => $content,
            ]);
        }

        return HttpHelper::showSuccess([
            'msg' => 'Заказ принят',
            'online' => 0,
        ]);

    }
}