<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class PageText extends Model
{
    protected $table = 'main_page_text';

    protected $fillable = ['text'];
}
