<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Page;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use App\Helpers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use DB;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    public function index(Request $request)
    {
        $search = '';
        if(isset($request->search)){
            $search = $request->search;
        }

        $status_id = 1;
        $status_cond = '!=';
        if(isset($request->status_id)){
            $status_id = $request->status_id;
            $status_cond = '=';
        }

        $row = \App\Models\Request::where('status_id',$status_cond,$status_id)
            ->select('request.*',
                DB::raw('DATE_FORMAT(request.created_at,"%d.%m.%Y") as date')
            )
            ->where(function($query) use ($search){
                $query->where('user_name','like','%' .$search .'%')
                    ->orWhere('phone','like','%' .$search .'%')
                    ->orWhere('comment','like','%' .$search .'%')
                    ->orWhere('address','like','%' .$search .'%')
                    ->orWhere('email','like','%' .$search .'%');
            })
            ->orderBy('created_at','desc')
            ->paginate(8);

        $all = \App\Models\Request::count();
        $waiting = \App\Models\Request::where('status_id', 2)->count();
        $delivered = \App\Models\Request::where('status_id', 4)->count();
        $canceled = \App\Models\Request::where('status_id', 5)->count();

        return  view('admin.order.order',[
            'row' => $row,
            'request' => $request,
            'all' => $all,
            'waiting' => $waiting,
            'delivered' => $delivered,
            'canceled' => $canceled
        ]);
    }


    public function destroy($id)
    {
        $row = \App\Models\Request::find($id);
        $row->delete();
    }

    public function editRequest(Request $request)
    {
        $row = \App\Models\Request::find($request->request_id);
        $row->status_id = $request->status_id;
        $row->comment = $request->comment;
        $row->save();
    }
}
