<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ProductRecommended extends Model
{
    protected $table = 'product_recommended';
    protected $primaryKey = 'product_recommended_id';

    public function checkExistProductRecommended($product_id,$other_product_id){
        $row = ProductRecommended::where('product_id','=',$product_id)
            ->where('other_product_id','=',$other_product_id)
            ->count();
        return $row;
    }

    public static function getProductRecommended($id){
        $row = ProductRecommended::leftJoin('product','product.product_id','=','product_recommended.other_product_id')
            ->where('product_recommended.product_id','=',$id)
            ->select('product.*')
            ->take(8)
            ->get();
        $items = [];
        foreach ($row as $item) {
            $tmp = Product::find($id);
            //$item->image_url = Image::where('product_id', $id)->where('is_main', 1)->first()->full_path;
            $items[] = $tmp;
        }
        return $items;
    }

    public function getProductRecommendedByToken($token){
        $row = ProductRecommended::leftJoin('product','product.product_id','=','product_recommended.other_product_id')
            ->leftJoin('brand','brand.brand_id','=','product.brand_id')
            ->where('product_recommended.product_recommended_token','=',$token)
            ->select('product.*','brand.*')
            ->get();
        return $row;
    }
}
