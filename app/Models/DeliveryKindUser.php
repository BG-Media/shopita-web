<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class DeliveryKindUser extends Model
{
    protected $table = 'delivery_kind_user';
    protected $primaryKey = 'delivery_kind_user_id';
}
