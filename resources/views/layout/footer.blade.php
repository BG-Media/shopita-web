<div class="row grey">
    <div class="col-md-offset-2 col-md-5 col-sm-7">
        <div class="forms">
            <span class="label1 hidden-xs">Подпишитесь &nbsp;</span>
            <input type="text" class="input2" id="subscribe_email">
            <a href="javascript:void(0)" onclick="subscribeNews()">
                <input type="submit" value="Подписаться" class="help_input">
            </a>
        </div>

    </div>
    <div class="col-md-5 col-sm-5">

        <div class="icons">
            <a target="_blank" href="{{$social[0]->page_text_ru}}">
                <img src="/image/sh_vk.png" alt="">
            </a>
            <a target="_blank" href="{{$social[1]->page_text_ru}}">
                <img src="/image/sh_fb.png" alt="">
            </a>
            <a target="_blank" href="{{$social[2]->page_text_ru}}">
                <img src="/image/sh_ins.png" alt="">
            </a>
        </div>

    </div>
</div>