<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Helpers\HttpHelper;
use App\Models\Category;
use App\Models\Comment;
use App\Models\Discount;
use App\Models\Image;
use App\Models\Product;
use App\Models\ProductFavorite;
use App\Models\ProductFeature;
use App\Models\ProductRecommended;
use Illuminate\Http\Request;
use App\Models\Page;
use Auth;
use DB;
use Illuminate\Support\Facades\Validator;
class ProductController extends Controller
{

    public function sortingProducts($items, $request)
    {
        if ($request->has('min_price'))
        {
            $items = $items->where('product_price', '>', $request->input('min_price'));
        }

        if ($request->has('max_price'))
        {
            $items = $items->where('product_price', '<', $request->input('max_price'));
        }

        if ($request->has('sizes'))
        {
            $items = $items->leftJoin('product_size','product.product_id','=','product_size.product_id')
                ->where(function($q) use ($request){
                    return $q->whereIn('product_size.unique_size', $request->input('sizes'));
                });
        }

        if ($request->has('collections'))
        {
            $items = $items->leftJoin('product_feature','product.product_id','=','product_feature.product_id')
                ->where(function($q) use ($request){
                    return $q->whereIn('product_feature.item_id', $request->input('collections'));
                });
        }

        if ($request->has('brands'))
        {
            $items = $items->where(function($q) use ($request){
                return $q->whereIn('product.brand_id', $request->input('brands'));
            });
        }

        switch($request->input('order'))
        {
            default:
            case 'priceAsc':
                $orderBy = 'product_price';
                $orderDest = 'asc';
                break;
            case 'priceDesc':
                $orderBy = 'product_price';
                $orderDest = 'desc';
                break;
            case 'view_count':
                $orderBy = 'view_count';
                $orderDest = 'desc';
                break;
        }


        return $items->orderBy($orderBy, $orderDest)->get();
    }

    public function productsBySCID(Request $request, $subcategory_id)
    {
        Auth::loginUsingId($request->safeUser->user_id);
        $items = Category::find($subcategory_id)->products();

        $items = $this->sortingProducts($items, $request);

        $result = [
            'count' => count($items),
            'result' => $items,
        ];
        return HttpHelper::showSuccess($result);
    }

    public function byDiscount(Request $request, $id)
    {
        Auth::loginUsingId($request->safeUser->user_id);
        $items = Product::where('discount_id', $id);

        $items = $this->sortingProducts($items, $request);

        $result = [
            'discount' => Discount::find($id),
            'count' => count($items),
            'result' => $items,
        ];
        return HttpHelper::showSuccess($result);
    }

    public function clothes($gender)
    {
        $sex = $gender == 'man';
        $main_category = \App\Models\Category::where('category_level',1)
            ->where('is_male',$sex)
            ->where('is_show_main','1')
            ->orderBy('sort_num','asc')
            ->select([
                'category_id',
                'category_name_ru',
                'category_id',
            ])
            ->get();
        return HttpHelper::showSuccess($main_category);
    }

    public function categories($category_id)
    {
        $category = \App\Models\Category::where('category_level',2)
            ->where('main_category_id', $category_id)
            ->where('is_show','!=','0')
            ->orderBy('sort_num','asc')
            ->select([
                'category_id',
                'category_name_ru',
                'category_id',
            ])
            ->get();
        return HttpHelper::showSuccess($category);
    }

    public function subcategories($subcategory_id)
    {
        $category = \App\Models\Category::where('category_level',3)
            ->where('main_category_id', $subcategory_id)
            ->where('is_show','!=','0')
            ->orderBy('sort_num','asc')
            ->select([
                'category_id',
                'category_name_ru',
                'category_id',
            ])
            ->get();
        return HttpHelper::showSuccess($category);
    }



    public function filterBySCID()
    {
        $result = [
            //'min_price' => Category::find($subcategory_id)->products()->min('product_price'),
            //'max_price' => Category::find($subcategory_id)->products()->max('product_price'),
            'min_price' => 0,
            'max_price' => 5000,
            'sizes' => DB::table('size')->orderBy('unique_size','asc')->groupBy('unique_size')->select([
                'unique_size',
            ])->get(),
            'collections' => DB::table('item')
                ->join('feature','item.feature_id','=','feature.feature_id')
                ->where('feature.feature_id',9)
                ->get(['item_name_ru','item.item_id',]),
            'brands' => DB::table('brand')
                ->whereNull('deleted_at')
                ->orderBy('brand_name')
                ->get(['brand_name','brand_id']),
        ];
        return HttpHelper::showSuccess($result);
    }

    public function productByID(Request $request, $id)
    {
        Auth::loginUsingId($request->safeUser->user_id);
        $product = Product::where('product_id', $id)->select([
            'product_id',
            'product_name_ru',
            'product_price',
            'product_desc_ru',
            'brand_id',
            'discount_id',
            'product_url',
            'view_count',
            'product_code',
        ])->first();
        $product->view_count++;
        $product->save();
        $images =  Image::where('product_id',$product->product_id)
            ->where('is_main','0')
            ->orderBy('image_id', 'desc')
            ->select(['image_url'])
            ->get();

        $product_size = DB::table('product_size')->where('product_id',$product->product_id)->pluck('unique_size');
        $size = DB::table('size')->whereIn('unique_size',$product_size)->get(['size_name_id']);
        $size_type = DB::table('size_type')
            ->leftJoin('size_name_type','size_type.id','=','size_name_type.type_size_id')
            ->where('size_name_type.name_size_id',1)
            ->get(['size_type.*','size_name_type.*']);
        $type = count($size_type);
        $chunks = array_chunk($size,$type);
        for($i=0;$i<count($chunks); $i++) {
            for ($c = 0; $c < count($chunks[$i]); $c++) {
                $resultsize[$i][] = $chunks[$i][$c]->size_name_id;
            }
        }

        $product_feature = ProductFeature::leftJoin('feature','feature.feature_id','=','product_feature.feature_id')
            ->where('product_id',$product->product_id)
            ->where('feature.feature_id','>',0)
            ->select('feature.*','product_feature.*')
            ->orderBy('feature.sort_num','asc')
            ->get();

        foreach($product_feature as $val)
        {
            if($val->kind_id == 3)
            {
                $resultFeatures[] = [
                    'key' => $val->feature_name_ru,
                    'value' => $val->product_feature_desc,
                ];
            } elseif($val->kind_id == 2 || $val->kind_id == 1)
            {
                $product_item = \App\Models\ProductFeature::leftJoin('item','item.item_id','=','product_feature.item_id')
                    ->where('product_feature.product_id', $product->product_id)
                    ->where('product_feature.item_id','>',0)
                    ->where('item.feature_id',$val->feature_id)
                    ->select('item.*')
                    ->get();
                foreach($product_item as $key => $item_val)
                {
                    $items[] = $item_val->item_name_ru;
                }

                $resultFeatures[] = [
                    'key' => $val->feature_name_ru,
                    'value' => implode(', ', $items),
                ];
            }

        }

        $comment = Comment::orderBy('comment_id','desc')
            ->where('product_id','=',$product->product_id)
            ->where('is_show',1)
            ->select('comment.*',
                DB::raw('DATE_FORMAT(comment.created_at,"%d.%m.%Y") as date'))
            ->take(2)
            ->get();

        $recomends = ProductRecommended::getProductRecommended($product->product_id);

        $result = [
            'product' => $product,
            'images' => $images,
            'category' => $product->categories()->first()->category_name_ru,
            'sizes' => $resultsize,
            'features' => $resultFeatures,
            'reviews' => $comment,
            'recomends' => $recomends,
        ];
        return HttpHelper::showSuccess($result);
    }

    public function reviews(Request $request, $id)
    {
        $comments = Comment::orderBy('comment_id','desc')
            ->where('product_id','=',$id)
            ->where('is_show',1)
            ->select('comment.*',
                DB::raw('DATE_FORMAT(comment.created_at,"%d.%m.%Y") as date'))
            ->get();
        return HttpHelper::showSuccess($comments);
    }

    public function favorite(Request $request, $id)
    {
        $check = ProductFavorite::where('user_id', $request->safeUser->user_id)->where('product_id', $id)->count();
        if ($check)
        {
            ProductFavorite::where('user_id', $request->safeUser->user_id)->where('product_id', $id)->first()->delete();
            return HttpHelper::showSuccess([
                'action' => 'delete',
                'text' => 'Удалено',
            ]);
        }


        ProductFavorite::create([
            'user_id' => $request->safeUser->user_id,
            'product_id' => $id,
        ]);

        return HttpHelper::showSuccess([
            'action' => 'add',
            'text' => 'Добавлено',
        ]);
    }

    public function addReview(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'message' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return HttpHelper::showError($error[0]);
        }

        $comment = new Comment();
        $comment->message = $request->message;
        $comment->user_id = $request->safeUser->user_id;
        $comment->user_name = $request->safeUser->name.' '.$request->safeUser->surname;
        $comment->email = $request->safeUser->email;
        $comment->product_id = $id;
        $comment->save();

        return HttpHelper::showSuccess('Спасибо за Ваш отзыв!');
    }
}