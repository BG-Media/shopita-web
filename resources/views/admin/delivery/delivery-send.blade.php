@extends('layout.admin-layout')

@section('content')

        <section class="content-header">
            <h1>
              {{ $title }}
            </h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                    @if (isset($error))
                        <div class="alert alert-danger">
                           {{ $error }}
                        </div>
                    @endif

                    <div id="error_text" class="alert alert-danger" style="display: none"></div>

                        <form action="/admin/delivery-send" method="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Заголовок рассылки</label>
                                    <input value="{{ $row->title }}" type="text" class="form-control" name="title" placeholder="Введите">
                                </div>
                                <div class="form-group">
                                  <label>Текст рассылки</label>
                                  <textarea id="delivery_text" name="delivery_text" class="form-control">{!! $row->delivery_text !!}</textarea>
                                </div>
                                <div class="form-group">
                                  <label>Получатели писем</label>
                                  <div>
                                    <div class="clear-float"></div>
                                    <div class="clear-float"></div>
                                      <?foreach($delivery_kind as $value){?>

                                          <div class="left-float delivery-checkbox">
                                              <input class="check-box-classs" name="delivery_kind[]" type="checkbox" value="<?=$value->delivery_kind_id?>"/>
                                          </div>
                                          <div class="left-float">
                                              <span><?=$value->delivery_kind_name?></span>
                                          </div>
                                          <div class="clear-float"></div>
                                      <?}?>
                                  </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Отправить рассылку</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </section>

            <link href="/wysiwyg/default.css" rel="stylesheet"/>
            <script type="text/javascript" src="/wysiwyg/kindeditor.js"></script>
            <script type="text/javascript" src="/wysiwyg/ru_Ru.js"></script>
        <script src="/admin/plugins/jQuery/jQuery-2.1.4.min.js"></script>

            <script type="text/javascript">

                KindEditor.ready(function(K) {
                    K.create('#delivery_text', {

                        cssPath : [''],
                        autoHeightMode : true, // это автоматическая высота блока
                        afterCreate : function() {
                            this.loadPlugin('autoheight');
                        },
                        allowFileManager : true,
                        items : [// Вот здесь задаем те кнопки которые хотим видеть
                            'source', '|', 'undo', 'redo', '|', 'preview', 'print', 'template', 'code', 'cut', 'copy', 'paste',
                            'plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 'justifyright',
                            'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript',
                            'superscript', 'clearhtml', 'quickformat', 'selectall', '|', 'fullscreen', '/',
                            'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold',
                            'italic', 'underline', 'strikethrough', 'lineheight', 'removeformat', '|', 'image', 'multiimage',
                            'flash', 'media', 'insertfile', 'table', 'hr', 'emoticons','deliverybreak',
                            'anchor', 'link',  'unlink','map', '|', 'about'
                        ]
                    });
                    //Ниже инициализируем доп. например выбор цвета или загрузка файла
                    var colorpicker;
                    K('#colorpicker').bind('click', function(e) {
                        e.stopPropagation();
                        if (colorpicker) {
                            colorpicker.remove();
                            colorpicker = null;
                            return;
                        }
                        var colorpickerPos = K('#colorpicker').pos();
                        colorpicker = K.colorpicker({
                            x : colorpickerPos.x,
                            y : colorpickerPos.y + K('#colorpicker').height(),
                            z : 19811214,
                            selectedColor : 'default',
                            noColor : 'Очистить',
                            click : function(color) {
                                K('#color').val(color);
                                colorpicker.remove();
                                colorpicker = null;
                            }
                        });
                    });
                    K(document).click(function() {
                        if (colorpicker) {
                            colorpicker.remove();
                            colorpicker = null;
                        }
                    });

                    var editor = K.editor({
                        allowFileManager : true
                    });
                });

                $('.check-box-class').click(function () {
                    console.log('ok');

                    if ($(this).is(':checked')) {
                        $(this).val(1);
                    }
                    else {
                        $(this).val(0);
                    }
                });

                function showNews(){
                    $('#news_deliver').toggle();
                }
            </script>

@endsection

