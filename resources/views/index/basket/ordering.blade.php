<div class="row" id="ordering">
    <h1>Оформление заказа</h1>
    <div class="col-md-9">
        <div class="row1">
            <div class="row">
                <div class="col-md-3 col-sm-3">
                    <p>Имя</p>
                </div>
                <div class="col-md-9 col-sm-9">
                    <input value="@if(Auth::check()){{Auth::user()->name}}@endif" type="text" class="inputs_busket" id="order_user_name">
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-3">
                    <p>Телефон</p>
                </div>
                <div class="col-md-9 col-sm-9">
                    <input value="@if(Auth::check()){{substr(Auth::user()->mobile_phone, 2)}}@endif" id="order_phone" type="text" class="inputs_busket">
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-3">
                    <p>Электронная почта</p>
                </div>
                <div class="col-md-9 col-sm-9">
                    <input value="@if(Auth::check()){{Auth::user()->email}}@endif" id="order_email" type="email" class="inputs_busket">
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-3">
                    <p>Город</p>
                </div>
                <div class="col-md-9 col-sm-9">
                    <input value="@if(Auth::check()){{Auth::user()->city}}@endif" id="order_city" type="text" class="inputs_busket">
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-3">
                    <p>Адрес</p>
                </div>
                <div class="col-md-9 col-sm-9">
                    <input value="@if(Auth::check()){{Auth::user()->address}}@endif" id="order_address" type="text" class="inputs_busket">
                    <input value="0" type="hidden" id="order_is_pickup">
                    <a href="javascript:void(0)" onclick="changePickup(this, 0)">
                        <button class="submits2 pickup active">Курьерская доставка</button>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-9 green_block">
        <h3><span class="basket_count"><?=$basket->count();?></span> товара на сумму <span id="order_sum">0</span> тг.</h3>
        <div class="row">
            <div class="col-md-7 col-sm-7 top_green_block">
                <form action="javascript:void(0);" id="code">
                    <input type="text" placeholder="Введите промокод" name="code" class="input_green_block1">
                    <input type="submit" value="применить" class="input_green_block2">
                </form>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-5">
                        <p>Варианты оплаты</p>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-7">
                        <input name="is_pickup" id="pay_kind" type="checkbox" value="0">&nbsp; &nbsp; &nbsp; При получении
                        <br>
                        <input name="is_pay" id="is_pay" type="checkbox" value="0">&nbsp; &nbsp; &nbsp; Онлайн оплата
                    </div>
                </div>
            </div>
            <div class="col-md-5 col-sm-5">
                <h4 class="total">Итого: <span id="order_sum_all">0</span> тг.</h4>
            </div>
        </div>

        <div class="row bottom_green_block">
            <div class="col-md-6 col-sm-6">
                <p>Нажимая на кнопку "Отправить заказ", вы принимаете условия <a href="http://shopita.kz/page/publichnaya-oferta-u8">Публичной оферты</a></p>
            </div>
            <div class="col-md-6 col-sm-6 sent_order">
                <a href="#" onclick="createOrder()">
                    <button type="button">Отправить заказ</button>
                </a>
            </div>
        </div>
    </div>

</div>