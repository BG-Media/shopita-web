<?php

namespace App\Models;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\SoftDeletes;


class Users extends Model implements  AuthenticatableContract
{
    use Authenticatable;

    protected $table = 'users';
    protected $primaryKey = 'user_id';

    protected $fillable = ['name', 'email', 'surname', 'password', 'api_token', 'sex', 'avatar', 'mobile_phone', ];

    protected $hidden = ['password', 'updated_at', 'deleted_at', 'created_at'];

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function favorites() {
        return $this->belongsToMany('App\Models\Product', 'product_favorite', 'user_id', 'product_id');
    }
}
