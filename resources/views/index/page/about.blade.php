@extends('layout.layout')

@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="container">
                    <div class="row content_contacts">
                        <div class="tabs-left tab_1">
                            <div class="col-md-3 col-sm-3 col-12">
                                <ul class="nav nav-tabs">
                                    <li @if(!isset($request->page) || $request->page == 6) class="active" @endif>
                                        <a href="#a" data-toggle="tab">
                                            <span>Контакты</span>
                                        </a>
                                    </li>
                                    <li @if(isset($request->page) && $request->page == 7) class="active" @endif>
                                        <a href="#b" data-toggle="tab">
                                            <span>О нас</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                            <div class="col-md-9 col-sm-9 col-12 tab-content">

                                <div class="tab-pane @if(!isset($request->page) || $request->page == 6) active @endif" id="a">
                                    <ol class="breadcrumb">
                                        <li><a href="/">главная</a></li>
                                        <li class="active">контакты</li>
                                    </ol>
                                    <h2>Контакты</h2>
                                    <div>
                                        <? $page = \App\Models\Page::where('page_id',6)->first();?>
                                        <?=$page->page_text_ru?>
                                    </div>
                                </div>

                                <div class="tab-pane @if(isset($request->page) && $request->page == 7) active @endif" id="b">
                                    <ol class="breadcrumb">
                                        <li><a href="/">главная</a></li>
                                        <li class="active">о нас</li>
                                    </ol>
                                    <h2>О нас</h2>
                                    <div>
                                        <? $page = \App\Models\Page::where('page_id',7)->first();?>
                                        <?=$page->page_text_ru?>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    @include('layout.footer')

                </div>
            </div>
        </div>
    </div>

@endsection

