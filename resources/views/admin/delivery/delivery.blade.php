@extends('layout.admin-layout')

@section('content')

<div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title" style="margin-top: 5px">Рассылка</h3>
            <a href="/admin/delivery-send/0" style="float: right; margin-left: 30px">
                <button class="btn btn-primary" style="margin-bottom: 10px; ">Отправить рассылку</button>
            </a>
{{--          <a href="/admin/delivery/create" style="float: right">
               <button class="btn btn-primary" style="margin-bottom: 10px; ">Создать шаблон</button>
           </a>--}}
        </div>

        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr style="border: 1px">
                <th style="width: 30px">№</th>
                <th>Заголовок</th>
                <th>Дата отправки</th>
{{--                <th style="width: 30px">Отправить</th>--}}
                <th style="width: 30px"></th>
                <th style="width: 30px"></th>
              </tr>
            </thead>

            <tbody>

                  @foreach($row as $key => $val)

                     <tr>
                        <td> {{ $key + 1 }}</td>
                        <td>
                             {{ $val->title }}
                        </td>
                        <td>
                             {{ $val->date }}
                        </td>
{{--                        <td style="text-align: center">
                            <a href="/admin/delivery-send/{{ $val->delivery_id }}" target="_blank">
                                <li class="fa fa-envelope" style="font-size: 20px; color: #367FA9;"></li>
                            </a>
                        </td>--}}
                        <td style="text-align: center">
                            <a href="javascript:void(0)" onclick="delItem(this,{{ $val->delivery_id }})">
                                <li class="fa fa-trash-o" style="font-size: 20px; color: red;"></li>
                            </a>
                        </td>
                        <td style="text-align: center">
                            <a href="/admin/delivery/{{ $val->delivery_id }}/edit">
                                <li class="fa fa-eye" style="font-size: 20px;"></li>
                            </a>
                        </td>
                     </tr>

                  @endforeach

            </tbody>

          </table>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
    </div><!-- /.row -->

    <meta name="csrf-token" content="{{ csrf_token() }}">

       <script>
           function delItem(ob,id){
               if(confirm('Действительно хотите удалить?')){
                   $(ob).closest('tr').remove();
                   $.ajax({
                       type: 'DELETE',
                       headers: {
                           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                       },
                       url: "/admin/delivery/" + id,
                       success: function(data){

                       }
                   });
               }
           }
       </script>

@endsection