@extends('layout.layout')

@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="container">
                    <div class="row">

                        <div class="single-item">

                            <?
                                $banner = \App\Models\Page::where('page_position','1')
                                        ->where([
                                            ['page_kind', 3],
                                            ['is_show', '!=', 0]
                                        ])
                                        ->get();
                            ?>
                            @foreach($banner as $value)
                                <a href="/sale/{{$value->page_url}}-u{{$value->page_id}}">
                                    <img src="/image/banner/{{$value->page_image}}" alt="">
                                </a>
                            @endforeach

                        </div>

                        <div class="images">
                            <div class="col-md-4 block-height col-sm-6 leftImg">
                                <? $banner = \App\Models\Page::where('page_position','2')
                                        ->where([
                                            ['page_kind', 3],
                                            ['is_show', '!=', 0]
                                        ])
                                        ->first();
                                ?>
                                @if($banner != null)
                                    <a href="/sale/{{$banner->page_url}}-u{{$banner->page_id}}">
                                        <img src="/image/banner/{{$banner->page_image}}" class="img-responsive center-block" alt="">
                                    </a>
                                @endif
                            </div>
                            <div class="col-md-4 block-height col-sm-6">
                                <? $banner = \App\Models\Page::where('page_position','4')
                                        ->where([
                                            ['page_kind', 3],
                                            ['is_show', '!=', 0]
                                        ])
                                        ->first();
                                ?>
                                @if($banner != null)
                                    <a href="/sale/{{$banner->page_url}}-u{{$banner->page_id}}">
                                        <img src="/image/banner/{{$banner->page_image}}" class="img-responsive center-block" alt="">
                                    </a>
                                @endif
                            </div>
                            <div class="col-md-4 block-height col-sm-6 rightImg">
                                <? $banner = \App\Models\Page::where('page_position','3')
                                        ->where([
                                            ['page_kind', 3],
                                            ['is_show', '!=', 0]
                                        ])
                                        ->first();
                                ?>
                                @if($banner != null)
                                    <a href="/sale/{{$banner->page_url}}-u{{$banner->page_id}}">
                                        <img src="/image/banner/{{$banner->page_image}}" class="img-responsive center-block" alt="">
                                    </a>
                                @endif
                            </div>
                            <div class="col-md-4 block-height col-sm-6 leftImg">
                                <? $banner = \App\Models\Page::where('page_position','5')
                                        ->where([
                                            ['page_kind', 3],
                                            ['is_show', '!=', 0]
                                        ])
                                        ->first();
                                ?>
                                @if($banner != null)
                                    <a href="/sale/{{$banner->page_url}}-u{{$banner->page_id}}">
                                        <img src="/image/banner/{{$banner->page_image}}" class="img-responsive center-block" alt="">
                                    </a>
                                @endif
                            </div>
                            <div class="col-md-8 block-height rightImg">
                                <? $banner = \App\Models\Page::where('page_position','6')
                                        ->where([
                                            ['page_kind', 3],
                                            ['is_show', '!=', 0]
                                        ])
                                        ->first();
                                ?>
                                @if($banner != null)
                                    <a href="/sale/{{$banner->page_url}}-u{{$banner->page_id}}">
                                        <img src="/image/banner/{{$banner->page_image}}" class="img-responsive center-block" alt="">
                                    </a>
                                @endif
                            </div>

                        </div>

                        <div class="col-md-12 content_text">
                            {!! $pageText->page_text  !!}
                        </div>

                        {{--<div class="col-md-12 hidden-xs brend_block">
                            <h4 class="title_brend"><span>Бренды</span></h4>
                            <div class="brends">

                                @foreach($brand as $value)
                                    <a target="_blank" href="{{$value->page_url}}">
                                        <img src="/image/brand/{{$value->page_image}}" alt="" class="img-responsive">
                                    </a>
                                @endforeach

                            </div>
                        </div>--}}

                    </div>

                    @include('layout.footer')

                </div>
            </div>
        </div>
    </div>

@endsection

