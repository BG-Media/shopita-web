<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\PromoCode;

use Illuminate\Support\Facades\Validator;
class PromoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = '';
        if(isset($request->search)){
            $search = $request->search;
        }

        $row = PromoCode::orderBy('id','desc')
            ->where(function($query) use ($search){
                $query->where('code_name','like','%' .$search .'%');
            })
            ->paginate(10);

        return  view('admin.promo.index',[
            'row' => $row,
            'request' => $request
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $row = new PromoCode();

        return  view('admin.promo.create_edit', [
            'title' => 'Добавить промокод',
            'row' => $row
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'code_name' => 'required',
            'promo_code' => 'required',
            'code'=>'required',
            'code_type'=>'required'
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return  view('admin.promo.create_edit', [
                'title' => 'Добавить промокод',
                'row' => (object) $request->all(),
                'error' => $error[0]
            ]);
        }

        $promo = new PromoCode();
        $promo->code_name = $request->code_name;
        $promo->promo_code = $request->promo_code;
        $promo->code = $request->code;
        $promo->code_type = $request->code_type;
        $promo->save();
        return redirect('/admin/promo-code');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $row = PromoCode::find($id);
        return  view('admin.promo.create_edit', [
            'title' => 'Изменить промокод',
            'row' => $row
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'code_name' => 'required',
            'promo_code' => 'required',
            'code'=>'required',
            'code_type'=>'required'
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return  view('admin.promo.create_edit', [
                'title' => 'Изменить промокод',
                'row' => (object) $request->all(),
                'error' => $error[0]
            ]);
        }

        $promo = PromoCode::find($id);
        $promo->code_name = $request->code_name;
        $promo->promo_code = $request->promo_code;
        $promo->code = $request->code;
        $promo->code_type = $request->code_type;
        $promo->save();
        return redirect('/admin/promo-code');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $page = PromoCode::find($id);
        $page->delete();
    }
}
