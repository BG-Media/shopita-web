@extends('layout.admin-layout')

@section('content')

        <section class="content-header">
            <h1>
              {{ $title }}
            </h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                   <div class="col-md-8" style="padding-left: 0px">
                       <div class="box box-primary">
                           @if (isset($error))
                               <div class="alert alert-danger">
                                  {{ $error }}
                               </div>
                           @endif
                               @if($row->user_id > 0)
                                   <form action="/admin/user/{{$row->user_id}}" method="POST">
                                   <input type="hidden" name="_method" value="PUT">
                               @else
                                   <form action="/admin/user" method="POST">
                               @endif

                                   <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                   <input id="user_id" type="hidden" name="user_id" value="{{ $row->user_id }}">
                                   <input type="hidden" id="avatar" name="avatar" value="{{ $row->avatar }}"/>

                                   <div class="box-body">
                                       <div class="form-group">
                                           <label>ФИО</label>
                                           <input value="{{ $row->name }}" type="text" class="form-control" name="name" placeholder="Введите">
                                       </div>
                                       <div class="form-group">
                                          <label>Email</label>
                                          <input value="{{ $row->email }}" type="text" class="form-control" name="email" placeholder="Введите">
                                       </div>
                                       <div class="form-group">
                                           <label>Выберите роль</label>
                                           <select class="form-control" name="role_id">

                                               <?foreach($role as $value){
                                               $selected = '';
                                               if($value['role_id'] == $row->role_id){
                                                   $selected=' selected ';
                                               }?>
                                               <option value="<?=$value['role_id']?>" <?=$selected?>><?=$value['role_name']?></option>
                                               <?}?>

                                           </select>
                                       </div>
                                       <div class="form-group">
                                           <label>Выберите пол</label>
                                           <select class="form-control" name="sex">
                                               <option value="0" @if($row->sex == '0') selected @endif>Муж</option>
                                               <option value="1" @if($row->sex == '1') selected @endif>Жен</option>
                                           </select>
                                       </div>
                                       <div class="form-group">
                                         <label>Выберите город</label>
                                         <select class="form-control" name="city_id">

                                             <?foreach($city as $value){
                                                 $selected = '';
                                                 if($value['city_id'] == $row->city_id){
                                                     $selected=' selected ';
                                                 }?>
                                                 <option value="<?=$value['city_id']?>" <?=$selected?>><?=$value['city_name_ru']?></option>
                                             <?}?>

                                         </select>
                                      </div>
                                      <div class="form-group">
                                        <label>Адрес</label>
                                        <input value="{{ $row->address }}" type="text" class="form-control" name="address" placeholder="Введите">
                                      </div>
                                      <div class="form-group">
                                           <label>Номер телефона</label>
                                           <input value="{{ $row->mobile_phone }}" type="text" class="form-control" id="phone" name="mobile_phone" placeholder="Введите">
                                       </div>

                                    </div>

                                   <div class="box-footer">
                                       <button type="submit" class="btn btn-primary">Сохранить</button>
                                   </div>
                               </form>
                           </div>
                   </div>
                   <div class="col-md-4">
                       <div class="box box-primary" style="padding: 30px; text-align: center">
                           <div style="border-radius: 50%; padding: 20px; border: 1px solid #c2e2f0">
                               @if(strpos($row->avatar, 'http') !== false)
                                   <img id="avatar_img" src="{{ $row->avatar }}" style="width: 100%; border-radius: 50%;"/>
                               @else
                                   <img id="avatar_img" src="/image/avatar/{{ $row->avatar }}" style="width: 100%; border-radius: 50%;"/>
                               @endif


                           </div>
                           <div style="background-color: #c2e2f0;height: 40px;margin: 0 auto;width: 2px;"></div>
                           <form id="avatar_form" enctype="multipart/form-data" method="post" class="avatar-form">
                               <i class="fa fa-plus"></i>
                               <input id="avatar-file" type="file" onchange="uploadAvatar()" name="image"/>
                           </form>
                       </div>
                   </div>
                </div>
            </div>
        </section>

         <meta name="csrf-token" content="{{ csrf_token() }}">

        <script src="/javascript/jquery-2.1.1.min.js"></script>
        <script src="/javascript/jquery.maskedinput.js"></script>
        <script src="/javascript/uploadfile.js"></script>

        <script>
            $("#phone").mask("+7(999)999-99-99");

            var cw = $('#avatar_img').width();
            $('#avatar_img').css('height',cw);
        </script>

        <script>
            function uploadAvatar(){
                $("#avatar_form").submit();
            }
        </script>
@endsection

