<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Image extends Model
{
    protected $table = 'image';
    protected $primaryKey = 'image_id';
    public $appends = ['full_path'];

    public function getFullPathAttribute() {
        return 'http://shopita.kz/image/product/'.$this->image_url;
    }

    public function checkExistProductImage($product_id){
        $product_image = Image::where('product_id','=',$product_id)
            ->count();
        return $product_image;
    }

    public function checkExistProductImageByToken($image_token){
        $product_image = Image::where('image_token','=',$image_token)
            ->count();
        return $product_image;
    }
}
