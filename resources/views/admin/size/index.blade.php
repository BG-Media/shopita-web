@extends('layout.admin-layout')
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title" style="margin-top: 5px">Блок размеров</h3>
                    <br>
                    <br>
                    <a href="/admin/size/type/create" style="float: left">
                        <button class="btn btn-primary" style="margin-bottom: 10px; ">Добавить название размеров</button>
                    </a>
                    <a href="/admin/size/create" style="float: right">
                        <button class="btn btn-primary" style="margin-bottom: 10px; ">Добавить размер</button>
                    </a>
                </div>
                <div style="padding-top: 10px; border-top: 1px solid rgb(127, 127, 127);"></div>
            </div>
            <div class="box">
                <div class="panel panel-default">
                    <div class="panel-heading"></div>
                    <ul class="list-group">
                        @for($i=0;$i<count($name);$i++)
                            <li class="list-group-item"><a href="/admin/size/{{$name[$i]->id}}">{{$name[$i]->title}}</a>
                            {{--<div style="float: right;">--}}
                                {{--<form action="size/type/delete/{{$name[$i]->id}}" method="POST">--}}
                                    {{--<button type="submit" class="btn btn-danger">Удалить</button>--}}
                                {{--</form>--}}
                            {{--</div>--}}
                            <div style="float: right;">
                                <a href="/admin/size/type/edit/{{$name[$i]->id}}" class="btn btn-info">Изменит</a>
                            </div>
                            </li>
                        @endfor
                    </ul>
                </div>
                </div>
        </div>
    </div>
@endsection