
@extends('layout.layout')

@section('content')
    <input type="hidden" value="{{$row->product_id}}" id="product_id"/>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="container">
                    <div class="row">
                        <div class="clearfix">
                            <ol class="breadcrumb top_breadcrumb">
                                <li><a href="/">Главная</a></li>
                                @if(isset($row->main_category_name_ru))
                                    <li>
                                        <a href="/catalog?main={{$row->main_category_url}}-u{{$row->main_category_id}}">{{$row->main_category_name_ru}}</a>
                                    </li>
                                @endif

                                @if(isset($row->category_name_ru))
                                    <li>
                                        <a href="/catalog?category={{$row->category_url}}-u{{$row->category_id}}">{{$row->category_name_ru}}</a>
                                    </li>
                                @endif

                                @if(isset($row->subcategory_name_ru))
                                    <li>
                                        <a href="/catalog?subcategory={{$row->subcategory_url}}-u{{$row->subcategory_id}}">{{$row->subcategory_name_ru}}</a>
                                    </li>
                                @endif

                                <li>
                                    {{$row->product_name_ru}}
                                </li>

                            </ol>
                        </div>
                        <div class="clearfix products_see">
                            <div class="col-md-6 col-sm-6 col-md-offset-0 col-sm-offset-0 col-xs-offset-2 col-xs-8">
                                <div class="row clearfix">

                                    <div class="zoom-wrapper hidden-sm hidden-xs">
                                        <div class="zoom-left">

                                            <div class="col-md-3 col-sm-3" id="gallery_01" style=" float:left; ">

                                                @foreach($product_image as $val)

                                                    <a href="#" class="elevatezoom-gallery active" data-update="" data-image="/image/product/{{$val->image_url}}" data-zoom-image="/image/product/{{$val->image_url}}">
                                                        <img src="/image/product/{{$val->image_url}}" width="110" />
                                                    </a>

                                                @endforeach


                                            </div>

                                            <div class="col-md-8 col-sm-8">
                                                <img style="border:1px solid #e8e8e6;/*padding:5px;*/max-height: 476px;" class="img-responsive" id="zoom_03" src="/image/product/@if(isset($product_image[0])){{$product_image[0]->image_url}}@endif" data-zoom-image="/image/product/@if(isset($product_image[0])){{$product_image[0]->image_url}}@endif" />
                                            </div>

                                        </div>
                                    </div>

                                    <div class="mob_gallery hidden-lg hidden-md">

                                        @foreach($product_image as $val)

                                             <img src="/image/product/{{$val->image_url}}" class="img-responsive" alt="" />

                                        @endforeach

                                    </div>

                                </div>
                            </div>

                            <div class="col-md-6 col-sm-6">
                                <h1><b>{{$row->product_name_ru}}</b></h1>
                                <p>{{$row->brand_name}}</p>
                                <p>{{$row->subcategory_name_ru}}</p>
                                <h1><b>{{$row->product_price}} тг</b></h1>
                                <div class="size_product">
                                    <div class="input-group-btn select" id="select1">
                                        <button type="button" class="btn btn-default dropdown-toggle toggle_class" data-toggle="dropdown" aria-expanded="false"><span class="selected">Выберите размер</span> <span class="caret"></span></button>
                                        <ul class="dropdown-menu option option_menu"  role="menu">
                                            <span class="flex_disabled">
                                                @if(count($size_type))
                                                    @foreach($size_type as $item)
                                                        <li class="disabled"><a href="#">{{$item->type}}</a></li>
                                                    @endforeach
                                                @endif
                                            </span>
                                            <?php
                                                $size = DB::table('size')->whereIn('unique_size',$product_size)->get();
                                                $type = count($size_type);
                                                $chunks = array_chunk($size,$type);
                                            ?>
                                            @for($i=0;$i<count($chunks); $i++)
                                                <li data-default="{{$chunks[$i][0]->size_name_id}}" class="select_option">
                                                    <a href="#">
                                                        @for($c=0;$c<count($chunks[$i]);$c++)
                                                            <p class="option_span" >{{$chunks[$i][$c]->size_name_id}}</p>
                                                        @endfor
                                                    </a>
                                                </li>
                                            @endfor
                                        </ul>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <p>Количество:</p>
                                    <div class="col-md-4 col-sm-5 col-xs-6">
                                        <div class="row">
                                            <div class="btn-group" role="group" aria-label="...">
                                                <button onclick="minuscount()" type="button" class="btn btn-default "><img src="/image/1.png" alt=""></button>
                                                <button disabled="" type="button" class="btn btn-default " id="productcount">1</button>
                                                <button onclick="pluscount()" type="button" class="btn btn-default "><img src="/image/2.png" alt=""></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-7 col-sm-7 col-xs-6">
                                        <a href="javascript:void(0)" onclick="addToBasket()">
                                            <button type="button" class="add_bucket">Добавить в корзину</button>
                                        </a>

                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="clearfix">
                            <div class="col-md-12">
                                <!-- Nav tabs -->
                                <div class="row card">
                                    <ul class="nav nav-tabs horizontal-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Характеристики товара</a></li>
                                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">О товаре</a></li>
                                        <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Отзывы</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="home">

                                            @include('index.product.product-feature-loop')

                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="profile">
                                            <p><?=$row->product_desc_ru?></p>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="messages">

                                            <div id="comment_list">
                                                @include('index.product.comment-loop')
                                            </div>


                                            <div class="clearfix feedback">
                                                <a href="#openModal_main">
                                                    <button type="button" class="give_feedback">Оставить отзыв</button>
                                                </a>
                                                <div id="openModal_main" class="modalDialog">
                                                    <div class="modal_otzyvy">
                                                        <a href="#close" title="Закрыть" class="close">&times;</a>
                                                        <h2>Оставить отзыв</h2>

                                                        <label for="">Имя</label>
                                                        <br>
                                                        <input value="@if(Auth::check()){{Auth::user()->name}}@endif" id="comment_user_name" type="text" class="inputs">
                                                        <br>
                                                        <label for="">Почта</label>
                                                        <br>
                                                        <input value="@if(Auth::check()){{Auth::user()->email}}@endif" id="comment_email" type="text" class="inputs">
                                                        <br>
                                                        <br>
                                                        <label for="">Ваш текст </label>
                                                        <br>
                                                        <textarea name="" id="comment_message" rows="4"></textarea>
                                                        <br>
                                                        <br>
                                                        <div style="text-align:center;">
                                                            <a href="javascript:void(0)" onclick="writeComment()">
                                                                <input type="button" value="Отправить" class="submits">
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="row reclams">
                        <h4>Наши рекомендации</h4>

                        <div class="sliderReclam">
                            @include('index.product.product-recommended-loop')
                        </div>
                    </div>

                    @include('layout.footer')

                </div>
            </div>
        </div>
    </div>

    <script src="/js/jquery_1.11.0.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/jquery.elevatezoom.js"></script>
    <script src="/Slick/slick.js"></script>
    <script>
        $('.ii-select__value').click(function(){
            var c = $(this).attr('select_click');
            if(c == 'true'){
                //$('.ii-select__value').append('<style>.ii-select__value:after{transform:rotate(90deg) !important;}</style>');
                $('.ii-select__columns').css('display','block');
                $(this).attr('select_click','false');
            }else{
                //$('.ii-select__value').append('<style>.ii-select__value:after{transform:rotate(-90deg) !important;}</style>');
                $('.ii-select__columns').css('display','none');
                $(this).attr('select_click','true');
            }
        });
        $(document).mouseup(function (e){ // событие клика по веб-документу
            var div = $(".ii-select__columns"); // тут указываем ID элемента
            if (!div.is(e.target) // если клик был не по нашему блоку
                && div.has(e.target).length === 0) { // и не по его дочерним элементам
                div.hide(); // скрываем его
            }
        });
        $('.colum p:not(:first-child)').hover(function(){
                $(this).addClass('hovered_p');
            },
            function(){
                $(this).removeClass('hovered_p');
            });

        $('.table>tbody>tr>td').click(function(){
            var size = $(this).text();
            var size_name = $(this).attr('size_name');
            document.querySelector('.ii-select__value').innerHTML=size;
        });
    </script>
    <script>
        $("#zoom_03").elevateZoom({
            gallery: 'gallery_01',
            cursor: 'pointer',
            galleryActiveClass: 'active',
            imageCrossfade: true/*,
             loadingIcon: 'http://www.elevateweb.co.uk/spinner.gif'*/
        });
        $("#zoom_03").bind("click", function(e) {
            var ez = $('#zoom_03').data('elevateZoom');
            $.fancybox(ez.getGalleryList());
            return false;
        });
    </script>
    <script>
        function minuscount() {
            var count = -1;

            var c = parseInt(document.getElementById('productcount').innerHTML);
            if (c > 0)
                c = c + count;
            document.getElementById('productcount').innerHTML = c;
        }

        function pluscount() {
            var count = 1;
            var c = parseInt(document.getElementById('productcount').innerHTML);
            if(<? echo 'c<'.$row->product_count?>)
                c = c + count;
            document.getElementById('productcount').innerHTML = c;
        }
    </script>
    <script>
        $('.mob_gallery').slick({
            dots: true,
            arrows: false

        });
    </script>
    <script>
        $('.sliderReclam').slick({
            dots: true,
            arrows: false,
            autoplay: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                }
            }, {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }]
        });
    </script>
    <script>
        $(document).ready(function () {

            $('.onsize').on('click',function () {
                var click = $(this).attr('click');
                if(click == 'false'){
                    $( this ).addClass( "selected" );
                    $(this).attr('click','true');
                }
                else {
                    $( this ).removeClass( "selected" );
                    $(this).attr('click','false');
                }

            })
        });
    </script>
    <script>
        function writeComment() {
            $('.ajax-loader').css('display','block');

            $.ajax({
                url:'/comment',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    message: $('#comment_message').val(),
                    user_name: $('#comment_user_name').val(),
                    email: $('#comment_email').val(),
                    product_id: $('#product_id').val()
                },
                success: function (data) {
                    $('.ajax-loader').fadeOut(100);
                    if(data.success == 0){
                        showError(data.error);
                        return;
                    }
                    $.get('/comment/' +  $('#product_id').val() + '?product_id=' + $('#product_id').val(), function(response){
                        window.location.href='#close';
                        $('#comment_list').html(response);
                    });
                    showMessage(data.message);
                }
            });
        }


        function addToBasket() {
            var size = $('.selected').text().match(/^[-\+]?\d+/);
            if(size != null){
            $('.ajax-loader').css('display','block');
            $.ajax({
                url:'/basket',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    size: $('.selected').text().trim(),
                    unit: $('#productcount').html(),
                    product_id: $('#product_id').val(),
                },
                success: function (data) {
                    console.log(data);
                    $('.ajax-loader').fadeOut(100);
                    if(data.success == 0){
                        showError(data.error);
                        return;
                    }
                    showMessage(data.message);
                    getBasketCount();
                }
            });
            }else{
                showError('Выберите размер');
            }
        }


    </script>
    <script>
        $('.select_option').click(function () {
            var c= $(this).attr('data-default');
            $('.selected').html(c);
        });
    </script>
@endsection

