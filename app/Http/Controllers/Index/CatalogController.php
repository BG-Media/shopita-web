<?php

namespace App\Http\Controllers\Index;

use App\Models\Category;
use App\Models\Feature;
use App\Models\Page;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\Subscriber;
use App\Models\Users;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use App\Helpers;
use Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use View;
use DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Models\Item;


class CatalogController extends Controller
{
    public function __construct()
    {
        $sex = Helpers::getSessionSex();
        View::share('sex', $sex);
        
        View::share('url', '/profile');
    }

    public function index(Request $request)
    {
        $category_id = 0;
        $category_column = 'category';
        $category_cond = '=';
        $id = 0;
        $menu = '';
        $sale_cond = '!=';
        $sale_id = -1;
        $sale_title = '';

        if(isset($request->sale)){
            $category_cond = '!=';
            $menu = 'discount';
            $sale_cond = '=';
            $sale_id = 1;
            $request->page_title = 'Все результаты распродажи';
            $sale_title = 'распродажи ';
        }

        if(isset($request->subcategory)){
            $id = Helpers::getIdFromUrl($request->subcategory);
            $subcategory = Category::where('category_id',$id)->first();
            if($subcategory == null || (Helpers::getTranslatedSlugRu($subcategory['category_url']) .'-u'.$id) != $request->subcategory){
                return redirect('/');
            }
            $request->subcategory = $subcategory;
            $category = Category::where('category_id',$subcategory->main_category_id)->first();
            $request->category = $category;
            $main_category = Category::where('category_id',$category->main_category_id)->first();
            $request->main_category = $main_category;

            $category_column = 'subcategory';
            $category_cond = '=';
            $request->page_title = 'Результаты '.$sale_title.'по категории “' .$subcategory->category_name_ru .'”';

        }
        else if(isset($request->category)){
            $id = Helpers::getIdFromUrl($request->category);
            $category = Category::where('category_id',$id)->first();
            if($category == null || (Helpers::getTranslatedSlugRu($category['category_url']) .'-u'.$id) != $request->category){
                return redirect('/');
            }

            $request->category = $category;

            $main_category = Category::where('category_id',$category->main_category_id)->first();
            $request->main_category = $main_category;

            $category_column = 'category';
            $category_cond = '=';
            $request->page_title = 'Результаты '.$sale_title.'по категории “' .$category->category_name_ru .'”';

        }
        else if(isset($request->main)){
            $id = Helpers::getIdFromUrl($request->main);
            $category = Category::where('category_id',$id)->first();
            if($category == null || (Helpers::getTranslatedSlugRu($category['category_url']) .'-u'.$id) != $request->main) {
                return redirect('/');
            }
            $request->main_category = $category;

            $category_column = 'main_category';
            $category_cond = '=';
            $request->page_title = 'Результаты '.$sale_title.'по категории “' .$category->category_name_ru .'”';
        }
        else {
            $category_cond = '!=';
        }


        $search = '';
        $sort = 'product.sort_num';
        $sort_desc = 'desc';
        $sort_color = $request['sort_color'];
        $sort_brand = $request['brand'];
        $sort_sizes = $request['sort_sizes'];
        $sort_collection = $request['collection'];
        $price_1 = $request['price_1'];
        $price_2 = $request['price_2'];
        if(isset($request->sort)){
            $sort = 'product.' .$request->sort;
            $sort_desc = $request->sort_desc;
            if($request->sort == 'random'){
                $sort = DB::raw('RAND()');
                $sort_desc = '';
            }
        }

        $product = ProductCategory::leftJoin('category as subcategory','subcategory.category_id','=','product_category.category_id')
                        ->leftJoin('category','subcategory.main_category_id','=','category.category_id')
                        ->leftJoin('category as main_category','category.main_category_id','=','main_category.category_id')
                        ->leftJoin('product','product.product_id','=','product_category.product_id')
                        ->leftJoin('brand','brand.brand_id','=','product.brand_id')
                        ->leftJoin('product_feature','product.product_id','=','product_feature.product_id')
                        ->leftJoin('product_size','product.product_id','=','product_size.product_id')
            ->where('product.is_show','=','1')
            ->where('product.deleted_at','=',null)
            ->where('product.is_sale',$sale_cond,$sale_id)
            ->where($category_column .'.category_id',$category_cond,$id)
            ->groupBy('product_name_ru')
            ->select(['product.*'])
            ->orderBy($sort,$sort_desc);
        if(isset($sort_color)) {
            $sort_color = explode('-',$sort_color);
            $product->where(function ($q) use ($sort_color) {
                return $q->whereIn('product.color_id',$sort_color);
            });
        }
        if(isset($sort_brand)){
            $sort_brand = explode('-',$sort_brand);
            $product->where(function($q) use ($sort_brand){
                return $q->whereIn('product.brand_id',$sort_brand);
            });
        }

        if(isset($sort_collection)) {
            $sort_collection = explode('-',$sort_collection);
            $product->where(function($q) use ($sort_collection){
                return $q->whereIn('product_feature.item_id',$sort_collection);
            });
        }

        if(isset($price_2) && isset($price_1)) {
            $product->whereBetween('product_price',[$price_1,$price_2]);
        } else {
            if(isset($price_1)) {
                $product->where('product_price','>=',$price_1);
            }
            if(isset($price_2)) {
                $product->where('product_price','<=',$price_2);
            }
        }

        if(isset($sort_sizes)){
            $sort_sizes = explode('-',$sort_sizes);

            $product->where(function($q) use ($sort_sizes){
                return $q->whereIn('product_size.unique_size',$sort_sizes);
            });

        }


        $product = $product->paginate(12);

        $color = DB::table('color')->get();

        $brands = DB::table('brand')
            ->whereNull('deleted_at')
            ->orderBy('brand_name')
            ->get(['brand_name','brand_id']);

        $collections = DB::table('item')
            ->join('feature','item.feature_id','=','feature.feature_id')
            ->where('feature.feature_id',9)
            ->get(['item_name_ru','item.feature_id','item.item_id']);

        $sizes = DB::table('size')->orderBy('unique_size','asc')->groupBy('unique_size')->get();
        return  view('index.catalog.product-list',[
            'menu' => $menu,
            'request' => $request,
            'product' => $product,
            'color'=>$color,
            'brands'=>$brands,
            'collections'=>$collections,
            'sort_color' => $sort_color,
            'sort_brand' => $sort_brand,
            'sort_collection' => $sort_collection,
            'sort_sizes'=>$sort_sizes,
            'sizes' => $sizes,
        ]);
    }

    private  function getCatalogProduct(){}
}
