@extends('layout.layout')

@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="container">

                    <div class="row contentSale">
                        <ol class="breadcrumb">
                            <li><a href="/">Главная</a></li>
                            <li><a href="/blog">Стильный лук</a></li>
                            <li class="active"><?=$row['page_name_ru']?></li>
                        </ol>

                        <h1 class="titleSale"><?=$row['page_name_ru']?></h1>
                    </div>

                    <div class="row topRowInStyle">

                        <?=$row['page_text_ru']?>

                    </div>

                    @include('layout.footer')

                </div>
            </div>
        </div>
    </div>

@endsection

