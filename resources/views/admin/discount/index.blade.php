@extends('layout.admin-layout')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title" style="margin-top: 5px">Скидки</h3>
                    </br>
                    <a href="/admin/discount/create" style="float: right">
                        <button class="btn btn-primary" style="margin-bottom: 10px; ">Добавить скидку</button>
                    </a>
                    <div style="clear: both"></div>
                </div>
                <div style="clear: both"></div>
                <div class="box-body">
                    <table id="news_datatable" class="table table-bordered table-striped">
                        <thead>
                        <tr style="border: 1px">
                            <th style="width: 30px">№</th>
                            <th>Название</th>
                            <th>Статус</th>
                            <th style="width: 30px"></th>
                            <th style="width: 30px"></th>
                        </tr>
                        </thead>

                        <tbody>

                        @foreach($list as $key => $val)

                            <tr>
                                <td> {{ $key + 1 }}</td>
                                <td>
                                    <div class="user-avatar">


                                            <a href="{{ $val->image }}" class="fancybox">
                                                <img src="{{ $val->image }}" style="max-width: 300px"/>
                                            </a>

                                    </div>
                                    <div class="user-name">
                                        <div>
                                            {{ $val->name }}
                                        </div>
                                        <div style="font-style: italic">
                                            {{ $val->percent }}%
                                        </div>
                                    </div>
                                    <div class="clear-float"></div>
                                </td>
                                <td class="arial-font">
                                    <div>
                                        @if ($val->active) Активно @else Неактивно @endif
                                    </div>
                                </td>
                                <td style="text-align: center">
                                    <a href="javascript:void(0)" onclick="delItem(this,{{ $val->id }})">
                                        <li class="fa fa-trash-o" style="font-size: 20px; color: red;"></li>
                                    </a>
                                </td>
                                <td style="text-align: center">
                                    <a href="/admin/discount/{{ $val->id }}/edit">
                                        <li class="fa fa-pencil" style="font-size: 20px;"></li>
                                    </a>
                                </td>
                            </tr>

                        @endforeach

                        </tbody>

                    </table>

                    @if($list->lastPage() > 1)

                        <div class="page_number" style="text-align: center">
                            <ul class="pagination">
                                <li>
                                    <a @if($row->currentPage() > 1) href="?{{preg_replace('~(\?|&)page=[^&]*~','',http_build_query($_GET))}}&page={{ $row->currentPage() - 1 }}" @endif >&laquo;</a>
                                </li>
                                <?
                                $start = 1;
                                if($row->currentPage() >= 10){
                                    $start = $row->currentPage();
                                }
                                $finish = $start + 9;
                                if($finish > $row->lastPage()){
                                    $finish = $row->lastPage();
                                    $start = $finish - 10;
                                    if($start <= 0){
                                        $start = 1;
                                    }
                                }
                                ?>
                                @for ($i = $start; $i <= $finish; $i++)
                                    <li class="{{ ($row->currentPage() == $i) ? 'active-paginator' : '' }}">
                                        <a href="?{{preg_replace('~(\?|&)page=[^&]*~','',http_build_query($_GET))}}&page={{ $i }}">{{ $i }}</a>
                                    </li>
                                @endfor
                                <li>
                                    <a @if($row->currentPage() < $row->lastPage()) href="?{{preg_replace('~(\?|&)page=[^&]*~','',http_build_query($_GET))}}&page={{ $row->currentPage() + 1 }}" @endif>&raquo;</a>
                                </li>
                            </ul>
                        </div>

                    @endif

                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->

    <script src="/admin/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <link href="/fancybox/jquery.fancybox.css" type="text/css" rel="stylesheet">
    <script src="/fancybox/jquery.fancybox.pack.js" type="text/javascript"></script>

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <script>
        function delItem(ob,id){
            if(confirm('Действительно хотите удалить?')){
                $(ob).closest('tr').remove();
                $.ajax({
                    type: 'DELETE',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "/admin/user/" + id,
                    success: function(data){

                    }
                });
            }
        }
    </script>

    <script type="text/javascript">
        $('a.fancybox').fancybox({
            padding: 10
        });
    </script>

@endsection