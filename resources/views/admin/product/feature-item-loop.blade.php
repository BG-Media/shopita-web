
<?

$product_item = \App\Models\ProductFeature::where('product_id',$product_id)
        ->where('item_id','>',0)->orderBy('product_feature_id','asc')->get();

$product_feature = \App\Models\ProductFeature::where('product_id',$product_id)
        ->where('feature_id',$feature->feature_id)
        ->orderBy('product_feature_id','asc')->first();
?>

@if($feature->kind_id == 3)

    <div class="form-group">
        <label>Описание</label>
        <textarea class="form-control" name="item_desc_{{$feature->feature_id}}">@if(isset($product_feature->product_feature_desc)){{$product_feature->product_feature_desc}}@endif</textarea>
    </div>

@elseif($feature->kind_id == 2)

    <div class="form-group">
        <label>Выберите</label>
        <select class="form-control" name="item_id[]">

            @foreach($item as $key => $val)

                <?$selected = '';?>

                @foreach($product_item as $item_val)

                    @if($val->item_id == $item_val->item_id)
                        <?$selected = 'selected';?>
                        @break;
                    @endif

                @endforeach

                <option value="{{$val->item_id}}" {{$selected}}>{{$val->item_name_ru}}</option>

            @endforeach

        </select>
    </div>

@elseif($feature->kind_id == 1)

    <div class="form-group">

        @foreach($item as $key => $val)

            <?$selected = '';?>

            @foreach($product_item as $item_val)

                @if($val->item_id == $item_val->item_id)
                    <?$selected = 'checked';?>
                    @break;
                @endif

            @endforeach

            <div>
                <div class="left-float delivery-checkbox">
                    <input name="item_id[]" {{ $selected }} type="checkbox" value="{{ $val->item_id }}"/>
                </div>
                <div class="left-float">
                    <span>{{ $val->item_name_ru }}</span>
                </div>
                <div class="clear-float"></div>
            </div>

        @endforeach

    </div>


@endif




