@extends('layout.admin-layout')

@section('content')

<div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
            <h3 class="box-title" style="margin-top: 5px">База</h3>
            <a href="/admin/delivery-kind/create" style="float: right">
               <button class="btn btn-primary" style="margin-bottom: 10px; ">Добавить новую базу</button>
            </a>
            <div style="clear: both"></div>
        </div>

        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr style="border: 1px">
                <th style="width: 30px">№</th>
                <th>Название</th>
                <th style="width: 30px"></th>
                <th style="width: 30px"></th>
              </tr>
            </thead>

            <tbody>

                  @foreach($row as $key => $val)

                     <tr>
                        <td> {{ $key + 1 }}</td>
                        <td>
                            <a style="color: rgb(30, 111, 158); text-decoration: underline;" href="/admin/contact-delivery?delivery_kind_id={{$val->delivery_kind_id}}">
                                {{ $val->delivery_kind_name }} (<? echo \App\Models\Subscriber::where('delivery_kind_id',$val->delivery_kind_id)->count();?>)
                            </a>
                        </td>

                        <td style="text-align: center">
                            <a href="javascript:void(0)" onclick="delItem(this,{{ $val->delivery_kind_id }})">
                                <li class="fa fa-trash-o" style="font-size: 20px; color: red;"></li>
                            </a>
                        </td>
                        <td style="text-align: center">
                            <a href="/admin/delivery-kind/{{ $val->delivery_kind_id }}/edit">
                                <li class="fa fa-pencil" style="font-size: 20px;"></li>
                            </a>
                        </td>
                     </tr>

                  @endforeach

            </tbody>

          </table>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
    </div><!-- /.row -->

    <meta name="csrf-token" content="{{ csrf_token() }}">

       <script>
           function delItem(ob,id){
               if(confirm('Действительно хотите удалить?')){
                   $(ob).closest('tr').remove();
                   $.ajax({
                       type: 'DELETE',
                       headers: {
                           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                       },
                       url: "/admin/delivery-kind/" + id,
                       success: function(data){

                       }
                   });
               }
           }
       </script>

@endsection