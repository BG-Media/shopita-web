@extends('layout.layout')

@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="container">
                    <div class="row">

                        <div class="col-md-12 error_page">
                            <img src="/image/error.png" class="img-responsive center-block" alt="">
                            <p>Ваша страница не наидена</p>
                        </div>

                    </div>

                    @include('layout.footer')

                </div>
            </div>
        </div>
    </div>

@endsection

