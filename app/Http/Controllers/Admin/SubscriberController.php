<?php

namespace App\Http\Controllers\Admin;

use App\Models\Delivery;
use App\Models\Subscriber;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Rubric;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use App\Helpers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use DB;

class SubscriberController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');

    }
    
    public function index()
    {
        $row = Subscriber::where('delivery_kind_id','=',1)
            ->orderBy('subscriber_id','desc')
            ->select('subscriber.*',
                 DB::raw('DATE_FORMAT(created_at,"%d.%m.%Y %H:%i") as date'))
            ->get();
        return  view('admin.subscriber.subscriber',[
            'row' => $row
        ]);
    }

    public function create()
    {
        $row = '';
        return  view('admin.subscriber.subscriber-edit', [
            'title' => 'Добавить подписку',
            'row' => $row
        ]);
    }

    public function store(Request $request)
    {
        if($request->email_users != ''){
            $email_users = trim($request->email_users);
            $email_users = str_replace(" ","",$email_users);
            $email_users = str_replace("\n","",$email_users);
            $email_users = str_replace("\r","",$email_users);
            $user_list = explode(',', $email_users);
            
            foreach ($user_list as $value){
                if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                    return  view('admin.subscriber.subscriber-edit', [
                        'title' => 'Добавить рассылку',
                        'row' => $request->email_users,
                        'error' => 'Укажите правильную почту'
                    ]);
                }
            }
        }
        
        if($request->email_users != ''){
            $email_users = trim($request->email_users);
            $email_users = str_replace(" ","",$email_users);
            $email_users = str_replace("\n","",$email_users);
            $email_users = str_replace("\r","",$email_users);
            $user_list = explode(',', $email_users);
            foreach ($user_list as $value){
                $follower_email = Subscriber::where('email',$value)->first();
                if($follower_email == null){
                    $follower_email = new Subscriber();
                    $follower_email->email = $value;
                    $follower_email->delivery_kind_id = 1;
                    $follower_email->save();
                }
            }
        }
        return redirect('/admin/subscriber');
    }

    public function destroy($id)
    {
        $subscriber = Subscriber::find($id);
        $subscriber->delete();
    }
}
