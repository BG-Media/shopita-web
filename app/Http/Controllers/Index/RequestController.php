<?php

namespace App\Http\Controllers\Index;

use App\Models\Category;
use App\Models\Page;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\RequestProduct;
use App\Models\Subscriber;
use App\Models\Users;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use App\Helpers;
use Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use View;
use DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;


class RequestController extends Controller
{
    public function __construct()
    {
        $sex = Helpers::getSessionSex();
        View::share('sex', $sex);
        
        View::share('url', '/profile');
        
    }

    public function addToBasket(Request $request)
    {
        $key = $request['product_id'];
        $user_id = Helpers::getUserId();
        $session = csrf_token();
        $request_db = new \App\Models\Request();

        if($user_id > 0)
        {
            $request_session = \App\Models\Request::where('session',$session)->get();
            foreach ($request_session as $val){
                $request_row = \App\Models\Request::find($val->request_id);
                $request_row->user_id = $user_id;
                $request_row->session = null;
                $request_row->save();
            }
            $request_result = $request_db->checkExistRequestByUser($user_id);
        }
        else
        {
            $request_result = $request_db->checkExistRequestBySession($session);
        }

        $request_id = 0;
        if($request_result == null)
        {
            if($user_id > 0)
            {
                $request_db->user_id = $user_id;
            }
            else
            {
                $request_db->session = $session;
            }
            $request_db->status_id = 1;
            $request_db->save();

            $request_id = $request_db->request_id;
        }
        else
        {
            $request_id = $request_result->request_id;
        }

        $product = Product::find($request->product_id);

        if($product == null || $product->is_show == '0' || $product->product_count < 1)
        {
            $result['error'] = 'К сожалению, такого товара нет на складе!';
            $result['success'] = false;
            return response()->json($result);
        }

        if($product->product_count < $request->unit)
        {
            $result['error'] = 'К сожалению, на складе нет такого количества товара!';
            $result['success'] = false;
            return response()->json($result);
        }


        $request_product = new RequestProduct();
        $request_product->product_id = $request->product_id;
        $request_product->request_id = $request_id;
        $request_product->price = $product->product_price;
        $request_product->product_size = $request['size'];
        $request_product->unit = $request->unit;
        $request_product->product_feature_desc = 'ok';
        $request_product->save();

        $result['message'] = 'Товар успешно добавлен в корзину!</br><a href="/basket"><input type="button" value="Перейти в корзину"/></a>';
        $result['success'] = true;
//        $result = $request['size'];
        return response()->json($result);
    }

    public function getBasketCount()
    {
        $basket_count = new \App\Models\Request();
        $result['count'] = $basket_count->getProductCountInBasket();
        $result['success'] = true;
        return response()->json($result);
    }

    public function deleteProductFromBasket(Request $request)
    {
        $user_id = Helpers::getUserId();
        $session = csrf_token();

        if($user_id > 0)
        {
            $row = \App\Models\Request::where('user_id','=',$user_id)
                ->where('status_id','=',1)
                ->first();
        }
        else
        {
            $row = \App\Models\Request::where('session','=',$session)
                ->where('status_id','=',1)
                ->first();
        }

        $request_id = 0;
        if(isset($row->request_id))
        {
            $request_id = $row->request_id;
        }

        $request_product = RequestProduct::where('request_id',$request_id)
            ->where('product_id',$request->product_id)->first();
        $request_product->delete();

        $request_product = new RequestProduct();
        $result['success'] = true;
        $result['total_sum'] = $request_product->getSumByRequest($request_id);
        
        return response()->json($result);
    }

    public function changeProductUnitInBasket(Request $request)
    {
        $user_id = Helpers::getUserId();
        $session = csrf_token();

        if($user_id > 0)
        {
            $row = \App\Models\Request::where('user_id','=',$user_id)
                ->where('status_id','=',1)
                ->first();
        }
        else
        {
            $row = \App\Models\Request::where('session','=',$session)
                ->where('status_id','=',1)
                ->first();
        }

        $request_id = 0;
        if(isset($row->request_id))
        {
            $request_id = $row->request_id;
        }

        $request_product = RequestProduct::where('request_id',$request_id)
            ->where('product_id',$request->product_id)->first();

        $product_db = New Product();
        $result = $product_db->isAvailableProduct($request_product->product_id,$request->unit);
        if($result['result'] == false)
        {
            $result['success'] = false;
            return response()->json($result);
        }

        $request_product->unit = $request->unit;
        $request_product->save();
        
        $request_product = new RequestProduct();
        $result['success'] = true;
        $result['total_sum'] = $request_product->getSumByRequest($request_id);
        return response()->json($result);
    }

    public function getProductListInBasket(Request $request)
    {
        $user_id = Helpers::getUserId();
        $session = csrf_token();

        if($user_id > 0)
        {
            $row = \App\Models\Request::where('user_id','=',$user_id)
                ->where('status_id','=',1)
                ->first();
        }
        else
        {
            $row = \App\Models\Request::where('session','=',$session)
                ->where('status_id','=',1)
                ->first();
        }

        $request_id = 0;
        if(isset($row->request_id))
        {
            $request_id = $row->request_id;
        }

        $basket = RequestProduct::leftJoin('product','product.product_id','=','request_product.product_id')
            ->where('request_id',$request_id)
            ->select('product.*','request_product.*')
            ->get();
        
        if(!isset($request->is_loop))
        {
            return  view('index.basket.basket',[
                'basket' => $basket,
            ]);
        }
        else {
            return  view('index.basket.basket-list-loop',[
                'basket' => $basket,
            ]);
        }
    }

    public function addOrder(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_name' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
            'city' => 'required',
            'address' => 'required'
        ]);

        if ($validator->fails())
        {
            $messages = $validator->errors();
            $error = $messages->all();
            $result['error_list'][0]['order_user_name'] = $messages->first('user_name');
            $result['error_list'][1]['order_phone'] = $messages->first('phone');
            $result['error_list'][2]['order_email'] = $messages->first('email');
            $result['error_list'][3]['order_city'] = 'Укажите город';
            $result['error_list'][4]['order_address'] = 'Укажите адрес';
            $result['error'] = $error[0];
            $result['success'] = false;
            return response()->json($result);
        }

        $user_id = Helpers::getUserId();
        $session = csrf_token();

        if ($user_id > 0)
        {
            $row = \App\Models\Request::where('user_id','=',$user_id)
                ->where('status_id','=',1)
                ->first();
        }
        else
        {
            $row = \App\Models\Request::where('session','=',$session)
                ->where('status_id','=',1)
                ->first();
        }

        $request_id = 0;
        if(isset($row->request_id))
        {
            $request_id = $row->request_id;
        }

        $product_list = RequestProduct::leftJoin('product','product.product_id','=','request_product.product_id')
            ->where('request_id',$request_id)
            ->select('request_product.*','product.product_name_ru')
            ->get();

        $product_db = New Product();
        foreach ($product_list as $val)
        {
            $result = $product_db->isAvailableProduct($val->product_id,$val->unit);
            if($result['result'] == false)
            {
                $result['success'] = false;
                $result['error'] = $result['error'] .$val->product_name_ru;
                return response()->json($result);
            }
        }

        $str_message = '';
        
        if($user_id == 0)
        {
            $user = Users::where('email',$request->email)->first();
            if($user != null)
            {
                $user_id = $user->user_id;
                $user->city = $request->city;
                $user->address = $request->address;
                $user->save();
            }
            else
            {
                $user = new Users();
                $user->name = $request->user_name;
                $user->city = $request->city;
                $user->address = $request->address;
                $user->email = $request->email;
                $user->mobile_phone = $request->phone;
                $rand_password = str_random(8);
                $user->password = Hash::make($rand_password);
                $user->save();
                $user_id = $user->user_id;

                $delivery_kind = DB::table('delivery_kind')->pluck('delivery_kind_id');
                foreach ($delivery_kind as $item)
                {
                    Db::table('subscriber')->insert([
                        'email'=>$user->email,
                        'email_is_send'=>1,
                        'delivery_kind_id'=>$item,
                        'name'=>$user->name,
                        'phone'=>$user->phone
                    ]);
                }

                $userdata = array(
                    'email' => $request->email,
                    'password' => $rand_password
                );

                Auth::attempt($userdata);

                $str_message = 'Вы автоматически зарегистрированы на нашем сайте!</br>Новый пароль отправлен на Вашу почту!';
            }
        }

        $request_row = \App\Models\Request::find($request_id);
        $request_row->status_id = 2;
        $request_row->user_name = $request->user_name;
        $request_row->email = $request->email;
        $request_row->city = $request->city;
        $request_row->address = $request->address;
        $request_row->phone = $request->phone;
        $request_row->is_pickup = $request->is_pickup;
        $request_row->user_id = $user_id;
        $request_row->session = null;
        $request_row->size = Session::get('size');
        $request_row->is_epay = $request->is_pay;
        $request_row->is_pickup = $request->is_pickup;
        $request_row->save();

        $result['success'] = true;

        foreach ($product_list as $val)
        {
            $product = Product::where('product_id','=',$val->product_id)->first();
            $count = $product->product_count - $val->unit;
            $product->product_count = $count;
            $product->save();
        }

        if($request_row->is_epay > 0){
            $result['is_epay'] = true;
            $result['url'] = '/epay/process/'.$request_id;
            $result['message'] = $str_message;
        }
        elseif(Helpers::getUserId() > 0)
        {
            $result['url'] = 'profile?page_tab=2&modal=openModal4&modal_msg=' .$str_message;
        }
        else
        {
            $str_message = 'Войдите в личный кабинет, чтобы увидеть ваши заказы';
            $result['url'] = 'index?modal=openModal4&modal_msg=' .$str_message;
        }
        return response()->json($result);
    }

    public function epayProcess(Request $request,$request_id) {
        $request_row = \App\Models\Request::where('request_id',$request_id)->first();
        if($request_row == null || $request_row->is_epay != 1 || $request_row->is_epay_success != 0)
            return redirect('/');

        $request_product = new RequestProduct();
        $sum = $request_product->getSumByRequest($request_id);
        $total = $request_product->getProductUnitByRequest($request_id);

        return view('index.basket.epay-block', ['request_id' => $request_id,'sum' => $sum,'unit' => $total]);
    }

    public function epaySuccess(Request $request){
        require_once("kkb_cert/kkb.utils.php");
        $path1 = 'kkb_cert/config.txt';
        $result = 0;
        $file = "log.txt";
        $current = file_get_contents($file);

        if(isset($_POST["response"])){$response = $_POST["response"];};
        $result = process_response(stripslashes($response),$path1);
        if (is_array($result)){
            if (in_array("ERROR",$result)){
                if ($result["ERROR_TYPE"]=="ERROR"){
                    $current .= "System error:".$result["ERROR"];
                } elseif ($result["ERROR_TYPE"]=="system"){
                    $current .= "Bank system error > Code: '".$result["ERROR_CODE"]."' Text: '".$result["ERROR_CHARDATA"]."' Time: '".$result["ERROR_TIME"]."' Order_ID: '".$result["RESPONSE_ORDER_ID"]."'";
                }elseif ($result["ERROR_TYPE"]=="auth"){
                    $current .= "Bank system user autentication error > Code: '".$result["ERROR_CODE"]."' Text: '".$result["ERROR_CHARDATA"]."' Time: '".$result["ERROR_TIME"]."' Order_ID: '".$result["RESPONSE_ORDER_ID"]."'";
                };
            };
            if (in_array("DOCUMENT",$result)){
                $current .= "Result DATA: <BR>";
                if($result['CHECKRESULT'] == "[SIGN_GOOD]"){
                    if($result['PAYMENT_RESPONSE_CODE'] == 0){
                        $request_amount = $result['ORDER_AMOUNT'];
                        $payment_amount = $result['PAYMENT_AMOUNT'];

                        $request_id = $result['ORDER_ORDER_ID'];

                        //if($payment_amount == 100){
                        $request_row = \App\Models\Request::where('request_id',$request_id)->first();
                        if($request_row == null){
                            return 'error';
                        }

                        $request_row->is_epay_success = 1;
                        $request_row->save();

                        $request_product = new RequestProduct();
                        $sum = $request_product->getSumByRequest($request_id);

                        $reference = "";
                        $approval_code = "";

                        foreach ($result as $key => $value) {
                            if($key == "PAYMENT_REFERENCE"){
                                $reference = $value;
                            }
                            else if($key == "PAYMENT_APPROVAL_CODE"){
                                $approval_code = $value;
                            }
                        }

                        $request_str = process_complete($reference, $approval_code, $request_id, "398", $sum, 'kkb_cert/config.txt');

                        $response = file_get_contents("https://epay.kkb.kz/jsp/remote/control.jsp?" . urlencode($request_str));
                        //}
                    }
                }
                foreach ($result as $key => $value) {
                    $current .= "Postlink Result: ".$key." = ".$value."<br>";
                };
            };
        } else { $current .= "System error".$result; };
        file_put_contents($file, $current);
    }
}
