
@foreach($image as $key => $val)

        <div class="image-item @if($val->is_main == 1) main-image-item @endif" id="image_id_{{ $val['image_id'] }}">
            <input type="hidden" value="{{ $val['image_id'] }}" class="image-id">
            <a href="javascript:void(0)" onclick="confirmDeleteImage('{{ $val['image_id'] }}')">
                <i class="icon icon-times-blue-o"></i>
            </a>

            @if($val->is_main == 0)
                <a href="javascript:void(0)" onclick="changeMainImage('{{ $val['image_id'] }}')">
                    <span class="change-main-image-icon">Сделать главной</span>
                </a>
            @endif

            <a href="/image/product/{{ $val->image_url }}" class="fancybox">
                <img  src="/image/product/{{ $val->image_url }}">
            </a>
        </div>

@endforeach



