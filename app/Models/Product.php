<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class Product extends Model
{
    protected $table = 'product';
    protected $primaryKey = 'product_id';

    use SoftDeletes;
    protected $dates = ['deleted_at'];
    public $appends = ['favorited', 'brand', 'share_link', 'current_price', 'preview'];

    public function isAvailableProduct($product_id,$unit){
        $result['result'] = true;

        $row = Product::where('product_id','=',$product_id)
            ->where('is_show','=',1)
            ->count();

        if($row == 0){
            $result['error'] = 'К сожалению, на складе нет такого товара ';
            $result['result'] = false;
            return $result;
        }
        else {
            $row = Product::where('product_id','=',$product_id)
                ->where('product_count','>=',$unit)
                ->count();

            if($row == 0){
                $result['error'] = 'К сожалению, на складе нет такого количества товара ';
                $result['result'] = false;
                return $result;
            }
        }
        return $result;
    }

    public function getFavoritedAttribute()
    {
        return ProductFavorite::where('user_id', Auth::user()->user_id)->where('product_id', $this->product_id)->count();
    }

    public function getBrandAttribute()
    {
        return $this->brands()->first()->brand_name;
    }

    public function brands()
    {
        return $this->hasOne('App\Models\Brand', 'brand_id', 'brand_id');
    }

    public function getShareLinkAttribute() {
        return 'http://shopita.kz/product/'.$this->product_url.'-u'.$this->product_id;
    }

    public function categories()
    {
        return $this->belongsToMany('App\Models\Category', 'product_category', 'product_id', 'category_id');
    }

    public function getCurrentPriceAttribute()
    {
        if ($this->discount_id == 0)
        {
            return [
                'price' => $this->product_price,
                'discount' => 0,
            ];//$this->product_price;
        }

        $discount = Discount::find($this->discount_id)->percent;

        $priceWithDiscount = ($discount / 100) * $this->product_price;

        return [
            'price' => $this->product_price - $priceWithDiscount,
            'discount' => $discount,
        ];
    }

    public function getPreviewAttribute()
    {
        $image = Image::where('product_id', $this->product_id)->where('is_main', 1)->first();
        return $image->full_path;
    }
}
