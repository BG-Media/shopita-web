
<div class="clearfix table_hor">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <p>Артикул</p>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6">
        <p>{{$row->product_code}}</p>
    </div>
</div>

@foreach($product_feature as $val)

    <div class="clearfix table_hor">
        <div class="col-md-3 col-sm-4 col-xs-6">
            <p>{{$val->feature_name_ru}}</p>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-6">

            @if($val->kind_id == 3)

                <p>{{$val->product_feature_desc}}</p>

            @elseif($val->kind_id == 2 || $val->kind_id == 1)

                <?
                $product_item = \App\Models\ProductFeature::leftJoin('item','item.item_id','=','product_feature.item_id')
                        ->where('product_feature.product_id',$row->product_id)
                        ->where('product_feature.item_id','>',0)
                        ->where('item.feature_id',$val->feature_id)
                        ->select('item.*')
                        ->get();
                ?>

                <p>
                    @foreach($product_item as $key => $item_val)
                        {{$item_val->item_name_ru}}@if(isset($product_item[$key + 1])), @endif
                    @endforeach
                </p>

            @endif

        </div>
    </div>

@endforeach
