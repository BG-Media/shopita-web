<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Brand extends Model
{
    protected $table = 'brand';
    protected $primaryKey = 'brand_id';

    use SoftDeletes;
    protected $dates = ['deleted_at'];
}
