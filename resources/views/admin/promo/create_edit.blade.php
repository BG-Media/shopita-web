@extends('layout.admin-layout')

@section('content')

    <section class="content-header">
        <h1>
            {{ $title }}
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-8" style="padding-left: 0px">
                    <div class="box box-primary">
                        @if (isset($error))
                            <div class="alert alert-danger">
                                {{ $error }}
                            </div>
                        @endif

                        <div id="error_text" class="alert alert-danger" style="display: none"></div>

                        @if($row->id > 0)
                            <form action="/admin/promo-code/{{$row->id}}" method="POST">
                                <input type="hidden" name="_method" value="PUT">
                        @else
                            <form action="/admin/promo-code" method="POST">
                        @endif
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="page_id" value="{{ $row->page_id }}">

                            <div class="box-body">
                                <div class="form-group">
                                    <label>Название промокода</label>
                                    <input value="{{ $row->code_name }}" type="text" class="form-control" name="code_name" placeholder="Введите">
                                </div>
                                <div class="form-group">
                                    <label>Код</label>
                                    <input value="{{ $row->code }}" type="text" class="form-control" name="code" placeholder="Введите">
                                </div>
                                <div class="form-group">
                                    <label for="select_form">Акция</label>
                                    <select id="select_form" class="form-control" name="code_type">
                                        <option value="0" {{ $row->code_type == 0 ? 'selected': '' }}>Процент</option>
                                        <option value="1" {{ $row->code_type == 1 ? 'selected': '' }}>Тенге</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Скидка</label>
                                    <input value="{{ $row->promo_code }}" type="text" class="form-control" name="promo_code" placeholder="Введите">
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Сохранить</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <link href="/wysiwyg/default.css" rel="stylesheet"/>
    <script type="text/javascript" src="/wysiwyg/kindeditor.js"></script>
    <script type="text/javascript" src="/wysiwyg/ru_Ru.js"></script>


    <script src="/javascript/jquery-2.1.1.min.js"></script>
    <script src="/javascript/jquery.maskedinput.js"></script>
    <script src="/javascript/uploadfile.js"></script>


    <script>
        var cw = $('#avatar_img').width();
        $('#avatar_img').css('height',cw);

        function uploadImage(){
            $("#blog_image_form").submit();
        }
    </script>
@endsection

