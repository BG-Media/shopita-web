<?php

namespace App\Http\Controllers\Index;

use App\Http\Controllers\Admin\CommentController;
use App\Models\Category;
use App\Models\Comment;
use App\Models\Image;
use App\Models\Page;
use App\Models\Product;
use App\Models\ProductFavorite;
use App\Models\ProductFeature;
use App\Models\Subscriber;
use App\Models\Users;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use App\Helpers;
use Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use View;
use DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;


class ProductController extends Controller
{
    public function __construct()
    {
        $sex = Helpers::getSessionSex();
        View::share('sex', $sex);
        
        View::share('url', '/profile');
    }

    public function getProductById($product_url)
    {
        $product_id = Helpers::getIdFromUrl($product_url);
        $row = Product::leftJoin('brand','brand.brand_id','=','product.brand_id')
            ->leftJoin('product_category','product_category.product_id','=','product.product_id')
            ->leftJoin('category as subcategory','subcategory.category_id','=','product_category.category_id')
            ->leftJoin('category','subcategory.main_category_id','=','category.category_id')
            ->leftJoin('category as main_category','category.main_category_id','=','main_category.category_id')
            ->where('product.product_id',$product_id)
            ->select('brand.*',
                     'product.*',
                     'subcategory.category_id as subcategory_id',
                     'subcategory.category_url as subcategory_url',
                     'subcategory.category_name_ru as subcategory_name_ru',
                     'category.category_id as category_id',
                     'category.category_url as category_url',
                     'category.category_name_ru as category_name_ru',
                     'main_category.category_id as main_category_id',
                     'main_category.category_url as main_category_url',
                     'main_category.category_name_ru as main_category_name_ru'
            )
            ->first();

        if($row == null || (Helpers::getTranslatedSlugRu($row['product_url']) .'-u'.$product_id) != $product_url ){
            return redirect('/');
        }

        $image = Image::where('product_id',$product_id)
            ->where('is_main','0')
            ->orderBy('image_id', 'desc')
            ->take(4)
            ->get();

        $product_feature = ProductFeature::leftJoin('feature','feature.feature_id','=','product_feature.feature_id')
                                ->where('product_id',$product_id)
                                ->where('feature.feature_id','>',0)
                                ->select('feature.*','product_feature.*')
                                ->orderBy('feature.sort_num','asc')
                                ->get();

        $comment = Comment::orderBy('comment_id','asc')
            ->where('product_id','=',$product_id)
            ->where('is_show',1)
            ->select('comment.*',
                DB::raw('DATE_FORMAT(comment.created_at,"%d.%m.%Y") as date'))
            ->get();

        $product = Product::find($product_id);
        $product->view_count = $product->view_count + 1;
        $product->save();
        
        $size_type = DB::table('size_type')
            ->leftJoin('size_name_type','size_type.id','=','size_name_type.type_size_id')
            ->where('size_name_type.name_size_id',1)
            ->get(['size_type.*','size_name_type.*']);
        $product_size = DB::table('product_size')->where('product_id',$product_id)->pluck('unique_size');
        return  view('index.product.product-detail',[
            'row' => $row,
            'menu' => 'product-detail',
            'product_image' => $image,
            'product_feature' => $product_feature,
            'comment' => $comment,
            'size_type'=>$size_type,
            'product_size'=>$product_size
        ]);
    }

    public function addComment(Request $request){
        $validator = Validator::make($request->all(), [
            'message' => 'required',
            'user_name' => 'required'
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            $result['error'] = $error[0];
            $result['success'] = false;
            return response()->json($result);
        }

        $comment = new Comment();
        $comment->message = $request->message;
        $comment->user_name = $request->user_name;
        $comment->email = $request->email;
        $comment->product_id = $request->product_id;
        $comment->save();

        $result['message'] = 'Спасибо за Ваш отзыв!';
        $result['success'] = true;
        return response()->json($result);

    }

    public function getCommentListByProduct(Request $request){
        $comment = Comment::where('product_id',$request->product_id)
            ->where('is_show',1)
            ->select(['comment.*'])
            ->orderBy('comment_id','asc')
            ->get();

        return  view('index.product.comment-loop',[
            'comment' => $comment,
        ]);
    }

    public function changeFavoriteProduct(Request $request){
        $user_id = Helpers::getUserId();

        if ($user_id == 0) {
            $result['error'] = 'Вы должны авторизоваться, чтобы добавить товар в избранное!</br><a href="?url=' .$request->url. '#openModal"><input type="button" value="Войти"/></a>';
            $result['success'] = false;
            return response()->json($result);
        }

        $product = ProductFavorite::where('product_id',$request->product_id)
                        ->where('user_id',$user_id)->first();

        if($product == null){
            $product = new ProductFavorite();
            $product->product_id = $request->product_id;
            $product->user_id = $user_id;
            $product->save();
            $result['message'] = 'Успешно добавлен в избранное!</br><a href="/profile?page_tab=4"><input type="button" value="Перейти в избранное"/></a>';
            $result['image_url'] = '/image/red_s.png';
        }
        else {
            $product->delete();
            $result['message'] = 'Успешно удален из избранного!</br><a href="/profile?page_tab=4"><input type="button" value="Перейти в избранное"/></a>';
            $result['image_url'] = '/image/black_s.png';
        }

        $result['success'] = true;
        return response()->json($result);
    }

    public function getFavoriteProductCount()
    {
        $result['count'] = ProductFavorite::where('user_id',Helpers::getUserId())->count();;
        $result['success'] = true;
        return response()->json($result);
    }
}
