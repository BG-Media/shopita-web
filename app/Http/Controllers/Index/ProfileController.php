<?php

namespace App\Http\Controllers\Index;

use App\Models\Category;
use App\Models\Page;
use App\Models\ProductFavorite;
use App\Models\Users;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use App\Helpers;
use Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use View;
use DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;


class ProfileController extends Controller
{
    public function __construct()
    {
        $sex = Helpers::getSessionSex();
        View::share('sex', $sex);
    }

    public function index(Request $request)
    {
        if(!Auth::check())
        {
            return redirect('?url=' .$_SERVER['REQUEST_URI'] .'#openModal');
        }

        if(Auth::user()->role_id == 1)
        {
            return redirect('/admin/order');
        }

        $product = ProductFavorite::leftJoin('product','product.product_id','=','product_favorite.product_id')
            ->leftJoin('brand','brand.brand_id','=','product.brand_id')
            ->where('product.is_show','=','1')
            ->where('product_favorite.user_id','=',Helpers::getUserId())
            ->orderBy('product_favorite.product_favorite_id','desc')
            ->select('brand.*',
                'product.*'
            )
            ->paginate(8);

        $all_subscription = DB::table('delivery_kind')->get();
        $user_subscription = DB::table('subscriber')->where('email',Helpers::getUserEmail())->get();

        return  view('index.profile.profile',[
            'menu' => 'profile',
            'request' => $request,
            'product' => $product,
            'all_subscription'=>$all_subscription,
            'user_subscription'=>$user_subscription
        ]);
    }

    public function saveProfile(Request $request)
    {
        $user_id = Helpers::getUserId();
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users,email,' .$user_id .',user_id',
            'name' => 'required',
            'phone' => 'required'
        ]);

        if ($validator->fails())
        {
            $messages = $validator->errors();
            $error = $messages->all();
            $result['error_list'][0]['profile_email'] = $messages->first('email');
            $result['error_list'][1]['profile_name'] = $messages->first('name');
            $result['error_list'][2]['profile_phone'] = $messages->first('profile_phone');
            $result['error'] = $error[0];
            $result['success'] = false;
            return response()->json($result);
        }

        $user = Users::find($user_id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->mobile_phone = $request->phone;
        $user->sex = $request->sex;
        $user->save();

        $result['success'] = true;
        $result['message'] = "Успешно сохранено!";
        return response()->json($result);
    }

    public function changePassword(Request $request)
    {
        $user = Users::where('user_id','=',Auth::user()->user_id)->first();
        $count = Hash::check($request->old_password, $user->password);

        if ($count == false)
        {
            $result['error_list'][0]['profile_password'] = '1';
            $result['success'] = false;
            $result['error'] = 'Неправильный старый пароль';
            return response()->json($result);
        }

        $validator = Validator::make($request->all(), [
            'old_password' => 'required',
            'new_password' => 'required|different:old_password',
            'confirm_password' => 'required|same:new_password',
        ]);

        if ($validator->fails())
        {
            $messages = $validator->errors();
            $error = $messages->all();
            $result['error_list'][0]['profile_password'] = '1';
            $result['error_list'][1]['new_password'] = $messages->first('new_password');
            $result['error_list'][2]['confirm_password'] = $messages->first('confirm_password');
            $result['success'] = false;
            $result['error'] = $error[0];
            return response()->json($result);
        }

        $user = Users::where('user_id','=',Auth::user()->user_id)->first();
        $user->password = Hash::make($request->new_password);
        $user->save();

        $result['success'] = true;
        $result['message'] = 'Успешно изменен';
        return response()->json($result);
    }
}
