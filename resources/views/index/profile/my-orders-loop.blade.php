<?
$order_list = \App\Models\Request::where('user_id','=',\App\Helpers::getUserId())
        ->where('status_id','!=',5)
        ->where('status_id','!=',1)
        ->select('request.*',
                 DB::raw('DATE_FORMAT(request.created_at,"%d.%m.%Y") as date')
        )
        ->orderBy('created_at','desc')
        ->paginate(8);
?>

@foreach($order_list as $val)

    <div class="products clearfix">
        <div class="col-md-2 col-sm-3 col-xs-6">
            <div class="row">
                <p>{{$val->date}}</p>
                <h6>заказ №{{$val->request_id}}</h6>
            </div>
        </div>

        <?
        $product_list = \App\Models\RequestProduct::leftJoin('product','product.product_id','=','request_product.product_id')
                ->where('request_id',$val->request_id)
                ->select('request_product.*','product.*')
                ->get();

        $product_sum = \App\Models\RequestProduct::leftJoin('product','product.product_id','=','request_product.product_id')
                ->where('request_id',$val->request_id)
                ->sum(DB::raw('price * unit'));
        ?>

        <div class="col-xs-6 hidden-sm hidden-md hidden-lg">
            <div class="row">
                <p class="many">{{$product_sum}} тг.</p>
            </div>
        </div>
        <div class="col-md-6 col-sm-5 col-xs-12">

                @foreach($product_list as $key => $value)

                    <a href="/product/{{$value->product_url}}-u{{$value->product_id}}" target="_blank">
                        <p>{{$value->product_name_ru}}</p>
                    </a>
                    <h6>{{$value->unit}} штук@if($value->unit == 1)а@elseи@endif, Артикул: {{$value->product_code}}, {{$value->product_feature_desc}},размер: {{$value->product_size}}</h6>

                @endforeach

        </div>
        <div class="col-md-2 col-sm-2">
            <div class="row">
                @if($val->status_id == 4)
                    <div class="label-status label label-success">Доставлено</div>
                @elseif($val->status_id == 2 && $val->is_pickup == 0)
                    <div class="label-status label label-warning">Ожидает доставки</div>
                @elseif($val->status_id == 5)
                    <div class="label-status label label-danger">Отменено</div>
                @elseif($val->status_id == 2 && $val->is_pickup == 1)
                    <div class="label-status label label-warning">Ожидает</div>
                @endif
            </div>
        </div>
        <div class="col-md-2 col-sm-2 hidden-xs">
            <div class="row">
                <p class="many">{{$product_sum}} тг.</p>
            </div>
        </div>
    </div>

@endforeach

<?
$page_url = preg_replace('~(\?|&)page=[^&]*~','',http_build_query($_GET)) .'&page_tab=2&';
?>

@if($order_list->lastPage() > 1)
    <div style="clear: both"></div>
    <div class="row" style="margin:0">
        <ul class="pagination">
            <li>
                <a @if($order_list->currentPage() > 1) href="?{{$page_url}}page={{ $order_list->currentPage() - 1 }}" @endif >&laquo;</a>
            </li>
            <?
            $start = 1;
            if($order_list->currentPage() >= 10){
                $start = $order_list->currentPage();
            }
            $finish = $start + 9;
            if($finish > $order_list->lastPage()){
                $finish = $order_list->lastPage();
                $start = $finish - 10;
                if($start <= 0){
                    $start = 1;
                }
            }
            ?>
            @for ($i = $start; $i <= $finish; $i++)
                <li class="{{ ($order_list->currentPage() == $i) ? 'active-pagination' : '' }}">
                    <a href="?{{$page_url}}page={{ $i }}">{{ $i }}</a>
                </li>
            @endfor
            <li>
                <a @if($order_list->currentPage() < $order_list->lastPage()) href="?{{$page_url}}page={{ $order_list->currentPage() + 1 }}" @endif>&raquo;</a>
            </li>
        </ul>
    </div>

@endif