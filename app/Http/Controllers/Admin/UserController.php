<?php

namespace App\Http\Controllers\Admin;

use App\Models\City;
use App\Models\Role;
use App\Models\Users;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Helpers;
use View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');

        $city = City::get();
        View::share('city', $city);

        $role = Role::get();
        View::share('role', $role);
    }

    public function index(Request $request)
    {
        $active = 1;

        if(!isset($request->active)){
            $active = 0;
            $request->active = 1;
        }

        $search = '';
        if(isset($request->search)) {
            $search = $request->search;
        }

        $row = Users::LeftJoin('city','city.city_id','=','users.city_id')
                      ->LeftJoin('role','role.role_id','=','users.role_id')
                      ->where('users.is_ban','=',$active)
                      ->where(function($query) use ($search){
                            $query->where('users.name','like','%' .$search .'%')
                                ->orWhere('users.mobile_phone','like','%' .$search .'%')
                                ->orWhere('users.email','like','%' .$search .'%');
                      })
                      ->orderBy('user_id','desc')
                      ->select('city.*','users.*','role.*')
                      ->paginate(10);

        return  view('admin.users.user',[
            'row' => $row,
            'title' => 'Пользователи',
            'request' => $request
        ]);
    }

    public function create()
    {
        $row = new Users();
        $row->avatar = 'ava.jpg';

        return  view('admin.users.user-edit', [
                'title' => 'Новый пользователь',
                'row' => $row
            ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:users'
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return  view('admin.users.user-edit', [
                'title' => 'Новый пользователь',
                'row' => (object) $request->all(),
                'error' => $error[0]
            ]);
        }

        $user = new Users();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->address = $request->address;
        $user->avatar = $request->avatar;
        $user->mobile_phone = $request->mobile_phone;
        $user->role_id = $request->role_id;
        $user->password = Hash::make('12345');
        $user->sex = $request->sex;
        $user->city_id = $request->city_id;
        $user->save();
        return redirect('/admin/user');
    }

    public function edit($id)
    {
        $row = Users::find($id);
        return  view('admin.users.user-edit', [
            'title' => 'Изменить пользователя',
            'row' => $row
        ]);
    }

    public function show(Request $request,$id){

    }

    public function update(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:users,email,' .$id .',user_id'
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return  view('admin.users.user-edit', [
                'title' => 'Изменить страницу',
                'row' => (object) $request->all(),
                'error' => $error[0]
            ]);
        }

        $user = Users::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->avatar = $request->avatar;
        $user->city_id = $request->city_id;
        $user->mobile_phone = $request->mobile_phone;
        $user->address = $request->address;
        $user->sex = $request->sex;
        $user->role_id = $request->role_id;
        $user->save();

        return redirect('/admin/user');

    }

    public function destroy($id)
    {
        $user = Users::find($id);
        $user->delete();
    }

    public function uploadAvatar(Request $request){
        $file = $request->image;
        $file_name = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();

        if(substr($file->getClientMimeType(), 0, 5) != 'image') {
            $result['error'] = 'Загружайте только файлы форматов JPEG, PNG';
            $result['success'] = false;
            return $result;
        }

        if(Storage::disk('avatar')->exists($file_name)){
            $file_name = date("YmdHisu").'.'.$extension;;
        }

        Storage::disk('avatar')->put($file_name,  File::get($file));
        $result['success'] = true;
        $result['file_name'] = $file_name;
        return $result;
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }

    public function changeIsBan(Request $request){
        $user = Users::find($request->user_id);
        $user->is_ban = $request->is_ban;
        $user->save();
    }

    public function password(Request $request){

        if(isset($request->old_password)){
            $user = Users::where('user_id','=',Auth::user()->user_id)->first();
            $count = Hash::check($request->old_password, $user->password);
            if($count == false){
                return  view('admin.users.password-edit',[
                    'error' => 'Неправильный старый пароль'
                ]);
            }

            $validator = Validator::make($request->all(), [
                'old_password' => 'required',
                'new_password' => 'required|different:old_password',
                'confirm_password' => 'required|same:new_password',
            ]);

            if ($validator->fails()) {
                $messages = $validator->errors();
                $error = $messages->all();
                return  view('admin.users.password-edit', [
                    'error' => $error[0]
                ]);
            }

            $user = Users::where('user_id','=',Auth::user()->user_id)->first();
            $user->password = Hash::make($request->new_password);
            $user->save();

            return  view('admin.users.password-edit', [
                'error' => 'Успешно изменен'
            ]);
        }
        return  view('admin.users.password-edit');
    }
}
