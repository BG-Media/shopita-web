@extends('layout.layout')

@section('content')

    {{--<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>--}}
    <script src="/js/jquery_1.11.0.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>

    <div class="content busket_page">
        <div class="container-fluid">
            <div class="row">
                <div class="container content_basket">

                    <div class="row">
                        <h1>Корзина</h1>
                    </div>

                    <div style="min-height: 100px">
                        <div id="basket_product_list">
                            @include('index.basket.basket-list-loop')
                        </div>

                        @include('index.basket.ordering')

                    </div>


                    @include('layout.footer')

                </div>
            </div>
        </div>
    </div>


    <script>

//        $(document).ready(function () {
//            var Timer = setInterval(function(){
//                getBasketProductList();
//            }, 10000);
//        });

        function minuscount(product_id) {
            var count = -1;
            var c = parseInt(document.getElementById('product_count_' + product_id).innerHTML);
            if (c > 1)
            {
                c = c + count;
                changeProductUnitInBasket(c, product_id);
            }
        }

        function pluscount(product_id)
        {
            var count = 1;
            var c = parseInt(document.getElementById('product_count_' + product_id).innerHTML);
            c = c + count;
            changeProductUnitInBasket(c, product_id);
        }
    </script>

    <script>

        function deleteProductFromBasket(product_id, elem) {
            $.ajax({
                url:'/basket',
                type: 'DELETE',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    product_id: product_id
                },
                success: function(data) {
                    $(elem).closest('.table_basket').remove();
                    $('#order_sum').html(data.total_sum);
                    $('#order_sum_all').html(data.total_sum);
                    getBasketCount();
                }
            });
        }

        function changeProductUnitInBasket(unit, product_id) {
            $.ajax({
                url:'/basket/unit',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    product_id: product_id,
                    unit: unit
                },
                success: function (data) {
                    if(data.success == true)
                    {
                        document.getElementById('product_count_' + product_id).innerHTML = unit;
                        $('#order_sum').html(data.total_sum);
                        $('#order_sum_all').html(data.total_sum);
                    }
                    else
                    {
                        showError(data.error);
                    }
                }
            });
        }

        function changePickup(ob, is_puckup)
        {
            $('.pickup').removeClass('active');
            $(ob).find('button').addClass('active');
            $('#order_is_pickup').val(is_puckup);
        }

        $(".pay_kind").on('click', function() {
            var $box = $(this);
            if ($box.is(":checked"))
            {
                var group = "input:checkbox[name='" + $box.attr("name") + "']";
                $(group).prop("checked", false);
                $box.prop("checked", true);
            }
            else
            {
                $box.prop("checked", false);
            }
        });

        function getBasketProductList() {
            $.ajax({
                url:'/basket?is_loop=true',
                type: 'GET',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    $('#basket_product_list').html(data);
                }
            });
        }

        function createOrder() {
            $('input').removeClass('error');
            $('.ajax-loader').fadeIn(100);
            $.ajax({
                url: '/order',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    user_name: $('#order_user_name').val(),
                    phone: $('#order_phone').val(),
                    email: $('#order_email').val(),
                    city: $('#order_city').val(),
                    address: $('#order_address').val(),
                    is_pickup: $('#order_is_pickup').val(),
                    is_pay : $('#is_pay').val()
                },
                success: function (data) {
                    $('.ajax-loader').fadeOut(100);
                    if (data.success == true)
                    {
                        if(data.is_epay == true){
                            $('#openModal5').find('.modal_msg').html(data.message);
                            $('#openModal5').modal(100);

                            setTimeout( function(){
                                window.location.href = data.url;
                            } , 5000);
                            return;

                        }
                        else window.location.href = data.url;
                    }
                    else
                    {
                        showError(data.error);
                        $(data.error_list).each(function() {
                            if (this[Object.keys(this)] !== "")
                            {
                                $('#' + Object.keys(this)).addClass('error');
                            }
                            return;
                        });
                    }
                }
            });
            return false;
        }

    </script>
    <script>
        var d =0;
        $('.input_green_block2').click(function () {
            var form = $('#code');
            $.ajax({
                type: "get",
                url :'/promo-code-activation',
                data :form.serialize(),
                success : function (data) {
                    if (data.result == 'error')
                    {
                        alert(data.code);
                    }
                    else
                    {
                        if (d == 0)
                        {
                            if (data.code_type == 0)
                            {
                                var summ_1 = parseInt($('#order_sum').text());
                                var summ_promo = (summ_1*data.code)/100;
                                $('#order_sum').text(summ_1-summ_promo);
                                $('#order_sum_all').text(summ_1-summ_promo);
                            }
                            else
                            {
                                var summ_1 = parseInt($('#order_sum').text());
                                var summ_promo = (data.code);
                                $('#order_sum').text(summ_1-summ_promo);
                                $('#order_sum_all').text(summ_1-summ_promo);
                            }
                            alert('Успешно');
                            d = 1;
                        }
                    }
                }
            })
        });



        $('#pay_kind').click(function () {
           var vals  = $(this).val();
           if (vals == 0)
           {
               $(this).val(1);
           }
           else
           {
               $(this).val(0);
           }
        });
        $('#is_pay').click(function () {
           var vals = $(this).val();
           if (vals == 0)
           {
               $(this).val(1);
           }
           else
           {
               $(this).val(0);
           }
        });
    </script>
@endsection

