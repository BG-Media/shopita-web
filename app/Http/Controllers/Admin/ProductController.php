<?php

namespace App\Http\Controllers\Admin;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Discount;
use App\Models\Feature;
use App\Models\Image;
use App\Models\Item;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductFeature;
use App\Models\ProductRecommended;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use App\Helpers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use View;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');

        $brand = Brand::orderBy('brand_name','asc')->where('is_show', 1)->get();
        View::share('brand', $brand);

        $main_category = Category::where('category_level','1')->orderBy('category_name_ru','asc')->get();
        View::share('main_category', $main_category);

        $category = Category::where('category_level','2')->orderBy('category_name_ru','asc')->get();
        View::share('category', $category);

        $subcategory = Category::where('category_level','3')->orderBy('category_name_ru','asc')->get();
        View::share('subcategory', $subcategory);

        $feature = Feature::orderBy('sort_num','asc')->get();
        View::share('feature', $feature);
    }
    
    public function index(Request $request)
    {
        $search = '';
        if(isset($request->search)){
            $search = $request->search;
        }

        $active = 0;
        if(!isset($request->active)){
            $active = 1;
            $request->active = 1;
        }
        
        $row = Product::leftJoin('brand','brand.brand_id','=','product.brand_id')
                ->orderBy('product_id','desc')
                ->where('product.is_show','=',$active)
                ->where(function($query) use ($search){
                    $query->where('product_name_ru','like','%' .$search .'%')
                    ->orWhere('brand_name','like','%' .$search .'%')
                    ->orWhere('product_code','like','%' .$search .'%');
                })
                ->select('brand.*',
                         'product.*'
                        )
                ->paginate(10);

        return  view('admin.product.product',[
            'row' => $row,
            'request' => $request
        ]);
    }

    public function create()
    {
        $row = new Product();
        $row->product_count = 10;
        $row->product_category = array();
        $name = DB::table('size_name')->get();
        $color = DB::table('color')->get();
        $discounts = Discount::where('active', 1)->get();
        return  view('admin.product.product-edit', [
                'title' => 'Добавить товар',
                'row' => $row,
                'name' => $name,
                'color'=>$color,
                'discounts' => $discounts,
        ]);
    }

    public function store(Request $request)
    {
        $request->product_category = array();

        $validator = Validator::make($request->all(), [
            'product_name_ru' => 'required',
            'product_price' => 'required',
            'color'=>'required',
            'size_title'=>'required',
            'product_size_value'=>'required'
        ]);

        $name = DB::table('size_name')->get();
        $color = DB::table('color')->get();
        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();



            return  view('admin.product.product-edit', [
                'title' => 'Добавить товар',
                'row' => (object) $request->all(),
                'name'=>$name,
                'color'=>$color,
                'error' => $error[0]
            ]);
        }

        $product = new Product();
        $product->product_name_ru = $request->product_name_ru;
        $product->product_desc_ru = $request->product_desc_ru;
        $product->product_code = $request->product_code;
        $product->brand_id = $request->brand_id;
        $product->product_price = $request->product_price;
        $product->product_old_price = $request->product_old_price;
        $product->sort_num = $request->sort_num;
        $product->is_sale = $request->is_sale;
        $product->discount = $request->discount;
        $product->tag = $request->tag;
        $product->product_count = $request->product_count;
        $product->product_url = Helpers::getTranslatedSlugRu($request->product_name_ru);
        $product->color_id = $request->color;

        if(!isset($request->subcategory[0]) || $request->subcategory[0] == ''){
            return  view('admin.product.product-edit', [
                'title' => 'Добавить товар',
                'name'=>$name,
                'color'=>$color,
                'row' => (object) $request->all(),
                'error' => 'Выберите как минимум одну категорию'
            ]);
        }

        $product->save();

        foreach($request->subcategory as $val){
            $check = new ProductCategory();
            if($val != ''){
                $check = $check->checkExistProductCategory($product->product_id,$val);
                if($check == 0){
                    $product_category = new ProductCategory();
                    $product_category->product_id = $product->product_id;
                    $product_category->category_id = $val;
                    $product_category->save();
                }
            }
        }

        if(isset($request->feature_id) && $request->feature_id[0] != 0){
            foreach($request->feature_id as $val){
                $check = new ProductFeature();
                if($val != ''){
                    $check = $check->checkExistProductFeature($product->product_id,$val);
                    if($check == 0){
                        $product_feature = new ProductFeature();
                        $product_feature->product_id = $product->product_id;
                        $product_feature->feature_id = $val;

                        $feature = Feature::find($val);
                        if($feature->kind_id == 3){
                            $product_feature->product_feature_desc = $request['item_desc_' .$val];
                        }

                        $product_feature->save();
                    }
                }
            }
        }

        if(isset($request->item_id)){
            foreach($request->item_id as $val){
                $check = new ProductFeature();
                if($val != ''){
                    $check = $check->checkExistProductItem($product->product_id,$val);
                    if($check == 0){
                        $product_item = new ProductFeature();
                        $product_item->product_id = $product->product_id;
                        $product_item->item_id = $val;
                        $product_item->save();
                    }
                }
            }
        }

        $image = Image::where('image_token',$request->_token)->get();
        foreach ($image as $value){
            $image_row = Image::where('image_id',$value->image_id)->first();
            $image_row->image_token = null;
            $image_row->product_id = $product->product_id;
            $image_row->save();
        }

        $recommended = ProductRecommended::where('product_recommended_token',$request->_token)->get();
        foreach ($recommended as $value){
            $recommended_row = ProductRecommended::where('product_recommended_id',$value->product_recommended_id)->first();
            $recommended_row->product_recommended_token = null;
            $recommended_row->product_id = $product->product_id;
            $recommended_row->save();
        }

        $product_size_value = $request['product_size_value'];
        if(isset($product_size_value)){
            if ($product_size_value != ''){
                foreach ($product_size_value as $key => $item) {
                    list($name[$key], $type[$key], $unique[$key]) = explode(",", $item);
                    DB::table('product_size')->insert([
                        'product_id' => $product->product_id,
                        'unique_size'=>$unique[$key],
                        'size_name_id'=>$name[$key],
                        'size_type_id'=>$type[$key]
                    ]);
                }
            }
        }

        return redirect('/admin/product');
    }

    public function edit($id)
    {

        $row = Product::find($id);
        $name = DB::table('size_name')->get();
        $selected_id = DB::table('product_size')->where('product_id',$id)->first(['size_name_id'])->size_name_id;
        $checked_sizes =DB::table('product_size')->where('product_id',$id)->get();

        $product_category = new ProductCategory();
        $product_category = $product_category->getProductCategory($id);
        $row->product_category = $product_category;

        $product_feature = ProductFeature::where('product_id',$id)
            ->where('feature_id','>',0)->orderBy('product_feature_id','asc')->get();
        $row->product_feature = $product_feature;
        $color = DB::table('color')->get();
        $size =  DB::table('size')
            ->where('size_name_title',1)
            ->where('size_type_id',1)
            ->latest('size_type_id','desc')
            ->join('size_type','size.size_type_id','=','size_type.id')
            ->get();

        $discounts = Discount::all();
        return  view('admin.product.product-edit', [
            'title' => 'Изменить товар',
            'row' => $row,
            'name'=>$name,
            'color'=>$color,
            'selected_id'=>$selected_id,
            'size'=>$size,
            'checked_sizes' =>$checked_sizes,
            'discounts' => $discounts,
        ]);
    }

    public function show(Request $request,$id){

    }

    public function update(Request $request,$id)
    {
        $product_category = new ProductCategory();
        $product_category = $product_category->getProductCategory($id);
        $request->product_category = $product_category;

        $product_feature = ProductFeature::where('product_id',$id)
            ->where('feature_id','>',0)->orderBy('product_feature_id','asc')->get();
        $request->product_feature = $product_feature;

        $validator = Validator::make($request->all(), [
            'product_name_ru' => 'required',
            'product_price' => 'required',
            'color'=>'required',
            'size_title'=>'required',
            'product_size_value'=>'required'

        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return  view('admin.product.product-edit', [
                'title' => 'Изменить товар',
                'row' => (object) $request->all(),
                'error' => $error[0]
            ]);
        }


        $product = Product::find($id);
        $product->product_name_ru = $request->product_name_ru;
        $product->product_desc_ru = $request->product_desc_ru;
        $product->product_code = $request->product_code;
        $product->brand_id = $request->brand_id;
        $product->product_price = $request->product_price;
        $product->discount_id = $request->discount_id;
        $product->product_old_price = $request->product_old_price;
        $product->sort_num = $request->sort_num;
        $product->is_sale = $request->is_sale;
        $product->discount = $request->discount;
        $product->tag = $request->tag;
        $product->product_count = $request->product_count;
        $product->product_url = Helpers::getTranslatedSlugRu($request->product_name_ru);
        $product->color_id = $request->color;
        $product->save();
        ProductCategory::where('product_id','=',$id)->delete();

        foreach($request->subcategory as $val){
            $check = new ProductCategory();
            if($val != ''){
                $check = $check->checkExistProductCategory($product->product_id,$val);
                if($check == 0){
                    $product_category = new ProductCategory();
                    $product_category->product_id = $product->product_id;
                    $product_category->category_id = $val;
                    $product_category->save();
                }
            }
        }
        ProductFeature::where('product_id','=',$id)->delete();

        if(isset($request->feature_id)){
            foreach($request->feature_id as $val){
                $check = new ProductFeature();
                if($val != ''){

                    $check = $check->checkExistProductFeature($product->product_id,$val);
                    if($check == 0){
                        $product_feature = new ProductFeature();
                        $product_feature->product_id = $product->product_id;
                        $product_feature->feature_id = $val;

                        $feature = Feature::find($val);

                        if (isset($feature)){
                            if($feature->kind_id == 3){
                                $product_feature->product_feature_desc = $request['item_desc_' .$val];
                            }
                            $product_feature->save();
                        }
                    }
                }
            }
        }

        if(isset($request->item_id)){
            foreach($request->item_id as $val){
                $check = new ProductFeature();
                if($val != ''){
                    $check = $check->checkExistProductItem($product->product_id,$val);
                    if($check == 0){
                        $product_item = new ProductFeature();
                        $product_item->product_id = $product->product_id;
                        $product_item->item_id = $val;
                        $product_item->save();
                    }
                }
            }
        }

        $product_size_value = $request['product_size_value'];

        if(isset($product_size_value)){
            if ($product_size_value != ''){
                DB::table('product_size')->where('product_id' ,'=',$product->product_id)->delete();
                foreach ($product_size_value as $key => $item) {
                    list($name[$key], $type[$key], $unique[$key]) = explode(",", $item);
                    DB::table('product_size')->insert([
                        'product_id' => $product->product_id,
                        'unique_size'=>$unique[$key],
                        'size_name_id'=>$name[$key],
                        'size_type_id'=>$type[$key]
                    ]);

                }
            }
        }

        return redirect('/admin/product');
    }

    public function destroy($id)
    {
        $page = Product::find($id);
        $page->delete();
    }

    public function uploadImage(Request $request){
        $file = $request->image;
        $file_name = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();

        if(substr($file->getClientMimeType(), 0, 5) != 'image') {
            $result['error'] = 'Загружайте только файлы форматов JPEG, PNG';
            $result['success'] = false;
            return $result;
        }

        if(Storage::disk('product')->exists($file_name)){
            $file_name = date("YmdHisu").'.'.$extension;;
        }

        Storage::disk('product')->put($file_name,  File::get($file));

        $image = new Image();
        $image->image_url = $file_name;
        if($request->product_id > 0){
            $image->product_id = $request->product_id;
            $check = $image->checkExistProductImage($request->product_id);
            if($check == 0){
                $image->is_main = 1;
            }
        }
        else {
            $image->image_token = $request->product_token;
            $check = $image->checkExistProductImageByToken($request->product_token);
            if($check == 0){
                $image->is_main = 1;
            }
        }

        $image->save();

        $result['success'] = true;
        $result['file_name'] = $file_name;
        return $result;
    }

    public function getImageList(Request $request){
        if(!isset($request->last_id)){
            $last_id = 0;
        }
        else {
            $last_id = $request->last_id;
        }

        if($request->product_id == 0 || $request->product_id == ''){
            $product_cond = 'image_token';
            $product_id = $request->product_token;
        }
        else {
            $product_cond = 'product_id';
            $product_id = $request->product_id;
        }

        $image = Image::where($product_cond,'=',$product_id)
            ->where('image_id','>',$last_id)
            ->orderBy('is_main', 'desc')
            ->orderBy('image_id', 'desc')
            ->get();

        return  view('admin.product.image-loop',[
            'image' => $image
        ]);
    }

    public function deleteImage(Request $request){
        $image = Image::where('image_id','=',$request->image_id)
            ->first();

        if($image === null){
            $result['success'] = false;
            return response()->json($result);
        }

        Storage::disk('product')->delete($image->image_url);
        Image::where('image_id','=',$request->image_id)->delete();

        $result['success'] = true;
        return response()->json($result);
    }

    public function changeMainImage(Request $request){
        $image_row = Image::where('image_id','=',$request->image_id)->first();

        if(isset($image_row->product_id)){
            $image = Image::where('product_id','=',$image_row->product_id)->get();
        }
        else {
            $image = Image::where('image_token','=',$image_row->image_token)->get();
        }

        foreach ($image as $val){
            $image_value = Image::where('image_id','=',$val->image_id)->first();
            $image_value->is_main = 0;
            $image_value->save();
        }

        $image_value = Image::where('image_id','=',$request->image_id)->first();
        $image_value->is_main = 1;
        $image_value->save();

        $result['success'] = true;
        return response()->json($result);
    }

    public function getProductListBySearch(Request $request){
        return  view('admin.product.product-list-for-recommended-loop',[
            'row' => $request
        ]);
    }

    public function getRecommendedProduct(Request $request){
        return  view('admin.product.product-recommended-loop',[
            'row' => $request
        ]);
    }

    public function addRecommendedProduct(Request $request){
        if(isset($request->product_id) && $request->product_id > 0){
            $recommended = new ProductRecommended();
            $recommended->product_id = $request->product_id;
            $recommended->other_product_id = $request->other_product_id;
            $recommended->save();
        }
        else {
            $recommended = new ProductRecommended();
            $recommended->product_recommended_token = $request->token;
            $recommended->other_product_id = $request->other_product_id;
            $recommended->save();
        }
        $result['success'] = true;
        return response()->json($result);
    }

    public function deleteRecommendedProduct(Request $request){
        if(isset($request->product_id) && $request->product_id > 0){
            ProductRecommended::where('product_id','=',$request->product_id)
                ->where('other_product_id','=',$request->other_product_id)
                ->delete();
        }
        else {
            ProductRecommended::where('product_recommended_token','=',$request->token)
                ->where('other_product_id','=',$request->other_product_id)
                ->delete();
        }
        $result['success'] = true;
        return response()->json($result);
    }

    public function changeIsShow(Request $request){
        $user = Product::find($request->id);
        $user->is_show = $request->is_show;
        $user->save();
    }
}
