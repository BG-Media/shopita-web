<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Comment extends Model
{
    protected $table = 'comment';
    protected $primaryKey = 'comment_id';

    public $appends = ['avatar'];

    public function getAvatarAttribute()
    {
        if ($this->user_id)
        {
            return Users::find($this->user_id)->avatar;
        }
        return '/image/avatar/ava.png';
    }
}
