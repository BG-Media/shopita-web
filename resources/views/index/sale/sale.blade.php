@extends('layout.layout')

@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="container">

                    <div class="row contentSale">
                        <ol class="breadcrumb">
                            <li><a href="/">Главная</a></li>
                            <li class="active">Скидочные купоны</li>
                        </ol>
                        <h1 class="titleSale">Скидочные купоны</h1>
                    </div>

                    <?$count = 0;?>

                    @foreach($row as $key => $value)

                        <?$count++;?>

                        @if($key % 4 == 0)
                                <div class="row rowSale">
                        @endif

                        <div class="col-md-3 col-sm-3 imgSale">
                            <a href="/sale/{{$value->page_url}}-u{{$value->page_id}}">
                                <img src="/image/sale/{{$value->page_image}}" class="img-responsive center-block sale11 @if($key % 2 != 0) sale22 @endif" alt="">
                            </a>
                            <h4>
                                <a href="/sale/{{$value->page_url}}-u{{$value->page_id}}">
                                    <b>{{$value->page_name_ru}}</b>
                                </a>
                            </h4>
                        </div>

                        @if($count == 4)
                            </div>
                            <?$count = 0;?>
                        @endif

                    @endforeach

                    @if($count != 0)
                        </div>
                    @endif

                    @if($row->lastPage() > 1)

                        <div class="row">
                            <ul class="pagination">
                                <li>
                                    <a @if($row->currentPage() > 1) href="?page={{ $row->currentPage() - 1 }}" @endif >&laquo;</a>
                                </li>
                                <?
                                $start = 1;
                                if($row->currentPage() >= 10){
                                    $start = $row->currentPage();
                                }
                                $finish = $start + 9;
                                if($finish > $row->lastPage()){
                                    $finish = $row->lastPage();
                                    $start = $finish - 10;
                                    if($start <= 0){
                                        $start = 1;
                                    }
                                }
                                ?>
                                @for ($i = $start; $i <= $finish; $i++)
                                    <li class="{{ ($row->currentPage() == $i) ? 'active-pagination' : '' }}">
                                        <a href="?page={{ $i }}">{{ $i }}</a>
                                    </li>
                                @endfor
                                <li>
                                    <a @if($row->currentPage() < $row->lastPage()) href="?page={{ $row->currentPage() + 1 }}" @endif>&raquo;</a>
                                </li>
                            </ul>
                        </div>

                    @endif


                    @include('layout.footer')


                </div>
            </div>
        </div>
    </div>

@endsection

