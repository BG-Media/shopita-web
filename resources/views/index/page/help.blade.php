@extends('layout.layout')

@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="container">
                    <div class="row content_contacts">
                        <div class="tabs-left tab_1">
                            <div class="col-md-3 col-sm-3 col-12">
                                <ul class="nav nav-tabs">
                                    <li @if(!isset($request->page) || $request->page == 1) class="active" @endif>
                                        <a href="#a" data-toggle="tab">
                                            <span>Как сделать заказ?</span>
                                        </a>
                                    </li>
                                    <li @if(isset($request->page) && $request->page == 3) class="active" @endif>
                                        <a href="#b" data-toggle="tab">
                                            <span>Как оплатить заказ?</span>
                                        </a>
                                    </li>
                                    <li @if(isset($request->page) && $request->page == 4) class="active" @endif>
                                        <a href="#c" data-toggle="tab">
                                            <span>Условия доставки</span>
                                        </a>
                                    </li>
                                    <li @if(isset($request->page) && $request->page == 48) class="active" @endif>
                                        <a href="#f" data-toggle="tab">
                                            <span>Таблица размеров</span>
                                        </a>
                                    </li>
                                    <li @if(isset($request->page) && $request->page == 5) class="active" @endif>
                                        <a href="#d" data-toggle="tab">
                                            <span>Возрат</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                            <div class="col-md-9 col-sm-9 col-12 tab-content">

                                <div class="tab-pane @if(!isset($request->page) || $request->page == 1) active @endif" id="a">
                                    <ol class="breadcrumb">
                                        <li><a href="/">Главная</a></li>
                                        <li>помощь</li>
                                        <li class="active">как сделать заказ?</li>
                                    </ol>
                                    <h2>Как сделать заказ?</h2>
                                    <div>
                                        <? $page = \App\Models\Page::where('page_id',1)->first();?>
                                        <?=$page->page_text_ru?>
                                    </div>
                                </div>

                                <div class="tab-pane @if(isset($request->page) && $request->page == 3) active @endif" id="b">
                                    <ol class="breadcrumb">
                                        <li><a href="/">Главная</a></li>
                                        <li>помощь</li>
                                        <li class="active">как оплатить заказ?</li>
                                    </ol>
                                    <h2>Как оплатить заказ?</h2>
                                    <div>
                                        <? $page = \App\Models\Page::where('page_id',3)->first();?>
                                        <?=$page->page_text_ru?>
                                    </div>
                                </div>

                                <div class="tab-pane @if(isset($request->page) && $request->page == 4) active @endif" id="c">
                                    <ol class="breadcrumb">
                                        <li><a href="/">Главная</a></li>
                                        <li>помощь</li>
                                        <li class="active">условия доставки</li>
                                    </ol>
                                    <h2>Условия доставки</h2>
                                    <div>
                                        <? $page = \App\Models\Page::where('page_id',4)->first();?>
                                        <?=$page->page_text_ru?>
                                    </div>
                                </div>

                                <div class="tab-pane @if(isset($request->page) && $request->page == 5) active @endif" id="d">
                                    <ol class="breadcrumb">
                                        <li><a href="/">Главная</a></li>
                                        <li>помощь</li>
                                        <li class="active">возврат</li>
                                    </ol>
                                    <h2>Возврат</h2>
                                    <div>
                                        <? $page = \App\Models\Page::where('page_id',5)->first();?>
                                        <?=$page->page_text_ru?>
                                    </div>
                                </div>
                                <div class="tab-pane @if(isset($request->page) && $request->page == 48) active @endif" id="f">
                                    <ol class="breadcrumb">
                                        <li><a href="/">Главная</a></li>
                                        <li>помощь</li>
                                        <li class="active">таблица размеров</li>
                                    </ol>
                                    <h2>Таблица размеров</h2>
                                    <div>
                                        <? $page = \App\Models\Page::where('page_id',48)->first();?>
                                        <?=$page->page_text_ru?>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    @include('layout.footer')

                </div>
            </div>
        </div>
    </div>

@endsection

