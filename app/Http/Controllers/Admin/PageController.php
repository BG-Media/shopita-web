<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Page;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use App\Helpers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class PageController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    public function index()
    {
        $row = Page::orderBy('page_id','desc')
                ->where('page_kind',1)
                ->get();
        return  view('admin.page.page',[
            'row' => $row
        ]);
    }

    public function create()
    {
        $row = new Page();
        return  view('admin.page.page-edit', [
                'title' => 'Добавить страницу',
                'row' => $row
            ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'page_name_ru' => 'required',
            'page_text_ru' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return  view('admin.page.page-edit', [
                'title' => 'Добавить страницу',
                'row' => (object) $request->all(),
                'error' => $error[0]
            ]);
        }

        $page = new Page();
        $page->page_name_ru = $request->page_name_ru;
        $page->page_text_ru = $request->page_text_ru;
        $page->page_kind = 1;
        $page->page_url = Helpers::getTranslatedSlugRu($request->page_name_ru);
        $page->save();
        return redirect('/admin/page');
    }

    public function edit($id)
    {
        $row = Page::find($id);
        return  view('admin.page.page-edit', [
            'title' => 'Изменить страницу',
            'row' => $row
        ]);
    }

    public function show(Request $request,$id){

    }

    public function update(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'page_name_ru' => 'required',
            'page_text_ru' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return  view('admin.page.page-edit', [
                'title' => 'Изменить страницу',
                'row' => (object) $request->all(),
                'error' => $error[0]
            ]);
        }

        $page = Page::find($id);
        $page->page_name_ru = $request->page_name_ru;
        $page->page_text_ru = $request->page_text_ru;
        $page->page_url = Helpers::getTranslatedSlugRu($request->page_name_ru);
        $page->save();
        return redirect('/admin/page');
    }

    public function destroy($id)
    {
        $page = Page::find($id);
        $page->delete();
    }
}
