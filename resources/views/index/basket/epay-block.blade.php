<?
require_once("kkb_cert/kkb.utils.php");
$self = $_SERVER['PHP_SELF'];
$path1 = 'kkb_cert/config.txt';	// Путь к файлу настроек config.dat
$order_id = $request_id;				// Порядковый номер заказа - преобразуется в формат "000001"
$currency_id = "398"; 			// Шифр валюты  - 840-USD, 398-Tenge
$amount = $sum;				// Сумма платежа
$content = process_request($order_id,$currency_id,$amount,$path1); // Возвращает подписанный и base64 кодированный XML документ для отправки в банк
$appendix = "";

$xml = '<document><item number="1" name="Товар" quantity="1" amount="'.$sum.'"/></document>';
$appendix = base64_encode($xml);

?>

<form name="SendOrder" method="post" action="https://epay.kkb.kz/jsp/process/logon.jsp" style="margin: 0px 0px 0px; display: none;">
    <input type="hidden" name="Signed_Order_B64" value="<?=$content?>">
    <input type="hidden" name="Language" value="rus">
    <input type="hidden" name="BackLink" value="http://shopita.kz?modal=openModal4">
    <input type="hidden" name="PostLink" value="http://shopita.kz/epay/success">
    <input type="hidden" name="FailureBackLink" value="http://shopita.kz">
    <input type="hidden" name="appendix" value="<?=$appendix?>"/>
    <input type="submit" name="GotoPay" class="btn purchase-btn pull-right pay-by-epay-btn" value="Оплатить" style="margin-right: 0px; margin-bottom: 5px;">
    <div class="clearfix"></div>
</form>

<script src="/js/jquery_1.11.0.min.js"></script>
<script>

    $(document).ready(function(){
        $(".pay-by-epay-btn").click();
    })
</script>