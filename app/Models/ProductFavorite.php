<?php

namespace App\Models;

use App\Helpers;
use Illuminate\Database\Eloquent\Model;


class ProductFavorite extends Model
{
    protected $table = 'product_favorite';
    protected $primaryKey = 'product_favorite_id';
    public $fillable = ['user_id', 'product_id'];

    public function checkExistProductFavorite($product_id){
        $user_id = Helpers::getUserId();
        $row = ProductFavorite::where('product_id','=',$product_id)
            ->where('user_id','=',$user_id)
            ->count();
        return $row;
    }
}
