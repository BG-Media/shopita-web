<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\PageText;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class PageTextController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        $text = PageText::where('id', 1)->get();

        return view('admin.page-text.text', compact('text'));
    }

    public function edit($id)
    {
        $text = PageText::find($id);

        return  view('admin.page-text.text-edit', [
            'title' => 'Изменить текст',
            'text' => $text
        ]);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'page_text' => 'required'
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();

            return view('admin.page-text.text-edit', [
                'title' => 'Изменить текст',
                'text' => (object) $request->all(),
                'error' => $error[0]
            ]);
        }
        $text = PageText::find($id);
        $text->page_text = $request->page_text;
        $text->save();

        return redirect('/admin/main-page-text');
    }
}