<?php

namespace App\Http\Controllers\Index;

use App\Models\Category;
use App\Models\Page;
use App\Models\Subscriber;
use App\Models\Users;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use App\Helpers;
use Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use function PHPSTORM_META\elementType;
use View;
use DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use App\Models\PromoCode;
use App\Models\PageText;

class IndexController extends Controller
{
    public function __construct()
    {
        $sex = Helpers::getSessionSex();
        View::share('sex', $sex);
        
        View::share('url', '/profile');

        $cache = Cache::get('mobile');

        $data = [
            'cache' => $cache
        ];
        view()->share($data);
    }

    public function index()
    {
        $brand = Page::where('page_kind',4)
                    ->orderBy('page_id','desc')
                    ->get();
        $pageText = PageText::first();

        return  view('index.index',[
            'brand' => $brand,
            'menu' => 'index',
            'pageText' => $pageText
        ]);
    }

    public function help(Request $request)
    {
        return  view('index.page.help',[
            'request' => $request,
            'menu' => 'help'
        ]);
    }

    public function about(Request $request)
    {
        return  view('index.page.about',[
            'request' => $request,
            'menu' => 'about'
        ]);
    }

    public function blog(Request $request)
    {
        $row = Page::where('page_kind',2)
            ->orderBy('page_id','desc')
            ->paginate(8);

        return  view('index.blog.blog',[
            'menu' => 'blog',
            'row' => $row
        ]);
    }

    public function blogDetail($blog_url)
    {
        $blog_id = Helpers::getIdFromUrl($blog_url);

        $row = Page::where('page_id',$blog_id)->first();

        if ($row == null || (Helpers::getTranslatedSlugRu($row['page_url']) .'-u'.$blog_id) != $blog_url )
        {
            return redirect('/');
        }

        return  view('index.blog.blog-detail',[
            'row' => $row,
            'menu' => 'blog'
        ]);
    }

    public function sale(Request $request)
    {
        $row = Page::where('page_kind',5)
            ->orderBy('page_id','desc')
            ->paginate(8);

        return  view('index.sale.sale',[
            'menu' => 'sale',
            'row' => $row
        ]);
    }

    public function saleDetail($sale_url)
    {
        $sale_id = Helpers::getIdFromUrl($sale_url);

        $row = Page::where('page_id',$sale_id)->first();

        if ($row == null || (Helpers::getTranslatedSlugRu($row['page_url']) .'-u'.$sale_id) != $sale_url )
        {
            return redirect('/');
        }

        return  view('index.sale.sale-detail',[
            'row' => $row,
            'menu' => 'sale'
        ]);
    }

    public function pageDetail($page_url)
    {
        $page_id = Helpers::getIdFromUrl($page_url);

        $row = Page::where('page_id',$page_id)->first();

        if ($row == null || (Helpers::getTranslatedSlugRu($row['page_url']) .'-u'.$page_id) != $page_url )
        {
            return redirect('/');
        }

        return  view('index.page.page-detail',[
            'row' => $row,
            'menu' => 'sale'
        ]);
    }

    public function subscribe(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email'
        ]);

        if ($validator->fails())
        {
            $messages = $validator->errors();
            $error = $messages->all();

            $result['error_list'][0]['subscribe_email'] = $messages->first('email');
            $result['error'] = $error[0];
            $result['success'] = false;
            return response()->json($result);
        }

        $subscriber = Subscriber::where('email',$request->email)->count();
        if ($subscriber > 0)
        {
            $result['error'] = 'Такая электронная почта уже подписана';
            $result['error_list'][0]['subscribe_email'] = '1';
            $result['success'] = false;
            return response()->json($result);
        }

        /*$subscriber = new Subscriber();
        $subscriber->email = $request->email;
        $subscriber->delivery_kind_id = 1;
        $subscriber->save();*/

        $delivery_kind = DB::table('delivery_kind')->pluck('delivery_kind_id');
        foreach ($delivery_kind as $item)
        {
            Db::table('subscriber')->insert([
                'email'=>$request->email,
                'email_is_send'=>1,
                'delivery_kind_id'=>$item
            ]);
        }

        $result['message'] = 'Успешно добавлен.';
        $result['success'] = true;
        return response()->json($result);
    }

    public function mobile(Request $request)
    {
        if ($request['mobile']== 1)
        {
            setcookie("mob","1");
        }
        elseif ($request['mobile']== 0)
        {
            setcookie("mob","0");
        }
        return back();
    }


    public function getSearchByProduct(Request $request)
    {
        $query = $request['query'];
        if (isset($query))
        {
            $product = DB::table('product')->orWhere('tag','like','%'.$query.'%')
                ->leftJoin('image','product.product_id','=','image.product_id')
                ->where('image.is_main',1)
                ->select(['product.*','image.image_url'])
                ->get();

            return view('index.search.search',compact('product','query'));
        }
    }

    public function promoCodeActivation(Request $request)
    {
        if ($request['code'])
        {
            $promo_code = PromoCode::where('code',$request['code'])->first();
            if (count($promo_code))
            {
                if ($promo_code->code_type == 0)
                {
                    $code_type = 0;
                }
                elseif ($promo_code->code_type == 1)
                {
                    $code_type = 1;
                }
                $result['code_type'] = $code_type;
                $result['result'] = 'ok';
                $result['code'] = $promo_code['promo_code'];
            }
            else
            {
                $result['result'] = 'error';
                $result['code'] = 'Код неправильный';
            }
            return $result;
        }


    }


    public function subscriptionProfile(Request $request)
    {
        if ($request['check_input'])
        {
            DB::table('subscriber')->where('email',\Auth::user()->email)->delete();
            foreach ($request['check_input'] as $item)
            {
                DB::table('subscriber')->insert([
                   'email'=>\Auth::user()->email,
                    'email_is_send'=>1,
                    'name'=>\Auth::user()->name,
                    'delivery_kind_id'=>$item
                ]);
            }
        }
        return $result['result'] = 'true';
    }

}
