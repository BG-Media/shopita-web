<div class="col-md-2 col-sm-2">
    <div class="row well sidebar-nav">
        <ul class="nav nav-list">

            <?
            $main_category = \App\Models\Category::where('category_level',1)
                    ->where('is_male',$sex)
                    ->orderBy('sort_num','asc')
                    ->get();
            ?>

            @foreach($main_category as $val)

                <li class="title_list_li">
                    <a href="#" data-toggle="collapse" data-target="#toggle_{{$val->category_id}}" class="title_list @if($request->main_category['category_id'] == $val->category_id) active-label @endif">{{$val->category_name_ru}}</a>
                </li>
                <ul id="toggle_{{$val->category_id}}" class="collapse out @if($request->main_category['category_id'] == $val->category_id) in @endif">

                    <?
                    $category = \App\Models\Category::where('category_level',2)
                            ->where('main_category_id',$val->category_id)
                            ->where('is_show','!=',0)
                            ->orderBy('sort_num','asc')
                            ->get();
                    ?>

                    @foreach($category as $value)

                            <li data-toggle="collapse" data-target="#toggle_{{$value->category_id}}">
                                <div class="container1">
                                    <div class="bars">
                                        <div class="bar1"></div>
                                        <div class="bar3"></div>
                                    </div>
                                    <div class="bars">

                                        @if($value->has_child == 1)

                                            <a href="#" class="@if($request->category['category_id'] == $value->category_id) active-label @endif" data-toggle="collapse" data-target="#toggle_{{$value->category_id}}">{{$value->category_name_ru}}</a>

                                        @else

                                            <a href="/catalog?category={{$value->category_url}}-u{{$value->category_id}}<?if(isset($request->sale)){?>&sale=1<?}?>" class="@if($request->category['category_id'] == $value->category_id) active-label @endif">{{$value->category_name_ru}}</a>

                                        @endif

                                    </div>
                                </div>
                            </li>

                            @if($value->has_child == 1)

                                <ul id="toggle_{{$value->category_id}}" class="collapse out insideList @if($request->category['category_id'] == $value->category_id) in @endif">

                                    <?
                                    $subcategory = \App\Models\Category::where('category_level',3)
                                            ->where('main_category_id',$value->category_id)
                                            ->orderBy('sort_num','asc')
                                            ->get();
                                    ?>

                                    @foreach($subcategory as $key => $subcategory_value)

                                         @if($key == 0)

                                                <li>
                                                    <a class="@if(!isset($request->subcategory) && $request->category['category_id'] == $value->category_id) active-label @endif" href="/catalog?category={{$value->category_url}}-u{{$value->category_id}}<?if(isset($request->sale)){?>&sale=1<?}?>">Все</a>
                                                </li>

                                         @endif

                                         <li>
                                             <a class="@if($request->subcategory['category_id'] == $subcategory_value->category_id) active-label @endif" href="/catalog?subcategory={{$subcategory_value->category_url}}-u{{$subcategory_value->category_id}}<?if(isset($request->sale)){?>&sale=1<?}?>">{{$subcategory_value->category_name_ru}}</a>
                                         </li>

                                    @endforeach

                                </ul>

                            @endif

                    @endforeach


                </ul>

            @endforeach

        </ul>
    </div>
</div>