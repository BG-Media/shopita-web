<div id="openModal" class="modalDialog">
    <div class="clearfix">
        <div class="col-md-6">
            <div class="inputs">
                <h2>Вход</h2>
                <p>E-mail</p>
                <input type="email" id="login_email">
                <br>
                <br>
                <p>Пароль</p>
                <input type="password" id="login_password">
                <p>
                    <a href="#openModal2" class="modal_link">Забыли пароль?</a>
                </p>
                <br>
                <div>
                    <a style="margin-left: 0px" href="javascript:void(0)" onclick="login()">
                        <input type="submit" value="Войти" class="submits">
                    </a>
                </div>
                <br>
                <p>Или <a href="#openModal1" class="modal_link">Регистрация</a></p>
            </div>
        </div>
        <div class="col-md-6 modal_social">
            <a href="#close" title="Закрыть" class="close">&times;</a>
            <script src="//ulogin.ru/js/ulogin.js"></script>
            <div style="position: relative; margin-top: 123px; padding-left: 60px;" id="uLogin" data-ulogin="display=panel;theme=classic;fields=first_name,last_name; optional=phone,email,photo,photo_big; providers=vkontakte,odnoklassniki,mailru,facebook;hidden=other;redirect_uri=http%3A%2F%2Fshopita.kz%2Fsocial-login;mobilebuttons=0;"></div>
        </div>

    </div>
</div>
<div id="openModal1" class="modalDialog1">
    <div class="clearfix">

        <a href="#close" title="Закрыть" class="close">&times;</a>
        <div class="col-md-6 ">
            <div class="inputs">
                <h2>Регистрация</h2>
                <p>Имя</p>
                <input type="text" id="register_name">
                <br>
                <p>E-mail</p>
                <input type="email" id="register_email">
                <br>
                <p>Телефон</p>
                <input type="text" id="register_phone">
                <br>
                <p>Пароль</p>
                <input type="password" id="register_password">
                <br>
                <p>Повторите пароль</p>
                <input type="password" id="confirm_password">
                <br>
                <br>
                <div>
                    <a style="margin-left: 0px" href="javascript:void(0)" onclick="registerUser()">
                        <input type="submit" value="Зарегистрироваться" class="submits">
                    </a>
                </div>
                <br>
            </div>
        </div>
        <div class="col-md-6 modal_social">
            <script src="//ulogin.ru/js/ulogin.js"></script>
            <div style="position: relative; margin-top: 123px; padding-left: 29px;" id="uLogin2" data-ulogin="display=panel;theme=classic;fields=first_name,last_name; optional=phone,email,photo,photo_big; providers=vkontakte,odnoklassniki,mailru,facebook;hidden=other;redirect_uri=http%3A%2F%2Fshopita.kz%2Fsocial-login;mobilebuttons=0;"></div>
        </div>

    </div>
</div>
<div id="openModal2" class="modalDialog2">
    <div class="clearfix">

        <a href="#close" title="Закрыть" class="close">&times;</a>
        <div class="inputs">
            <h2>Восстановление пароля</h2>
            <p>E-mail</p>
            <input type="email" id="reset_email">
            <span>На ваш e-mail придет сообщение с  инструкцей на восстановление</span>
            <br>
            <br>
            <div>
                <a style="margin-left: 0px" href="javascript:void(0)" onclick="resetPassword()">
                    <input type="submit" value="Отправить" class="submits">
                </a>
            </div>
            <br>
        </div>

    </div>
</div>

<div id="openModal4" class="modalDialog2">
    <div class="clearfix">
        <a href="javascript:void(0)" onclick="$('#openModal4').modal('hide')" title="Закрыть" class="close">&times;</a>
        <div class="inputs">
            <h2><b>Ваш заказ успешно принят!</b></h2>
            <p>Спасибо что выбрали нас!</p>
            <p>Наш оператор свяжется с Вами</p>
            <br>
            <p class="modal_msg"></p>
            <br>
        </div>

    </div>
</div>

<div id="openModal5" class="modalDialog2">
    <div class="clearfix">
        <a href="javascript:void(0)" onclick="$('#openModal5').modal('hide')" title="Закрыть" class="close">&times;</a>
        <div class="inputs">
            <h2><b>Ваш заказ успешно принят!</b></h2>
            <p>Через несколько секунд Вы автоматически перейдете на страницу оплаты</p>
            <p>Наш оператор свяжется с Вами!</p>
            <br>
            <p class="modal_msg"></p>
            <br>
        </div>

    </div>
</div>