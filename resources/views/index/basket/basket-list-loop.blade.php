<? $sum = 0; ?>
@if($basket->count() > 0)
    <div class="row clearfix hidden-xs" >
        <div class="col-md-4 col-sm-5 ">
            <h4 class="title_table"><span class="basket_count">{{$basket->count()}}</span> товара</h4>
        </div>
        <div class="col-md-3 col-sm-2 ">
            <h4 class="title_table">Характеристика</h4>
        </div>
        <div class="col-md-2 col-sm-2 ">
            <h4 class="title_table">Кол-ва</h4>
        </div>
        <div class="col-md-2 col-sm-3 ">
            <h4 class="title_table">Цена</h4>
        </div>
    </div>

    @foreach($basket as $val)

        <? $sum += $val->unit * $val->product_price; ?>

        <div class="row clearfix table_basket">
            <div class="col-md-4 col-sm-5">
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-5">
                        <?
                            $image = \App\Models\Image::where('product_id','=',$val->product_id)
                                    ->where('is_main','=','1')
                                    ->first();

                            $image_url = 'default.jpg';
                            if (isset($image->image_url)) {
                                $image_url = $image->image_url;
                            }
                        ?>
                        <a href="/product/{{ $val->product_url }}-u{{ $val->product_id }}" target="_blank">
                            <img src="/image/product/{{$image_url}}" class="img-responsive " alt="">
                        </a>

                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-7">
                        <a href="/product/{{$val->product_url}}-u{{$val->product_id}}" target="_blank">
                            <p><b>{{$val->product_name_ru}}</b></p>
                        </a>
                        <p>Артикул: {{$val->product_code}}</p>
                        <p class=" hidden-lg hidden-md hidden-sm">Размер: {{ $val->product_size }}</p>
                        <p class=" hidden-lg hidden-md hidden-sm">Цена: {{ $val->product_price }} тг.</p>

                        <h6>Осталось в наличии:  {{$val->product_count}}</h6>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-2 hidden-xs" >
                <p>Размер: {{$val->product_size}} </p>
                {{--<p>Цвет: зеленый</p>--}}
            </div>
            <div class="col-md-2 col-sm-2 hidden-xs">
                <div class="btn-group" role="group" aria-label="...">
                    <button onclick="minuscount('{{$val->product_id}}','{{$val->product_price}}')" type="button" class="btn btn-default add">
                        <img src="/image/1.png" alt="">
                    </button>
                    <button disabled="" type="button" class="btn btn-default add add1" id="product_count_{{$val->product_id}}">{{$val->unit}}</button>
                    <button onclick="pluscount('{{$val->product_id}}','{{$val->product_price}}')" type="button" class="btn btn-default add">
                        <img src="/image/2.png" alt="">
                    </button>
                </div>
            </div>
            <div class="col-md-2 col-sm-3 hidden-xs">
                <p><b>{{$val->price}} тг.</b></p>

                <a href="javascript:void(0)" onclick="deleteProductFromBasket('{{ $val->product_id }}', this)" class="pull-right">
                    <img src="/image/3.png" alt="">
                </a>

                @if($val->is_sale == 1)

                    <p class="old_price">{{$val->product_old_price}} тг.</p>
                    <span class="price_name">скидка {{$val->discount}}%</span>

                @endif

            </div>
            <div class="col-md-12 hidden-lg hidden-md hidden-sm delete text-center">
                <a href="javascript:void(0)" onclick="deleteProductFromBasket('{{ $val->product_id }}', this)">
                    <button class="delete-btn">Удалить с корзины</button>
                </a>
            </div>
        </div>

    @endforeach

    <script>

        $(document).ready(function () {
            $('#ordering').fadeIn(100);
            $('#order_sum').html('<?=$sum?>');
            $('#order_sum_all').html('<?=$sum?>');
        });

    </script>

@else

    <div>
        <p style="font-size: 19px">У вас нет товаров в корзине!</p>
    </div>

    <script>
        $(document).ready(function () {
            $('#ordering').fadeOut(100);
        });
    </script>

@endif
<script>
    function load() {
        var _cookie = Cookies.get('name');
        document.getElementById("razmer").innerHTML = ' Размер: ' + _cookie;
        console.log(cookie);
    }
</script>

