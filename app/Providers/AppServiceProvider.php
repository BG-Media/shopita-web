<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $social = \App\Models\Page::whereIn('page_id',[49,50,51])->get();
        $commentsCount = \App\Models\Comment::where('is_show', 0)->count();
        View::share(['social' => $social, 'commentsCount' => $commentsCount]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
