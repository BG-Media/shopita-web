<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Page;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use App\Helpers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class BlogController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    public function index(Request $request)
    {
        $search = '';
        if(isset($request->search)){
            $search = $request->search;
        }
        
        $row = Page::orderBy('page_id','desc')
                ->where('page_kind',2)
                ->where(function($query) use ($search){
                    $query->where('page_name_ru','like','%' .$search .'%');
                })
                ->paginate(10);

        return  view('admin.blog.blog',[
            'row' => $row,
            'request' => $request
        ]);
    }

    public function create()
    {
        $row = new Page();
        $row->page_image = 'default.jpg';

        return  view('admin.blog.blog-edit', [
                'title' => 'Добавить блог',
                'row' => $row
            ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'page_name_ru' => 'required',
            'page_text_ru' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return  view('admin.blog.blog-edit', [
                'title' => 'Добавить блог',
                'row' => (object) $request->all(),
                'error' => $error[0]
            ]);
        }

        $page = new Page();
        $page->page_name_ru = $request->page_name_ru;
        $page->page_text_ru = $request->page_text_ru;
        $page->page_image = $request->page_image;
        $page->page_kind = 2;
        $page->page_url = Helpers::getTranslatedSlugRu($request->page_name_ru);
        $page->save();
        return redirect('/admin/blog');
    }

    public function edit($id)
    {
        $row = Page::find($id);
        return  view('admin.blog.blog-edit', [
            'title' => 'Изменить блог',
            'row' => $row
        ]);
    }

    public function show(Request $request,$id){

    }

    public function update(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'page_name_ru' => 'required',
            'page_text_ru' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $error = $messages->all();
            return  view('admin.blog.blog-edit', [
                'title' => 'Изменить блог',
                'row' => (object) $request->all(),
                'error' => $error[0]
            ]);
        }

        $page = Page::find($id);
        $page->page_name_ru = $request->page_name_ru;
        $page->page_text_ru = $request->page_text_ru;
        $page->page_image = $request->page_image;
        $page->page_url = Helpers::getTranslatedSlugRu($request->page_name_ru);
        $page->save();
        return redirect('/admin/blog');
    }

    public function destroy($id)
    {
        $page = Page::find($id);
        $page->delete();
    }

    public function uploadImage(Request $request){
        $file = $request->image;
        $file_name = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();

        if(substr($file->getClientMimeType(), 0, 5) != 'image') {
            $result['error'] = 'Загружайте только файлы форматов JPEG, PNG';
            $result['success'] = false;
            return $result;
        }

        if(Storage::disk('blog')->exists($file_name)){
            $file_name = date("YmdHisu").'.'.$extension;;
        }

        Storage::disk('blog')->put($file_name,  File::get($file));
        $result['success'] = true;
        $result['file_name'] = $file_name;
        return $result;
    }
}
