{{--{{dd($sort_color)}}--}}
<div class="dropdowns_top">

    <div class="dropdown">
        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            <?echo count($color);?> цвета
        </button>
        <ul class="dropdown-menu color_prod" aria-labelledby="dropdownMenu1">
            <?$a=0;?>
            @for($i=0;$i<count($color)/3;$i++)
                    <li>
                        @for($c=$a; $c<count($color);$c++)
                            @for($j=0;$j<count($sort_color);$j++)
                                @if($color[$c]->id == $sort_color[$j])
                                    <a href="javascript:void(0)" active='1' color_id='{{$color[$c]->id}}' >
                                        <div class="div1" style="background-color:{{$color[$c]->code_color}}"></div>
                                    </a>
                                    @break
                                @elseif($j == count($sort_color)-1)
                                    <a href="javascript:void(0)" active='0' color_id='{{$color[$c]->id}}' >
                                        <div class="div1" style="background-color:{{$color[$c]->code_color}}"></div>
                                    </a>
                                @endif
                            @endfor

                            @if(!count($sort_color))
                                <a href="javascript:void(0)" active='0' color_id='{{$color[$c]->id}}' >
                                    <div class="div1" style="background-color:{{$color[$c]->code_color}}"></div>
                                </a>
                            @endif

                            @if($c == 2+$a) <? $a=$a+3;?> @break @endif
                        @endfor
                    </li>
            @endfor

        </ul>
    </div>

    <div class="dropdown">
        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            Бренд
        </button>
        <ul class="dropdown-menu sort_by_brand" aria-labelledby="dropdownMenu1">
            @foreach($brands as $key =>$brand)
                <li>
                    <div class="checkboxFour">
                        @for($j=0;$j<count($sort_brand);$j++)
                            @if($brand->brand_id == $sort_brand[$j])
                                <input type="checkbox" checked value="{{$brand->brand_id}}" id="checkboxFourInput1" name="check1" class="input_check sort_check" />
                                @break
                            @elseif($j == count($sort_brand)-1)
                                <input type="checkbox" value="{{$brand->brand_id}}" id="checkboxFourInput1" name="check1" class="input_check sort_check" />
                            @endif
                        @endfor
                        @if(!count($sort_brand))
                           <input type="checkbox" value="{{$brand->brand_id}}" id="checkboxFourInput1" name="check1" class="input_check sort_check" />
                        @endif
                        <label for="checkboxFourInput1"><span>{{$brand->brand_name}}</span></label>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>

    <div class="dropdown">
        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            Коллекция
        </button>
        <ul class="dropdown-menu sort_by_collection" aria-labelledby="dropdownMenu1">
            @foreach($collections as $key => $collection)
                <li>
                    <div class="checkboxFour">
                        @for($j=0;$j<count($sort_collection);$j++)
                            @if($collection->item_id == $sort_collection[$j])
                                <input checked type="checkbox" value="{{$collection->item_id}}" id="checkboxFourInput" name="" class="input_check sort_brand" />
                                @break
                            @elseif($j == count($sort_collection)-1)
                                <input type="checkbox" value="{{$collection->item_id}}" id="checkboxFourInput" name="" class="input_check sort_brand" />
                            @endif
                        @endfor
                        @if(!count($sort_collection))
                            <input type="checkbox" value="{{$collection->item_id}}" id="checkboxFourInput" name="" class="input_check sort_brand" />
                        @endif
                        <label for="checkboxFourInput">{{$collection->item_name_ru}}</label>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
    <div class="dropdown">
        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            цена
        </button>
        <ul class="dropdown-menu price_prod" aria-labelledby="dropdownMenu1">
            <input class="form-control price_from" type="number" placeholder="от" value="{{Request::get('price_1')}}"/>
            <input class="form-control price_before" type="number" placeholder="до" value="{{Request::get('price_2')}}"/>
        </ul>
    </div>
    <div class="dropdown">
        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            размер
        </button>
        <ul class="dropdown-menu size_sort" aria-labelledby="dropdownMenu1" >
            @for($j=0;$j<count($sizes);$j++)
                <li>
                    <div class="checkboxFour">
                        @for($c=0;$c<count($sort_sizes);$c++)
                            @if($sizes[$j]->unique_size == $sort_sizes[$c])
                                <input type="checkbox" checked value="{{$sizes[$j]->unique_size}}" id="checkboxFourInput1" name="check12" class="input_check sort_check">
                                @break
                            @elseif($c == count($sort_sizes)-1)
                                <input type="checkbox"  value="{{$sizes[$j]->unique_size}}" id="checkboxFourInput1" name="check12" class="input_check sort_check">
                            @endif
                        @endfor
                        @if(!count($sort_sizes))
                            <input type="checkbox"  value="{{$sizes[$j]->unique_size}}" id="checkboxFourInput1" name="check12" class="input_check sort_check">
                        @endif
                        <label for="checkboxFourInput1"><span>{{$sizes[$j]->unique_size}}</span></label>

                    </div>
                </li>
            @endfor
        </ul>
    </div>
    <div class="left_link">

    </div>
    <button type="button" class="btn btn-success confirm_sort pull-right">Применить</button>
</div>

<style>
    .right_link, .confirm_sort {
        margin-top: 10px;
    }
</style>

@push('scripts')

<script>
    $('.color_prod a').click(function() {
        var active = $(this).attr('active');
        if(active == '1') {
            $(this).attr('active','0');
        } else {
            $(this).attr('active','1');
        }
    });

    $('.dropdown-toggle').click(function() {
        $(this).parent().find('.dropdown-menu').toggle();
    });

    /*sort location on click*/
    $('.confirm_sort').click(function() {
        var color = $(".color_prod a[active='1']");
        var all_color= color.length > 0 ? "" : 0;
        for(var i=0;i<color.length;i++) {
            all_color += color.eq(i).attr('color_id');
            if(i!=color.length-1) {
                all_color += '-';
            }
        }

        var brand = $(".sort_by_brand input:checked");
        var all_brand = brand.length > 0 ? "" : 0;
        for(var i=0;i<brand.length;i++) {
            all_brand += brand.eq(i).val();
            if(i!=brand.length-1) {
                all_brand += '-';
            }
        }

        var collect = $(".sort_by_collection input:checked");
        var all_collect = collect.length > 0 ? "" : 0;
        for(var i=0;i<collect.length;i++) {
            all_collect += collect.eq(i).val();
            if(i != collect.length-1) {
                all_collect += '-';
            }
        }

        var price_1 = $('.price_from').val();
        var price_2 = $('.price_before').val();

        var size = $(".size_sort input:checked");
        var all_size =size.length > 0 ? "" : 0;
        for(var i=0;i<size.length;i++) {
            all_size += size.eq(i).val();
            if(i!=size.length-1) {
                all_size += '-';
            }
        }



        var url = getUrl();
        var symb = '&';
        if(!url) {
            symb = '?';
            url = "{{Request::url()}}";
        }
        if(all_color) {
            url += symb+'sort_color='+all_color;
            symb = '&';
        }
        if(all_brand) {
            url += symb+'brand='+all_brand;
            symb = '&';
        }
        if(all_collect) {
            url += symb+'collection='+all_collect;
            symb = '&';
        }
        if(price_1) {
            url += symb+'price_1='+price_1;
            symb = '&';
        }
        if(price_2) {
            url += symb+'price_2='+price_2;
            symb = '&';
        }
        if(all_size) {
            url += symb+'sort_sizes='+all_size;
            symb = '&';
        }


        location = url;
    });

    function getUrl() {
        var params = ['category','subcategory','main','sale','sort','sort_desc'];
        var url = "{{Request::url()}}";
        var p;
        var symb = '?';
        for (var i = 0; i < params.length; i++) {
            p = getParam(params[i]);
            if(p) {
                url += symb+params[i]+'='+p;
                symb = '&';
            }
        }
        if(symb == '?') return false;
        else return url;
    }

    function getParam(p) {
        if(p == 'category')
            return "{{Request::get('category')}}";
        if(p == 'subcategory')
            return "{{Request::get('subcategory')}}";
        if(p == 'main')
            return "{{Request::get('main')}}";
        if(p == 'sale')
            return "{{Request::get('sale')}}";
        if(p == 'sort_desc')
            return "{{Request::get('sort_desc')}}";
        if(p == 'sort')
            return "{{Request::get('sort')}}";
    }
</script>

@endpush
