<script>

    $(".sort_check").change(function(){
        var val = $(this).val();
        <? $cat=0; $cats = ['category','subcategory','main']; ?>
                @for($i=0;$i<count($cats);$i++)
                @if(Request::get($cats[$i]))
        <?$cat = $cats[$i];?>
                @break;
                @endif
                @endfor

        var url =  "{{Request::url().($cat ? '?'.$cat.'='.Request::get($cat):'')}}";

        @if($cat) url+='&'; @else url+='?'; @endif

        <? $types = ['sort_color','brand','collection','size']; $type='brand'; ?>
                @for($i=0;$i<count($types);$i++)
                @if($types[$i] == $type) @continue
                @elseif(Request::get($types[$i]))
                url += "{{$types[$i]}}={{Request::get($types[$i])}}&";
        @endif
                @endfor

                url+="{{$type}}="+val;
        location = url;
    });

    $('.div1').click(function(){
        var val = $(this).attr('item');
        <? $cat=0; $cats = ['category','subcategory','main']; ?>
                @for($i=0;$i<count($cats);$i++)
                @if(Request::get($cats[$i]))
        <?$cat = $cats[$i];?>
                @break;
                @endif
                @endfor

        var url =  "{{Request::url().($cat ? '?'.$cat.'='.Request::get($cat):'')}}";

        @if($cat) url+='&'; @else url+='?'; @endif

        <? $types = ['sort_color','brand','collection','size']; $type='sort_color'; ?>
                @for($i=0;$i<count($types);$i++)
                @if($types[$i] == $type) @continue
                @elseif(Request::get($types[$i]))
                url += "{{$types[$i]}}={{Request::get($types[$i])}}&";
        @endif
                @endfor

                url+="{{$type}}="+val;
        location = url;
    });

    $('.sort_brand').click(function(){
        var val = $(this).val();
        <? $cat=0; $cats = ['category','subcategory','main']; ?>
                @for($i=0;$i<count($cats);$i++)
                @if(Request::get($cats[$i]))
        <?$cat = $cats[$i];?>
                @break;
                @endif
                @endfor

        var url =  "{{Request::url().($cat ? '?'.$cat.'='.Request::get($cat):'')}}";

        @if($cat) url+='&'; @else url+='?'; @endif

        <? $types = ['sort_color','brand','collection','size']; $type='collection'; ?>
                @for($i=0;$i<count($types);$i++)
                @if($types[$i] == $type) @continue
                @elseif(Request::get($types[$i]))
                url += "{{$types[$i]}}={{Request::get($types[$i])}}&";
        @endif
                @endfor

                url+="{{$type}}="+val;
        location = url;
    });


</script>