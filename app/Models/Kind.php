<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Kind extends Model
{
    protected $table = 'kind';
    protected $primaryKey = 'kind_id';

    use SoftDeletes;
    protected $dates = ['deleted_at'];
}
