<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/******* Admin page *******/
Route::group([
    'prefix' => 'admin',
    'namespace' => 'Admin',
    'middleware' => ['web']
], function() {
    Route::post('user/is_ban', 'UserController@changeIsBan');
    Route::post('user/upload-avatar', 'UserController@uploadAvatar');
    Route::resource('user', 'UserController');
    Route::resource('page', 'PageController');
    Route::post('blog/upload-image', 'BlogController@uploadImage');
    Route::resource('blog', 'BlogController');
    Route::post('sale/upload-image', 'SaleController@uploadImage');
    Route::resource('sale', 'SaleController');
    Route::post('brand/upload-image', 'BrandController@uploadImage');
    Route::resource('brand', 'BrandController');
    Route::resource('main-page-text', 'PageTextController');
    Route::post('banner/is_show', 'BannerController@changeIsShow');
    Route::post('banner/upload-image', 'BannerController@uploadImage');
    Route::resource('banner', 'BannerController');

    Route::post('discount/{id}/uploadImage', 'DiscountController@uploadImage');
    Route::resource('discount', 'DiscountController');
    Route::get('logout', 'UsersController@logout');
    Route::get('password', 'IndexController@password');
    Route::post('password', 'IndexController@password');
    Route::resource('subscriber', 'SubscriberController');
    Route::resource('delivery', 'DeliveryController');
    Route::get('delivery-send/{delivery_id}', 'DeliveryController@sendDeliveryShow');
    Route::post('delivery-send', 'DeliveryController@sendDelivery');
    Route::resource('delivery-kind', 'DeliveryKindController');
    Route::resource('contact-delivery', 'ContactDeliveryController');
    Route::get('password', 'UserController@password');
    Route::post('password', 'UserController@password');
    Route::get('status-of-sale', 'BannerController@getStatus');

    Route::resource('main-category', 'MainCategoryController');
    Route::post('main-category/is_show', 'MainCategoryController@changeIsShow');
    Route::resource('category', 'CategoryController');
    Route::post('category/is_show', 'CategoryController@changeIsShow');
    Route::resource('subcategory', 'SubCategoryController');
    Route::post('subcategory/is_show', 'SubCategoryController@changeIsShow');
    Route::get('/category/category-list-by-main/{category_id}', 'CategoryController@getCategoryListByMainCategory');
    Route::get('/category/subcategory-list-by-category/{category_id}', 'CategoryController@getSubCategoryListByCategory');
    
    Route::resource('brand-product', 'BrandProductController');
    Route::post('brand-product/is_show', 'BrandProductController@changeIsShow');
    Route::get('product/product-list-by-search', 'ProductController@getProductListBySearch');
    Route::get('product/image', 'ProductController@getImageList');
    Route::get('product/recommended', 'ProductController@getRecommendedProduct');
    Route::post('product/recommended', 'ProductController@addRecommendedProduct');
    Route::delete('product/recommended', 'ProductController@deleteRecommendedProduct');
    Route::post('product/change-main-image', 'ProductController@changeMainImage');
    Route::delete('product/image', 'ProductController@deleteImage');
    Route::resource('product', 'ProductController');
    Route::post('product/is_show', 'ProductController@changeIsShow');
    Route::post('product/upload-image', 'ProductController@uploadImage');
    Route::resource('feature', 'FeatureController');
    Route::get('feature-item', 'FeatureController@getItemListByFeature');
    Route::post('feature-item', 'FeatureController@addItem');
    Route::post('feature-item/{item_id}', 'FeatureController@changeItem');
    Route::delete('feature-item', 'FeatureController@deleteItem');
    Route::resource('review', 'CommentController');
    Route::post('review/is_show', 'CommentController@changeIsShow');
    Route::post('order/status', 'OrderController@editRequest');
    Route::resource('order', 'OrderController');
    Route::resource('size','SizeController');
    Route::get('size_cat','SizeController@sizeCat');
    Route::get('size_value','SizeController@sizeValue');
    Route::resource('color','ColorController');
    Route::get('size/type/create','SizeController@getType');
    Route::post('size/type/store','SizeController@postType');
    Route::get('size/type/edit/{id}','SizeController@getTypeEdit');
    Route::post('size/type/update/{id}','SizeController@postTypeUpdate');
    Route::post('size/type/delete/{id}','SizeController@typeDelete');
    Route::resource('promo-code','PromoController');
});

/******* Main page *******/
Route::group([
    'middleware' => ['web'],
    'namespace' => 'Index',
], function(){
    Route::get('/', 'IndexController@index');
    Route::get('/mobile','IndexController@mobile');
    Route::get('/index', 'IndexController@index');
    Route::get('/help', 'IndexController@help');
    Route::get('/about', 'IndexController@about');
    Route::get('/blog', 'IndexController@blog');
    Route::get('/blog/{blog_url}', 'IndexController@blogDetail');
    Route::get('/sale', 'IndexController@sale');
    Route::get('/sale/{sale_url}', 'IndexController@saleDetail');
    Route::get('/page/{page_url}', 'IndexController@pageDetail');
    Route::post('/subscribe', 'IndexController@subscribe');

    Route::post('/register', 'AuthController@registerAjax');
    Route::post('/login', 'AuthController@loginAjax');
    Route::get('/auth-social', 'AuthController@authSocial');
    Route::get('/logout', 'AuthController@logout');
    Route::post('/reset-password', 'AuthController@resetPassword');
    
    Route::get('/profile', 'ProfileController@index');
    Route::post('/profile', 'ProfileController@saveProfile');

    Route::get('/catalog', 'CatalogController@index');

    Route::post('/password', 'ProfileController@changePassword');

    Route::get('/product/{product_url}', 'ProductController@getProductById');
    Route::post('/product/favorite', 'ProductController@changeFavoriteProduct');
    Route::get('/favorite-count', 'ProductController@getFavoriteProductCount');

    Route::post('/comment', 'ProductController@addComment');
    Route::get('/comment/{product_id}', 'ProductController@getCommentListByProduct');

    Route::get('/basket', 'RequestController@getProductListInBasket');
    Route::post('/basket', 'RequestController@addToBasket');
    Route::post('/basket/unit', 'RequestController@changeProductUnitInBasket');
    Route::delete('/basket', 'RequestController@deleteProductFromBasket');
    Route::get('/basket-count', 'RequestController@getBasketCount');
    Route::post('/order', 'RequestController@addOrder');

    Route::get('/search','IndexController@getSearchByProduct');
    Route::get('promo-code-activation','IndexController@promoCodeActivation');
    Route::post('subscription-profile','IndexController@subscriptionProfile');

    Route::get('/epay/process/{order_id}','RequestController@epayProcess');
});


Route::group([
    'namespace' => 'Index',
], function() {
    Route::post('/epay/success','RequestController@epaySuccess');
});

Route::post('social-login', 'Index\AuthController@loginWithSocial');

Route::group([
    'prefix' => 'api',
    'namespace' => 'Api',
], function(){


    Route::get('main', 'MainController@main');
    Route::get('contacts', 'MainController@contacts');
    Route::post('blog', 'MainController@blog');
    Route::post('blog/{id}', 'MainController@blogShow');

    Route::group([
        'prefix' => 'shop',
    ], function(){

        Route::get('{gender}/clothes', 'ProductController@clothes');
        Route::get('categories/{category_id}', 'ProductController@categories');
        Route::get('subcategories/{category_id}', 'ProductController@subcategories');
        Route::post('filter', 'ProductController@filterBySCID');
    });


    Route::group([
        'prefix' => 'account',
    ], function(){
        Route::post('signUp', 'AccountController@signUp');
        Route::post('signIn', 'AccountController@signIn');
        Route::post('resetPassword', 'AccountController@resetPassword');
        Route::post('fromSocial', 'AccountController@fromSocial');
    });


    Route::group([
        'middleware' => 'account',
    ], function(){
        Route::group([
            'prefix' => 'account',
        ], function(){
            Route::post('edit', 'AccountController@edit');
            Route::get('profile', 'AccountController@profile');
            Route::post('changePassword', 'AccountController@changePassword');
            Route::post('favorites', 'AccountController@favorites');
            Route::post('myOrders', 'AccountController@myOrders');
        });

        Route::group([
            'prefix' => 'shop',
        ], function() {
            Route::post('products/byDiscount/{id}', 'ProductController@byDiscount');
            Route::post('products/bySubCat/{category_id}', 'ProductController@productsBySCID');
            Route::post('product/favorite/{id}', 'ProductController@favorite');
            Route::post('product/canReview/{id}', 'ProductController@productByID');
            Route::post('product/addReview/{id}', 'ProductController@addReview');
            Route::post('product/byID/{id}', 'ProductController@productByID');
            Route::post('product/reviews/{id}', 'ProductController@reviews');
        });

        Route::group([
            'prefix' => 'basket',
        ], function() {
            Route::post('list', 'OrderController@basket');
            Route::post('product/add/{id}', 'OrderController@addProduct');
            Route::post('product/delete/{id}', 'OrderController@deleteProduct');
            Route::post('product/setUnit/{id}', 'OrderController@setUnit');
            Route::post('promocode', 'OrderController@promocode');
            Route::post('payment', 'OrderController@payment');
        });
    });
});

